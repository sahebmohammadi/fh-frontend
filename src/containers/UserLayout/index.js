import Layout from "@/containers/Layout";
import Link from "next/link";
import { MdAccountBalance } from "react-icons/md";
import { BsFileCode } from "react-icons/bs";
import { logout } from "@/services/userAuthServicer";

const UserLayout = ({ children }) => {
  const signOuHandler = () => {
    logout()
      .then((res) => {
        localStorage.removeItem("userInfo");
        localStorage.removeItem("cartItems");
        localStorage.removeItem("token");
        document.location.href = "/auth";
        setIsShowMenu(false);
      })
      .catch();
  };

  return (
    <Layout>
      <main className="grid grid-cols-1 sm:grid-cols-4 gap-4">
        <section className={`section-border md:col-span-1 max-h-48`}>
          <aside className="flex flex-col space-y-4 font-bold text-gray-700 text-base">
            <Link href="/profile">
              <a className="flex py-2 text-gray-500 hover:text-primary dark:text-slate-400 dark:hover:text-blue-500">
                <MdAccountBalance size={20} className="ml-4" />
                <span className="font-bold text-sm block">حساب کاربری</span>
              </a>
            </Link>
            <Link href="/profile/downloads">
              <a className="flex py-2 text-gray-500 hover:text-primary dark:text-slate-400 dark:hover:text-blue-500">
                <BsFileCode size={20} className="ml-4" />
                <span className="font-bold text-sm block">دوره های من</span>
              </a>
            </Link>
            <p
              className="py-4 cursor-pointer text-gray-400 hover:text-red-400 text-sm"
              onClick={signOuHandler}
            >
              خروج از حساب کاربری
            </p>
          </aside>
        </section>
        <section className={`section-border md:col-span-3`}>{children}</section>
      </main>
    </Layout>
  );
};

export default UserLayout;
