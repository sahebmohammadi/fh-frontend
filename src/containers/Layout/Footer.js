import IconButton from "@material-ui/core/IconButton";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import GitHubIcon from "@material-ui/icons/GitHub";
import TwitterIcon from "@material-ui/icons/Twitter";
import TelegramIcon from "@material-ui/icons/Telegram";
import YouTubeIcon from "@material-ui/icons/YouTube";
import Link from "next/link";
const Footer = (props) => {
  return (
    <footer className="bg-gray-100 dark:bg-slate-800 dark:text-slate-400 p-4 md:py-8 text-gray-500 mt-10">
      <div className="grid gap-10 grid-cols-5 grid-row-2 container md:max-w-screen-xl mx-auto">
        <section className="col-span-5 md:col-span-2 flex flex-col justify-start">
          <div>
            <h2 className="text-primary font-bold text-xs sm:text-sm mb-4 dark:text-slate-200">
              آکادمی آنلاین فرانت هوکس، دوره های کوچ محور{" "}
              <strong>برنامه نویسی وب</strong>
            </h2>
            <div className="sm:leading-7 text-xs sm:text-sm leading-5">
              <span>
                <strong>صاحب محمدی هستم ، برنامه نویس وب. </strong>
                از سال 93 به برنامه نویسی مشغول هستم و بیش از سه سال هست که به
                صورت تخصصی به عنوان توسعه دهنده فرانت در استارتاپ هایی مثل{" "}
              </span>
              <a target="_blank" href="https://decooj.com/landing">
                {" "}
                دکوژ{" "}
              </a>
              <a target="_blank" href="https://www.calagh.ir/">
                {" "}
                ، سهم خلاق{" "}
              </a>
              <span>و صرافی های آنلاین مشغول به فعالیت بودم.</span>
              <p>
                در این چند سال چالش های زیادی را در زمینه برنامه نویسی چه به
                صورت کار فریلنسری در سایت های خارجی و همچنین در کار تیمی تجربه
                کرده ام. هزینه مالی و زمانی بسیار زیادی را بابت کسب آموزش در
                معتبر ترین دوره های سایت های خارجی صرف کردم. همه ی آموزش های
                فرانت هوکس بر پایه سه اصل، دوره های با کیفیت و پروژه محور و به
                روز، پشتیبانی مادام العمر به همراه آپدیت های رایگان و عودت هزینه
                در صورت هر گونه نارضایتی استوار است. امیدوارم فرانت هوکس همراه
                خوبی برای ورود شما به بازار کار و دنیای برنامه نویسی باشد. شما
                می توانید از طریق{" "}
                <a
                  className="text-blue-600"
                  href="https://www.instagram.com/sahebmohamadi.ir/"
                  target="_blank"
                >
                  اینستاگرام
                </a>
                ،{" "}
                <a
                  className="text-blue-600"
                  href="https://t.me/fronthooks_support"
                  target="_blank"
                >
                  تلگرام
                </a>
                ،{" "}
                <a
                  className="text-blue-600"
                  href="https://ir.linkedin.com/in/saheb-mohamadi-227ab4112"
                  target="_blank"
                >
                  یا لینکدین{" "}
                </a>
                با من در ارتباط باشید
              </p>
            </div>
          </div>
          <div className="mt-4">
            <ul className="flex flex-wrap justify-between items-center w-full">
              <li className="block">
                <a
                  href="https://www.instagram.com/sahebmohamadi.ir/"
                  target="_blank"
                >
                  <IconButton>
                    <InstagramIcon
                      fontSize="large"
                      className="hover:text-red-600 dark:text-slate-400 dark:hover:text-red-600"
                    />
                  </IconButton>
                </a>
              </li>
              <li className="block">
                <a
                  href="https://ir.linkedin.com/in/saheb-mohamadi-227ab4112"
                  target="_blank"
                >
                  <IconButton>
                    <LinkedInIcon
                      fontSize="large"
                      className="hover:text-blue-600 dark:text-slate-400 dark:hover:text-blue-600"
                    />
                  </IconButton>
                </a>
              </li>
              <li className="block">
                <a href="https://github.com/sahebmohammadi" target="_blank">
                  <IconButton>
                    <GitHubIcon
                      fontSize="large"
                      className="hover:text-blue-600 dark:text-slate-400 dark:hover:text-blue-600"
                    />
                  </IconButton>
                </a>
              </li>
              <li className="block">
                <a href="https://twitter.com/mohamadisaheb" target="_blank">
                  <IconButton>
                    <TwitterIcon
                      fontSize="large"
                      className="hover:text-blue-600 dark:text-slate-400 dark:hover:text-blue-600"
                    />
                  </IconButton>
                </a>
              </li>
              <li className="block">
                <a href="https://t.me/fronthooks_support" target="_blank">
                  <IconButton>
                    <TelegramIcon
                      fontSize="large"
                      className="hover:text-blue-600 dark:text-slate-400 dark:hover:text-blue-600"
                    />
                  </IconButton>
                </a>
              </li>
              <li className="block">
                <a
                  href="https://www.youtube.com/channel/UCJwBq05vX977h-uqNhwWzBA"
                  target="_blank"
                >
                  <IconButton>
                    <YouTubeIcon
                      fontSize="large"
                      className="hover:text-red-600 dark:text-slate-400 dark:hover:text-red-600"
                    />
                  </IconButton>
                </a>
              </li>
            </ul>
          </div>
        </section>
        <section className="col-span-5 md:col-span-1">
          <h3 className="text-xl font-bold mb-4 dark:text-slate-200">
            بخش های سایت
          </h3>
          <nav>
            <ul className="">
              <li className="list-disc py-2 list-inside">
                <Link href="/courses">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    دوره های آموزشی
                  </a>
                </Link>
              </li>
              <li className="list-disc py-2 list-inside">
                <Link href="/blogs">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    بلاگ های آموزشی
                  </a>
                </Link>
              </li>
              <li className="list-disc py-2 list-inside">
                <Link href="/about-us">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    درباره ما
                  </a>
                </Link>
              </li>
              <li className="list-disc py-2  list-inside">
                <Link href="/learning-path">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    شروع یادگیری
                  </a>
                </Link>
              </li>
            </ul>
          </nav>
        </section>
        <section className="col-span-5 md:col-span-1">
          <h3 className="text-xl font-bold mb-4 dark:text-slate-200">
            دوره های آموزشی
          </h3>
          <nav>
            <ul className="">
              <li className="list-disc py-2 list-inside">
                <Link href="/courses/react-course">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    دوره متخصص ریکت و ریداکس
                  </a>
                </Link>
              </li>
              <li className="list-disc py-2 list-inside">
                <Link href="/courses/nextjs">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    دوره متخصص Next.js
                  </a>
                </Link>
              </li>
              <li className="list-disc py-2 list-inside">
                <Link href="/courses/advanced-javascript">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    دوره پیشرفته جاوااسکریپت
                  </a>
                </Link>
              </li>
              <li className="list-disc py-2  list-inside">
                <Link href="/courses/tailwindCSS">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    دوره پروژه محور تیلویند
                  </a>
                </Link>
              </li>
              <li className="list-disc py-2  list-inside">
                <Link href="/courses/responsive-web">
                  <a className="py-2 font-bold dark:hover:text-slate-300 transition-all duration-300 hover:text-blue-500">
                    دوره طراحی وب رسپانسیو
                  </a>
                </Link>
              </li>
            </ul>
          </nav>
        </section>
        <section className="col-span-5 md:col-span-1 flex">
          <img
            src="https://cdn.zarinpal.com/badges/trustLogo/1.svg"
            style={{ width: "96px", height: "100px" }}
            alt="نماد اعتماد زرین پال"
          />
        </section>
        <section className="col-span-5">
          <p className="">تمامی حقوق برای فرانت هوکس محفوظ است</p>
        </section>
      </div>
    </footer>
  );
};

export default Footer;
