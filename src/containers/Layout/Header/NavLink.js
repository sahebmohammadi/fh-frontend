import { useRouter } from "next/router";
import Link from "next/link";
import React from "react";

const NavLink = ({ href, children }) => {
  const router = useRouter();
  return (
    <li className="block">
      <Link href={href}>
        <a
          className={`text-gray-700 dark:text-slate-300 block hover:text-primary dark:hover:text-slate-200
          px-3 py-2 rounded-md text-sm font-medium ${
            router.pathname === href ? "text-primary" : ""
          }`}
        >
          {children}
        </a>
      </Link>
    </li>
  );
};

export default NavLink;
