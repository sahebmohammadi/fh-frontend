import { HiOutlineLogin } from "react-icons/hi";
import { IconButton } from "@material-ui/core";
import MenuOutlinedIcon from "@material-ui/icons/MenuOutlined";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import CancelOutlinedIcon from "@material-ui/icons/CancelOutlined";
import { useState } from "react";
import NavItems from "./NavItems";
import Badge from "@material-ui/core/Badge";
import Link from "next/link";
import { useSelector } from "react-redux";
import { RiArrowDropDownLine } from "react-icons/ri";
import ProfileMenu from "./ProfileMenu";
import ToggleTheme, { MobileThmeToggler } from "@/common/DropDownHeadLess";
import DrawerComponent from "@/common/Drawer";
import { XIcon } from "@heroicons/react/outline";

export default function Header() {
  const [isOpenDrawer, setIsOpenDrawer] = useState(false);

  return (
    <header
      className={`sticky top-0 w-full z-20 bg-white shadow-lg border-b border-gray-90 dark:bg-slate-900 dark:text-slate-400 dark:border-slate-700 shadow-white/10 
      `}
    >
      <div className="container md:max-w-screen-xl mx-auto top-0 py-2">
        <nav className="top-0">
          <div className="w-full mx-auto">
            <div className="flex items-center justify-between">
              <div className="flex justify-between md:justify-start items-center w-full">
                <div className="flex justify-start items-center">
                  <div className="ml-3  md:hidden">
                    <IconButton
                      color="primary"
                      onClick={() => setIsOpenDrawer(true)}
                    >
                      {isOpenDrawer ? (
                        <CancelOutlinedIcon fontSize="medium" />
                      ) : (
                        <MenuOutlinedIcon fontSize="medium" />
                      )}
                    </IconButton>
                  </div>
                  <div className="">
                    <Link href="/">
                      <a>
                        <img
                          className="h-7 w-7"
                          src="/images/fh-logo.svg"
                          alt="Workflow"
                        />
                      </a>
                    </Link>
                  </div>
                </div>
                <div className="md:hidden flex justify-end items-center">
                  <ShoppingCartComponent />
                  <div className="mr-3">
                    <ProfileComponent />
                  </div>
                </div>
                {/* Desktop NavBar */}
                <div className="hidden md:block mr-10">
                  <NavItems />
                </div>
              </div>
              {/* desktop cart / accont */}
              <div className="hidden md:flex items-center gap-x-4">
                <div className="ml-4 gap-x-4 flex items-center md:ml-4">
                  <ToggleTheme />
                  <ShoppingCartComponent />
                  <ProfileComponent />
                </div>
              </div>
              {/* <!-- humberger section --> */}
            </div>
          </div>

          {/* <!-- Mobile menu, show/hide based on menu state. --> */}

          <DrawerComponent isOpen={isOpenDrawer} setIsOpen={setIsOpenDrawer}>
            {/* Drawer header */}
            <div className="flex items-center justify-between w-full mb-6 pb-3 border-gray-400 border-b dark:border-slate-500">
              <Link href="/">
                <a>
                  <img className="w-6 h-6" src="/images/fh-logo.svg" alt="" />
                </a>
              </Link>
              <button onClick={() => setIsOpenDrawer(false)}>
                <XIcon className="w-6 h-6 stroke-gray-500 dark:stroke-slate-400" />
              </button>
            </div>
            {/* drawer content */}
            <div className="overflow-y-auto">
              <NavItems />
              <MobileThmeToggler />
            </div>
          </DrawerComponent>
        </nav>
      </div>
    </header>
  );
}

const ShoppingCartComponent = () => {
  const cartItems = useSelector((state) => state.cart.addedItems.length);
  return (
    <Link href="/cart">
      <a>
        <IconButton color="primary">
          <Badge
            color="error"
            badgeContent={cartItems}
            showZero
            overlap="rectangular"
          >
            <ShoppingCartOutlinedIcon fontSize="medium" size="small" />
          </Badge>
        </IconButton>
      </a>
    </Link>
  );
};

const ProfileComponent = () => {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;
  const [isShowMenu, setIsShowMenu] = useState(false);

  return !userInfo ? (
    <Link href="/auth">
      <a>
        <div className="flex justify-start items-center">
          <IconButton color="primary">
            <HiOutlineLogin size={24} />
          </IconButton>
          <p className="text-primary text-sm font-bold ml-2">ورود</p>
        </div>
      </a>
    </Link>
  ) : (
    <>
      <div
        className={`flex cursor-pointer items-center ml-4 sm:ml-0 sm:mr-2 z-40`}
        onClick={() => setIsShowMenu(!isShowMenu)}
      >
        <img src="/images/account-icon.png" className="rounded-full w-8 h-8" />
        <RiArrowDropDownLine
          size={40}
          className="text-primary hidden sm:block"
        />
      </div>
      <ProfileMenu isShowMenu={isShowMenu} setIsShowMenu={setIsShowMenu} />
    </>
  );
};
