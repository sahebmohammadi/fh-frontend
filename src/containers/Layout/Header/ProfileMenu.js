import { toArabicDigits } from "@/utils/toArabicNumbers";
import { MdAccountBalance } from "react-icons/md";
import { HiOutlineLogout } from "react-icons/hi";
import { useSelector, useDispatch } from "react-redux";
import { useState } from "react";
import { signout } from "@/redux/user/userActions";
import Link from "next/link";
import { logout } from "@/services/userAuthServicer";
import { BsBookmarkCheck } from "react-icons/bs";

const ProfileMenu = ({ isShowMenu, setIsShowMenu }) => {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;
  const dispatch = useDispatch();
  const clickHandler = (event) => setIsOpen(event.currentTarget);

  const closeSignOutHandler = async () => {
    logout()
      .then((res) => {
        localStorage.removeItem("userInfo");
        localStorage.removeItem("cartItems");
        localStorage.removeItem("token");
        document.location.href = "/auth";
        setIsShowMenu(false);
      })
      .catch();
  };

  return (
    <section>
      <div
        onClick={() => setIsShowMenu(false)}
        style={{
          backgroundColor: "rgba(38,46,62,.39)",
          opacity: isShowMenu ? "1" : "0",
          display: !isShowMenu && "none",
        }}
        className={`absolute top-0 left-0 w-screen h-screen cursor-default transition duration-300
        ${isShowMenu ? "z-10" : "z-0"}`}
      ></div>
      <div
        className={`bg-white rounded p-6 pt-8 w-72 border
           border-gray-200 text-gray-500  text-sm
           absolute top-20 left-4 sm:top-28 sm:left-40 transition dark:bg-slate-700 dark:border-slate-500/70 dark:text-slate-400 duration-500 col-end-auto 
           ${isShowMenu ? "z-20" : "z-0"}`}
        style={{
          opacity: isShowMenu ? "1" : "0",
          transform: isShowMenu ? "scale(1)" : "scale(0)",
        }}
      >
        <section>
          <li className="flex justify-start items-center w-full h-12 cursor-default">
            <img
              src="/images/account-icon.png"
              className="rounded-full w-12 h-12 ml-4"
            />
            <div className="text-sm w-full flex flex-col h-full justify-between">
              <span className="font-bold block">{userInfo?.name}</span>
              <span className="block">
                {userInfo?.phoneNumber &&
                  toArabicDigits(userInfo?.phoneNumber || "")}
              </span>
            </div>
          </li>
          <hr className="my-2 text-gray-400 dark:h-0 dark:border-slate-500" />
          <li className="">
            <Link href="/profile">
              <a
                className="flex py-2 text-gray-500 hover:text-primary dark:text-slate-400 dark:hover:text-blue-500"
                onClick={() => setIsShowMenu(false)}
              >
                <MdAccountBalance size={20} className="ml-4" />
                <span className="font-bold text-sm block">حساب کاربری</span>
              </a>
            </Link>
          </li>
          <li className="">
            <Link href="/me/bookmarks">
              <a
                className="flex py-2 text-gray-500 hover:text-primary dark:text-slate-400 dark:hover:text-blue-500"
                onClick={() => setIsShowMenu(false)}
              >
                <BsBookmarkCheck size={20} className="ml-4" />
                <span className="font-bold text-sm block">
                  پست های ذخیره شده
                </span>
              </a>
            </Link>
          </li>
          <li
            onClick={closeSignOutHandler}
            className=" flex py-2 leading-8 hover:text-red-400 cursor-pointer"
          >
            <HiOutlineLogout size={20} className="ml-4 hover:text-red-400" />
            <span className="font-bold text-sm">خروج</span>
          </li>
        </section>
      </div>
    </section>
  );
};

export default ProfileMenu;
