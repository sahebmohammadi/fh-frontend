import NavLink from "./NavLink";
import CoursesPopover from "@/common/CoursesPopover";
import ContactsPopover from "@/common/ContacsPopover";

const NavItems = () => {
  return (
    <ul
      className={`space-y-1 md:space-y-0 flex flex-col 
    md:flex-row md:items-center justify-between gap-x-4`}
    >
      <NavLink href="/">خانه</NavLink>
      <li className="px-3 py-2">
        <CoursesPopover />
      </li>
      <NavLink href="/blogs">بلاگ ها</NavLink>
      <li className="px-3 py-2">
        <ContactsPopover />
      </li>
    </ul>
  );
};

export default NavItems;
