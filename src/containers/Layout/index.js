import { DiscountBanner } from "@/components/Campaign";
import Footer from "./Footer";
import Header from "./Header/Header";

function Layout({ children }) {
  return (
    <div>
      <div className="h-full min-h-screen bg-gray-50 relative  dark:text-slate-400  dark:bg-slate-900">
        <div className="mx-auto sm:px-0 h-full">
          <div className="flex flex-col min-h-screen">
            <Header />
            <div className="flex-1 container md:max-w-screen-xl mx-auto mt-4 sm:mt-8 px-4">
              {children}
            </div>
            <Footer />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Layout;
