import Layout from "@/containers/Layout";
import Link from "next/link";

const AdminLayout = ({ children }) => {
  return (
    <Layout>
      <main className="grid grid-cols-1 sm:grid-cols-12 gap-4">
        <section
          className={`section-border md:col-span-2 max-h-52 sticky top-24 dark:bg-slate-700`}
        >
          <aside className="flex flex-col space-y-4 font-bold text-gray-700 dark:text-slate-400 text-base">
            <Link href="/admin-profile/users">
              <a> کاربران</a>
            </Link>
            <Link href="/admin-profile/orders">
              <a> سفارشات</a>
            </Link>
            <Link href="/admin-profile/comments">
              <a> نظرات</a>
            </Link>
            <Link href="/admin-profile/discount">
              <a> تخفیف جشنواره</a>
            </Link>
            <Link href="/admin-profile/transactions">
              <a> تراکنش ها</a>
            </Link>
          </aside>
        </section>
        <section className="md:col-span-10 section-border">{children}</section>
      </main>
    </Layout>
  );
};

export default AdminLayout;
