import http from "./htppService";

const getAllProducts = () => {
  return http.get(`/products`);
};

export default getAllProducts;
