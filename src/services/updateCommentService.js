import http from "./htppService";

const updateComment = (commentId, status) => {
  return http.put(`/comment/${commentId}`, { status });
};

export default updateComment;
