import http from "./htppService";

const getAllUsers = () => {
  return http.get(`/user`);
};

export default getAllUsers;
