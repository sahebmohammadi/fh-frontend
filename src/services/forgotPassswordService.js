import http from "./htppService";

const forgotPassword = (email) => {
  return http.post(`/user/forgot-password`, { email: email });
};

export default forgotPassword;
