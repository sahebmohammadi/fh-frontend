import http from "./htppService";

const getOneProduct = (productId) => {
  return http.get(`/products/${productId}`);
};

export default getOneProduct;
