import http from "./htppService";

const getPosts = (queries) => {
  return http.get(`/posts?${queries}`);
};

export default getPosts;
