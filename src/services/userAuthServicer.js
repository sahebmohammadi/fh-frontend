import http from "./htppService";

export const sendCode = (emailPhone, token) => {
  return http.post(`/user/user-existence`, { emailPhone, token });
};

export const verifyCode = (verficationCode, token) => {
  return http.post(`/user/verify`, { verficationCode, token });
};

export const completeProfile = (data) => {
  return http.post(`/user/complete-profile`, data);
};

export const verifyToken = () => {
  return http.post(`/user/verify-token`);
};

export const logout = () => {
  return http.get(`/user/logout`);
};
