import http from "./htppService";

const checkCoupon = (coupon, cart) => {
  return http.post("/coupon", { coupon, cart });
};

export default checkCoupon;
