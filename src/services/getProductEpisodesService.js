import http from "./htppService";

const getProductEpisodes = (productId) => {
  return http.get(`/episode/${productId}`);
};

export default getProductEpisodes;
