import http from "./htppService";

const getAllOrders = () => {
  return http.get(`/order`);
};

export default getAllOrders;
