import http from "./htppService";

const getDownloadToken = (episodeId) => {
  return http.post(`/download/`, { id: episodeId });
};
export default getDownloadToken;
