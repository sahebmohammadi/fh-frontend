import http from "./htppService";

const resetPassword = (password, token) => {
  return http.patch(`/user/reset-password/${token}`, { password: password });
};

export default resetPassword;
