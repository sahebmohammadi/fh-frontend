import http from "./htppService";

const getAllDiscounts = () => {
  return http.get(`/discount`);
};

export default getAllDiscounts;
