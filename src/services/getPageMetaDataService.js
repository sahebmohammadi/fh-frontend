import http from "./htppService";

const getPageMetaData = (pageLink) => {
  return http.get(`/page-meta/get-meta?pageLink=${pageLink}`);
};

export default getPageMetaData;
