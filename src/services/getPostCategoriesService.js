import http from "./htppService";

const getPostCategories = () => {
  return http.get(`/post-category`);
};

export default getPostCategories;
