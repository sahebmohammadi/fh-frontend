import http from "./htppService";

const createComment = (data, token) => {
  return http.post("/comment/save-comment", data);
};

export default createComment;
