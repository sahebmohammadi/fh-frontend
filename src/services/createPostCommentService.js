import http from "./htppService";

const createPostComment = (data) => {
  return http.post("/posts-comment/save-comment", data);
};

export default createPostComment;
