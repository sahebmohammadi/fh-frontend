import axios from "axios";

// axios.defaults.baseURL = "https://api.fronthooks.ir/api";
// axios.defaults.baseURL = "http://localhost:5000/api";

const baseURL =
  process.env.NODE_ENV === "development"
    ? "http://localhost:5000/api"
    : "https://api.fronthooks.ir/api";

const app = axios.create({
  baseURL,
  withCredentials: true,
  // credentials: "include",
});

app.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error && error.response?.status === 401) {
      // console.log(error?.response?.data);
      localStorage.removeItem("token");
      localStorage.removeItem("persist:userSignin");
      document.location.href = "/auth";
    }
    return Promise.reject(error);
  }
);

const http = {
  get: app.get,
  post: app.post,
  delete: app.delete,
  put: app.put,
  patch: app.patch,
};
export default http;
