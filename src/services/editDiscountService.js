import http from "./htppService";

const editDiscount = (productId, data) => {
  return http.put(`/discount/${productId}`, data);
};

export default editDiscount;
