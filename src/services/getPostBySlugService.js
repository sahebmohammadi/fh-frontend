import http from "./htppService";

const getPostBySlug = (slug) => {
  return http.get(`/posts/${slug}`);
};

export default getPostBySlug;
