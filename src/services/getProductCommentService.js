import http from "./htppService";

const getProductComments = (productId) => {
  return http.get(`/comment/${productId}`);
};

export default getProductComments;
