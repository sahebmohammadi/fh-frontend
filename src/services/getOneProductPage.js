import http from "./htppService";

const getOneProductPage = (pageLink) => {
  return http.get(`/products/page/${pageLink}`);
};

export default getOneProductPage;
