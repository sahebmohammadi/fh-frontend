import http from "./htppService";

const getProductDiscount = (productId) => {
  return http.get(`/discount/${productId}`);
};

export default getProductDiscount;
