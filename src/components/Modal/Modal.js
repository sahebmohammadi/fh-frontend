import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import Select from "react-select";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import updateCommentStatus from "@/services/updateCommentService";
import toast from "react-hot-toast";

const options = [
  { value: 0, label: "reject" },
  { value: 1, label: "pending" },
  { value: 2, label: "confirm" },
];

export default function EditCommentModal({ open, setOpen, commentId }) {
  const [status, setStatus] = useState(1);
  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = () => {
    updateCommentStatus(commentId, status)
      .then((res) => toast.success(res.data.message))
      .catch((err) => {
        if (err?.response?.data?.message) {
          toast.error(err?.response?.data?.message);
        }
      });
    setOpen(false);
  };
  const handleChange = (input) => {
    setStatus(input.value);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">تغییر وضعیت کامنت</DialogTitle>

        <DialogContent className="w-72 h-48">
          <Select options={options} onChange={handleChange} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            cancel
          </Button>
          <Button onClick={handleSubmit} color="primary" autoFocus>
            save changes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
