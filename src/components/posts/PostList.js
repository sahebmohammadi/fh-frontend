import usePush from "@/hooks/usePush";
import http from "@/services/htppService";
import toArabicNumber from "@/utils/toArabicNumbers";
import Link from "next/link";
import { useRouter } from "next/router";
import toast from "react-hot-toast";
import { BsClockHistory } from "react-icons/bs";

const PostList = ({ posts }) => {
  const router = useRouter();

  const likeHandler = async (blogId) => {
    http
      .put(`/posts/like/${blogId}`)
      .then(({ data }) => {
        toast.success(data.message);
        usePush(router);
      })
      .catch((err) => {
        if (err?.response?.data?.message) {
          toast.error(err.response.data.message);
        }
      });
  };

  const bookmarkHandler = async (blogId) => {
    http
      .put(`/posts/bookmark/${blogId}`)
      .then(({ data }) => {
        toast.success(data.message);
        usePush(router);
      })
      .catch((err) => {
        // console.log(err?.response?.data?.message);
        if (err?.response?.data?.message) {
          toast.error(err.response.data.message);
        }
      });
  };

  return posts.docs.map((blog) => {
    return (
      <div
        key={blog._id}
        className="bg-white flex flex-col items-center p-3 rounded-[48px] col-span-6 md:col-span-3 lg:col-span-2 dark:bg-slate-800 ring-1 ring-slate-900/5"
      >
        <div className="mb-9 w-2/3 mt-4">
          <Link href={`/posts/${blog.slug}`}>
            <a href="">
              <img src={`/images/logos_vue.png`} alt="" />
            </a>
          </Link>
        </div>
        <div className="bg-myGray-200 dark:bg-slate-700 flex flex-col justify-between flex-1 text-myGray-500 font-bold text-xl p-3 rounded-[32px] w-full">
          <Link href={`/posts/${blog.slug}`}>
            <a className="mb-5 block font-bold text-slate-800 hover:text-blue-600 dark:text-white dark:hover:text-sky-400">
              {blog.title}
            </a>
          </Link>
          {/* blog description */}
          <div className="">
            <div className="flex items-center justify-between mb-4">
              <div className="flex items-center">
                <img
                  className="w-6 h-6 object-cover rounded-full border-2 border-white ml-2 ring ring-white/80"
                  src="/images/saheb.png"
                  alt=""
                />
                <span className="text-xs font-semibold text-gray-400 dark:text-slate-400">
                  {blog.author.name}
                </span>
              </div>
              <div className="px-3 py-1 text-xs text-blue-600 bg-blue-100 rounded-xl transition duration-200 hover:bg-blue-700 hover:text-white dark:bg-slate-600 dark:bg-opacity-60 dark:hover:bg-opacity-100 dark:hover:text-sky-500 dark:text-slate-200">
                <Link href={`/blogs/${blog.category.englishTitle}`}>
                  <a> {blog.category.title}</a>
                </Link>
              </div>
            </div>
            <div className="flex items-center text-xs justify-between mb-1">
              <div className="flex items-center gap-x-2">
                <button className="flex items-center bg-gray-200 transition duration-300 px-1 py-0.5 rounded text-gray-500 dark:bg-slate-500 dark:text-slate-300 dark:bg-opacity-30">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-4 w-4 ml-0.5"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="2"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
                    />
                  </svg>
                  <span className="">{toArabicNumber(blog.commentsCount)}</span>
                </button>
                <button
                  onClick={() => likeHandler(blog._id)}
                  className="flex items-end bg-red-100 hover:bg-red-500 transition duration-300 hover:text-white px-1 py-0.5 rounded text-red-500 dark:bg-opacity-10
                  dark:hover:bg-opacity-100
                  "
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className={`h-4 w-4 ml-0.5 ${
                      blog.isLiked ? "fill-red-500" : ""
                    }`}
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="2"
                    fill="none"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                    />
                  </svg>

                  <span className="">{toArabicNumber(blog.likesCount)}</span>
                </button>
                <button
                  onClick={() => bookmarkHandler(blog._id)}
                  className="flex items-end bg-blue-100 hover:bg-blue-500 transition duration-300 hover:text-white px-1 py-0.5 rounded text-blue-500
                  dark:bg-opacity-10 dark:hover:bg-opacity-100
                  "
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className={`h-4 w-4  ${
                      blog.isBookmarked ? "fill-blue-500" : ""
                    }`}
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="2"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
                    />
                  </svg>
                </button>
              </div>
              <div className="flex items-center gap-x-1 text-gray-400 text-[10px]">
                <BsClockHistory className="" size={12} />
                <span>زمان مطالعه:</span>
                <span>{blog.readingTime}</span>
                <span>دقیقه</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  });
};

export default PostList;
