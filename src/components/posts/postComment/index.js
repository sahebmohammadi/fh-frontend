import React, { useState } from "react";
import Button from "@/common/Button";
import createComment from "@/services/createPostCommentService";
import SingleComment from "./SingleComment";
import ReplyComment from "./ReplyComment";
import toast from "react-hot-toast";

const PostComments = ({ post }) => {
  const [comment, setComment] = useState("");

  const handleChange = (e) => {
    setComment(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const data = {
      content: comment,
      postId: post._id,
    };

    createComment(data)
      .then(({ data }) => {
        toast.success(data.message);
        setComment("");
      })
      .catch((err) => {
        if (err.response?.data?.message) {
          toast.error(err.response?.data?.message);
        }
      });
  };

  return (
    <>
      <h3 className="mb-10 font-extrabold text-2xl md:text-3xl">نظرات</h3>

      {post.comments.map((comment, index) => {
        return (
          !comment.responseTo &&
          comment.status === 2 && (
            <React.Fragment key={comment._id}>
              <SingleComment comment={comment} postId={post._id} />
              <ReplyComment
                commentList={post.comments}
                postId={post._id}
                parentCommentId={comment._id}
              />
            </React.Fragment>
          )
        );
      })}

      {/* root Comment Form */}
      <form onSubmit={handleSubmit} className="mt-8">
        <span className="dark:text-slate-200">ارسال دیدگاه جدید</span>
        <textarea
          className="focus:ring-primary p-4 rounded my-4 w-full border-none ring-2 ring-slate-300 shadow-sm focus:outline-none focus:ring-2 dark:focus-within:ring-blue-500 dark:text-slate-100 dark:placeholder:text-slate-500 dark:ring-2 dark:ring-slate-500 dark:focus:ring-blue-400 dark:bg-transparent"
          value={comment}
          onChange={handleChange}
          placeholder="نظرت رو برام بنویس ..."
        />
        <Button className="mt-4 mx-auto py-4 w-full sm:w-56">ارسال نظر</Button>
      </form>
    </>
  );
};

export default PostComments;
