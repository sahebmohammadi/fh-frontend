import Avatar from "@material-ui/core/Avatar";
import { useState } from "react";
import Button from "@/common/Button";
import createComment from "@/services/createPostCommentService";
import toJalaliDate from "../../../common/toJalaliDate";
import toast from "react-hot-toast";

const SingleComment = ({ comment, postId }) => {
  const [commentValue, setCommentValue] = useState("");
  const [openReply, setopenReply] = useState(false);

  const handleChange = (e) => {
    setCommentValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const data = {
      content: commentValue,
      postId,
      responseTo: comment._id,
    };

    createComment(data)
      .then(({ data }) => {
        toast.success(data.message);
        setCommentValue("");
        setopenReply(!openReply);
      })
      .catch((err) => {
        if (err.response?.data?.message) {
          toast.error(err.response.data.message);
        }
      });
  };

  return (
    <div className="border rounded-xl border-gray-200 dark:border-slate-600/50 dark:bg-slate-800 p-4 flex flex-col my-5">
      <div className="flex items-center justify-start">
        <Avatar
          alt={comment.writer?.name}
          src={
            comment.writer._id === "607ee3c86764a935ecb2f27c"
              ? "/images/saheb.png"
              : ""
          }
        />
        <div className="flex flex-col justify-between mr-4">
          <span className="block dark:text-slate-500">
            {comment.writer?.name}
            {comment.writer._id === "607ee3c86764a935ecb2f27c" && (
              <span className="text-xs text-gray-500 mr-1 dark:text-slate-600">
                (مدیر سایت)
              </span>
            )}
          </span>
          <span className="block text-xs text-gray-500 mt-2 dark:text-slate-500">
            {toJalaliDate(comment.createdAt)}
          </span>
        </div>
      </div>
      <div className="mt-4 dark:text-slate-300">{comment.content}</div>
      <div
        className="text-sm text-primary my-4 p-4 cursor-pointer"
        onClick={() => setopenReply(!openReply)}
      >
        {openReply ? "بیخیال" : "پاسخ به"}
      </div>

      {openReply && (
        <form onSubmit={handleSubmit}>
          <span className="text-xs text-gray-500 dark:text-slate-400">
            <span>در حال پاسخ به</span>
            <span> {comment.writer.name}</span>
          </span>
          <textarea
            className="focus:ring-primary p-4 rounded my-4 w-full border-none ring-2 ring-slate-300 shadow-sm focus:outline-none focus:ring-2 dark:focus-within:ring-blue-500 dark:text-slate-100 dark:placeholder:text-slate-500 dark:ring-2 dark:ring-slate-500 dark:focus:ring-blue-400 dark:bg-transparent"
            value={commentValue}
            onChange={handleChange}
            placeholder="نظرت رو برام بنویس ..."
          />
          <Button className="mt-4 mx-auto py-4 w-full sm:w-56">
            ارسال نظر
          </Button>
        </form>
      )}
    </div>
  );
};

export default SingleComment;
