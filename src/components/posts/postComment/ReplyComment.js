import SingleComment from "./SingleComment";
import React from "react";
const ReplyComment = ({ commentList, parentCommentId, postId }) => {
  let renderReplyComment = (parentCommentId) =>
    commentList.map((comment, index) => (
      <React.Fragment key={comment._id}>
        {comment.responseTo === parentCommentId && (
          <div className="mr-4 sm:mr-8">
            <SingleComment comment={comment} postId={postId} />
            <ReplyComment
              commentList={commentList}
              parentCommentId={comment._id}
              postId={postId}
            />
          </div>
        )}
      </React.Fragment>
    ));

  return <div>{renderReplyComment(parentCommentId)}</div>;
};

export default ReplyComment;
