import Head from "next/head";

const MetaTagSEO = ({ seo }) => {
  const {
    title,
    description,
    keywords,
    canonicalUrl,
    tiwtterCard,
    openGraph,
    children,
  } = seo;

  return (
    <Head>
      {/* Basic Meta tags */}
      <title>{title}</title>
      <meta
        name="theme-color"
        media="(prefers-color-scheme: dark)"
        content="#171a1c"
      />
      <meta name="description" content={description} />
      <meta
        name="viewport"
        content="minimum-scale=1, initial-scale=1, width=device-width"
      />
      <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta charSet="utf-8" />
      {/* <meta name="keywords" content={keywords} /> */}
      <meta name="author" content="saheb mohamadi | صاحب محمدی" />
      <link rel="shortcut icon" href="/fh-favicon.ico" />

      {/* Open graph meta tags */}
      <meta property="og:title" content={openGraph.title} />
      <meta property="og:description" content={openGraph.description} />
      <meta property="og:type" content={openGraph.type} />
      <meta property="og:url" content={canonicalUrl} />
      <meta property="og:locale" content="fa" />
      <meta property="og:site_name" content="وبسایت آموزشی فرانت هوکس" />
      <meta property="og:brand" content="فرانت هوکس" />
      <meta property="og:image" content={openGraph.image} />

      {/* Twitter meta tags */}
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content="@fronthooks" />
      <meta name="twitter:title" content={tiwtterCard.title} />
      <meta name="twitter:description" content={tiwtterCard.description} />
      <meta name="twitter:image" content={tiwtterCard.image} />

      {/* Canonical Link */}
      <link rel="canonical" href={canonicalUrl} />
      {children}
    </Head>
  );
};
export default MetaTagSEO;
