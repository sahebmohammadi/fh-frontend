import CircularProgress from "@material-ui/core/CircularProgress";
import { BsArrowRight } from "react-icons/bs";
export default function SingleFormLayout({
  children,
  title,
  loading,
  setStep,
  step = 0,
}) {
  return (
    <div className="sm:border rounded-lg border-gray-300 dark:border-slate-700/50 w-full sm:max-w-md sm:my-8 sm:m-auto md:my-4 flex flex-col">
      <div className="rounded-t-lg mb-4">
        <div className="px-8 flex items-center mt-4">
          {step > 1 && (
            <div
              className="text-gray-500 cursor-pointer py-4 dark:text-slate-400"
              onClick={() => setStep((s) => s - 1)}
            >
              <BsArrowRight size={25} className="dark:text-slate-500" />
            </div>
          )}
          <div className="text-2xl font-black  py-4 text-primary text-center flex-1">
            فرانت هوکس
          </div>
        </div>
        {step === 1 && (
          <div
            className={`my-4 px-8 font-bold text-lg text-gray-700 dark:text-white`}
          >
            {title}
          </div>
        )}
      </div>

      {!loading ? (
        <div className="py-4 px-8">{children}</div>
      ) : (
        <div className="py-4 px-8">
          <CircularProgress color="primary" />
        </div>
      )}
    </div>
  );
}
