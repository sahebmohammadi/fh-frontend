const Toast = ({ variant, children, className }) => {
  return (
    <p
      className={`p-4 my-2 rounded-lg 
    ${
      variant === "success"
        ? "bg-blue-700 text-blue-50"
        : "bg-rose-100 text-rose-700"
    } ${className}`}
    >
      {children}
    </p>
  );
};

export default Toast;
