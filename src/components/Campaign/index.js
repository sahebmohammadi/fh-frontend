import toArabicNumber from "@/utils/toArabicNumbers";
import Countdown from "react-countdown";
import { timeData } from "../CountDowntimer";
import Link from "next/link";

const renderer = (props) => {
  // Render a complete state
  if (props.completed) return null;

  return (
    <section
      //   style={{ backgroundColor: "rgba(45, 45, 45, 0.6)" }}
      className="rounded-2xl text-white px-8 py-8  my-4 
      from-violet-800 to-violet-200 bg-gradient-to-tl bg flex flex-col dark:text-slate-200 dark:from-slate-700 dark:to-slate-400"
    >
      <p className="text-center mb-4 font-bold">
        {new Date().getTime() < new Date("2023,Jan,26,13:00:00")
          ? "زمان شروع جشنواره تخفیف"
          : "زمان پایان جشنواره تخفیف"}
      </p>
      <div className="flex flex-row-reverse justify-between items-center">
        {timeData.map((item, index) => {
          return (
            <div
              className="flex flex-col justify-start text-center md:text-base"
              key={index}
            >
              <span>
                {index !== 3 && (
                  <span className="relative left-4 font-black text-yellow-300 inline-block">
                    :
                  </span>
                )}
                <span className="inline-block">
                  {toArabicNumber(props[item.value])}
                </span>
              </span>
              <span className="block">{item.label}</span>
            </div>
          );
        })}
      </div>
    </section>
  );
};

const Campaign = () => {
  return (
    <section className="mt-12 md:mt-28" id="campaign">
      <h2 className="text-center font-black text-primary text-xl md:text-3xl mb-4">
        جشنواره تخفیف ویژه دوره ها
      </h2>
      <div
        className="max-w-4xl mx-auto
       bg-gradient-to-tl bg-white p-4 md:py-8 md:px-12 dark:bg-slate-700 rounded-xl shadow-md"
      >
        <div className="flex flex-col space-y-4 md:flex-row md:justify-between md:items-center">
          <div className="md:w-1/2 flex items-center">
            <img
              className="rounded md:h-80"
              src="/images/bell.png"
              alt="جشنواره تخفیف دوره های فرانت هوکس"
            />
          </div>
          <div className="w-full md:max-w-xs md:mr-4 text-gray-700">
            <h3 className="font-black text-base mb-2 md:text-xl dark:text-slate-300">
              مسیر فرانت اند با فرانت هوکس
            </h3>
            <p className="leading-6 text-sm md:text-base md:leading-8 text-gray-500 dark:text-slate-400">
              تخفیف ویژه به همراه دوره هدیه و قرعه کشی برگشت وجه
            </p>
            <Link href="/lottery">
              <a className="mt-4 block text-sm md:text-base font-bold text-blue-600 dark:text-blue-500">
                مشاهده شرایط دوره هدیه و قرعه کشی؟
              </a>
            </Link>
            {
              <Countdown
                date={
                  new Date().getTime() < new Date("2023,Jan,26,13:00:00")
                    ? new Date("2023,Jan,26,13:00:00").getTime()
                    : new Date("2023,Jan,28,23:59:59").getTime()
                }
                renderer={renderer}
              />
            }
          </div>
        </div>
      </div>
    </section>
  );
};

export const DiscountBanner = (props) => {
  return (
    <aside className="sticky top-0 left-0 bg-gradient-to-tl bg-primary h-12 w-full z-20 flex items-center px-4 justify-between">
      <div className="flex-1 h-full text-white flex items-center justify-center cursor-pointer">
        <Link href="/#campaign">
          <a className="h-full w-full flex items-center justify-center font-bold md:text-lg">
            <span className="ml-3">
              <span className="flex h-3 w-3 relative">
                <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-purple-200 opacity-75"></span>
                <span className="relative inline-flex rounded-full h-3 w-3 bg-purple-200"></span>
              </span>
            </span>
            <span>آخرین تخفیف ویژه تابستان</span>
          </a>
        </Link>
      </div>
    </aside>
  );
};

export default Campaign;
