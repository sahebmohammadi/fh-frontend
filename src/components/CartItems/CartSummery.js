import { useSelector } from "react-redux";
import Button from "@/common/Button";
import MobileButtomCartSummery from "./MobileButtomCartSummery";
import Link from "next/link";
import toArabicNumber from "@/utils/toArabicNumbers";

const CartSummery = (props) => {
  const cart = useSelector((state) => state.cart);

  const originalTotalPrice = cart.addedItems.length
    ? cart.addedItems.reduce((acc, curr) => acc + curr.quantity * curr.price, 0)
    : 0;

  return (
    <div className="flex flex-col justify-start">
      <SummeryItem value={originalTotalPrice} label="قیمت کالاها" />
      <SummeryItem
        value={originalTotalPrice - cart.total}
        label="تخفیف کالاها"
      />
      <div className="border-b-2 border-gray-200"></div>
      <SummeryItem value={cart.total} label="جمع سبد خرید" total />
      <Link href="/auth?redirect=complete-profile">
        <a>
          <Button className="w-full">ادامه سفارش</Button>
        </a>
      </Link>
      <MobileButtomCartSummery />
    </div>
  );
};

export default CartSummery;

const SummeryItem = (props) => {
  return (
    <div
      className={`flex justify-between text-gray-500 items-center mb-2 ${
        props.total && "mt-4 mb-4 text-gray-900 font-bold"
      }`}
    >
      <p className="dark:text-slate-400">{props.label}</p>
      <p className="dark:text-slate-300">{toArabicNumber(props.value)}</p>
    </div>
  );
};
