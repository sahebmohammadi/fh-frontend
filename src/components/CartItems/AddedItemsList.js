import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { subtractQuantity } from "../../redux/cart/cartActions";
import { useSelector, useDispatch } from "react-redux";
import { BiTrash } from "react-icons/bi";
import CheckCircleOutlinedIcon from "@material-ui/icons/CheckCircleOutlined";
import toArabicNumbers from "@/utils/toArabicNumbers";
const SingleCartItem = (props) => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart);

  const subtractQuantityHandler = (product) => {
    dispatch(subtractQuantity(product));
  };

  return cart.addedItems.map((item) => {
    return (
      <section
        key={item._id}
        className="grid grid-colos-4 gap-4 md:gap-4 grid-rows-4 grid-flow-col mb-4 border-b-2 border-gray-200 pb-2"
      >
        <div className="row-span-3 md:row-span-4 md:row-end-5">
          <img
            src={item.image}
            className="object-cover object-center w-full h-28 md:object-contain md:h-52"
          />
        </div>
        <div className="row-span-1 md:row-start-4 md:col-span-3">
          <ButtonGroup
            size="small"
            aria-label="small outlined button group"
            color="primary"
          >
            <Button>{item.quantity}</Button>
            <Button
              onClick={() => subtractQuantityHandler(item)}
              disabled={!item.quantity}
            >
              {item.quantity === 1 ? <BiTrash /> : "-"}
            </Button>
          </ButtonGroup>
        </div>
        <div className="col-span-3 row-span-4 md:row-start-1 md:row-span-3 md:col-start-2 ">
          <section className="flex flex-col h-full">
            <p className="text-sm md:text-sm text-gray-700 dark:text-slate-300 mb-4">
              {item.name}
            </p>
            {item.description.map((desc, index) => (
              <div key={index} className="flex items-center justify-start mb-1">
                <CheckCircleOutlinedIcon
                  color="secondary"
                  fontSize="small"
                  className="dark:text-slate-400"
                />
                <p className="text-xs text-gray-500 dark:text-slate-400 mr-2">
                  {desc.support}
                </p>
              </div>
            ))}
            <p className="text-gray-700 text-sm md:text-lg dark:text-slate-300">
              {toArabicNumbers(item.offPrice)} تومان
            </p>
          </section>
        </div>
      </section>
    );
  });
};

export default SingleCartItem;
