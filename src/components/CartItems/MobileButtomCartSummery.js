import { useSelector } from "react-redux";
import Button from "@/common/Button";
import Link from "next/link";
import toArabicNumber from "@/utils/toArabicNumbers";

const MobileButtomCartSummery = () => {
  const cart = useSelector((state) => state.cart);

  return cart.addedItems.length ? (
    <div className="z-10 fixed flex justify-between items-center w-full dark:bg-slate-700 dark:border-t-slate-500 bottom-0 bg-white px-4 py-4 border-t-2 border-gray-200 left-0 sm:hidden">
      <Link href="/auth?redirect=complete-profile">
        <a>
          <Button className="w-full">ادامه سفارش </Button>
        </a>
      </Link>
      <div className="flex flex-col text-gray-500 dark:text-slate-200 text-xsm">
        <p>مبلغ قابل پرداخت</p>
        <p className="text-gray-900 font-bold dark:text-slate-300">
          {toArabicNumber(cart.total)} تومان
        </p>
      </div>
    </div>
  ) : null;
};

export default MobileButtomCartSummery;
