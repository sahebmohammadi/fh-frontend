import getProductDiscount from "@/services/getProductDiscountService";
import toArabicNumber from "@/utils/toArabicNumbers";
import { useEffect, useState } from "react";
import Countdown from "react-countdown";

export const timeData = [
  {
    label: "روز",
    value: "days",
  },
  {
    label: "ساعت",
    value: "hours",
  },
  {
    label: "دقیقه",
    value: "minutes",
  },
  {
    label: "ثانیه",
    value: "seconds",
  },
];

// Random component
const Completionist = () => <span>You are good to go!</span>;

// Renderer callback with condition
const renderer = (props) => {
  // Render a complete state
  if (props.completed) return null;

  return (
    <section
      //   style={{ backgroundColor: "rgba(45, 45, 45, 0.6)" }}
      className="rounded-2xl text-white px-8 py-8  my-4 bg-gray-600 flex flex-col"
    >
      <p className="text-center mb-4 font-bold">مهلت استفاده از تخفیف</p>
      <div className="flex flex-row-reverse justify-between items-center">
        {timeData.map((item, index) => {
          return (
            <div
              className="flex flex-col justify-start text-center"
              key={index}
            >
              <span>
                {index !== 3 && (
                  <span className="relative left-4 font-black text-yellow-300 inline-block">
                    :
                  </span>
                )}
                <span className="inline-block">
                  {toArabicNumber(props[item.value])}
                </span>
              </span>
              <span className="block">{item.label}</span>
            </div>
          );
        })}
      </div>
    </section>
  );
};

// const startTime = new Date("2021-05-02T19:45:00.000Z").getTime();
const CountDownTimer = ({ productId }) => {
  const [timerData, setTimerData] = useState(null);

  useEffect(() => {
    getProductDiscount(productId)
      .then((res) => setTimerData(res.data.discount))
      .catch((err) => console.log(err));
  }, []);

  if (!timerData) return null;

  return (
    timerData && (
      <Countdown
        date={
          new Date(timerData.startTime).getTime() +
          parseInt(timerData.duration) * 1000
        }
        renderer={renderer}
      />
    )
  );
};

export default CountDownTimer;
