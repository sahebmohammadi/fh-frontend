import { clearCart } from "@/redux/cart/cartActions";
import { useRouter } from "next/router";
import Router from "next/router";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
// import swal from "@sweetalert/with-react";
import Swal from "sweetalert2";
import queryString from "query-string";

const PaymentModal = () => {
  const router = useRouter();

  useEffect(() => {}, [router.query]);
  let query = "";

  for (const iterator in router.query) {
    query += iterator;
  }

  const { status, msg } = queryString.parse(query);

  const successAlert = () => {
    const paymentStatus = status === "OK" && msg === "Paid" ? true : false;
    const dispatch = useDispatch();
    const title = paymentStatus ? "پرداخت تایید شد" : "پرداخت ناموفق";
    const icon = paymentStatus ? "success" : "error";
    const text = paymentStatus
      ? " بابت اعتماد و همراهی شما متشکرم"
      : "مجددا اقدام کنید یا به پشتیبانی سایت اطلاع دهید";
    Swal.fire({
      title: title,
      text: text,
      icon: icon,
      confirmButtonText: "باشه",
      confirmButtonColor: "#733DD8",
    }).then(() => {
      dispatch(clearCart());
      if (typeof window !== "undefined") {
        window.history.replaceState(null, "", "/");
      }
      Router.reload("/");
    });
  };

  return (
    <section className="container max-w-lg">
      {status && msg && successAlert()}
    </section>
  );
};

export default PaymentModal;
