import CourseQuestions from "./CourseQuestions";
import CourseExplanation from "./CourseExplanation";
import CourseOutline from "./CourseOutline";
import CourseComments from "./CommentSection/CourseComments";
import { useEffect, useState } from "react";
import getProductComments from "@/services/getProductCommentService";
import getProductEpisodes from "@/services/getProductEpisodesService";

const CourseDetail = ({ product }) => {
  const [commentList, setCommentList] = useState([]);
  const [outlineList, setOutlineList] = useState([]);

  useEffect(() => {
    getProductComments(product._id)
      .then((res) => {
        setCommentList(res.data.comments);
      })
      .catch((err) => console.log(err?.response.data));

    getProductEpisodes(product._id)
      .then((res) => {
        setOutlineList(res.data);
      })
      .catch((err) => console.log(err?.response.data));
  }, [product._id]);

  return (
    <div className="mt-10">
      <h2 className="text-xl md:text-3xl font-extrabold mb-8">
        ویژگی های دوره
      </h2>
      <div className="space-y-20">
        <CourseExplanation product={product} />
        <CourseOutline outlineList={outlineList} />
        <CourseQuestions product={product} />
        <CourseComments
          product={product}
          setCommentList={setCommentList}
          commentList={commentList}
        />
      </div>
    </div>
  );
};

export default CourseDetail;
