import SingleComment from "./SingleComment";
import React from "react";
const ReplyComment = (props) => {
  let renderReplyComment = (parentCommentId) =>
    props.commentList.map((comment, index) => (
      <React.Fragment key={comment._id}>
        {comment.responseTo === parentCommentId && (
          <div className="mr-4 sm:mr-8">
            <SingleComment
              comment={comment}
              productId={props.productId}
              setCommentList={props.setCommentList}
            />
            <ReplyComment
              commentList={props.commentList}
              parentCommentId={comment._id}
              productId={props.productId}
              setCommentList={props.setCommentList}
            />
          </div>
        )}
      </React.Fragment>
    ));

  return <div>{renderReplyComment(props.parentCommentId)}</div>;
};

export default ReplyComment;
