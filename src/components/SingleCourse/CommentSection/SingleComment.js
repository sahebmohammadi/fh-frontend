import Avatar from "@material-ui/core/Avatar";
import { useState } from "react";
import { useSelector } from "react-redux";
import Button from "@/common/Button";
import Toast from "@/components/Toast";
import createComment from "@/services/createCommentService";
import toJalaliDate from "../../../common/toJalaliDate";

const SingleComment = ({ comment, productId, setCommentList }) => {
  const [message, setMessage] = useState(null);
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;
  const [commentValue, setCommentValue] = useState("");
  const [openReply, setopenReply] = useState(false);

  const handleChange = (e) => {
    setCommentValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!userInfo)
      return setMessage({
        variant: "error",
        text: "برای ارسال نظر، لاگین کنید",
      });

    const data = {
      content: commentValue,
      productId,
      responseTo: comment._id,
    };

    createComment(data, userInfo.token)
      .then((res) => {
        // console.log(res.data.message);
        setMessage({ variant: "success", text: res.data.message });
        setCommentValue("");
        setopenReply(!openReply);
        // setCommentList((prevstate) => {
        //   return [...prevstate, res.data.comments];
        // });
      })
      .catch((err) => {
        setMessage({ variant: "error", text: err.response?.data.message });
        // console.log(err.response?.data.message);
      });
  };

  return (
    <div className="border border-gray-300  rounded-xl p-4 flex flex-col my-4 dark:border-slate-600/50 dark:bg-slate-800">
      <div className="flex items-center justify-start">
        <Avatar
          alt={comment.writer?.name}
          src={
            comment.writer._id === "607ee3c86764a935ecb2f27c"
              ? "/images/saheb.png"
              : ""
          }
        />
        <div className="flex flex-col justify-between mr-4">
          <span className="block dark:text-slate-500">
            {comment.writer?.name}
            {comment.writer._id === "607ee3c86764a935ecb2f27c" && (
              <span className="text-xs text-gray-500 mr-1 dark:text-slate-600">
                (مدیر سایت)
              </span>
            )}
          </span>
          <span className="block text-xs text-gray-500 mt-2 dark:text-slate-500">
            {toJalaliDate(comment.createdAt)}
          </span>
        </div>
      </div>
      <div className="mt-4 md:text-base leading-6 md:leading-8 dark:text-slate-300">
        {comment.content}
      </div>
      <p
        className="text-sm text-primary my-4 p-4 cursor-pointer"
        onClick={() => setopenReply(!openReply)}
      >
        {openReply ? "بیخیال" : "پاسخ به"}
      </p>

      {openReply && (
        <form>
          <span className="text-xs text-gray-500 dark:text-slate-400">
            <span className="">در حال پاسخ به</span>
            <span> {comment.writer.name}</span>
          </span>
          <textarea
            className="focus:ring-primary p-4 rounded my-4 w-full border-none ring-2 ring-slate-300 shadow-sm focus:outline-none focus:ring-2 dark:focus-within:ring-blue-500 dark:text-slate-100 dark:placeholder:text-slate-500 dark:ring-2 dark:ring-slate-500 dark:focus:ring-blue-400 dark:bg-transparent"
            value={commentValue}
            onChange={handleChange}
            placeholder="نظرت رو برام بنویس ..."
          />
          {message && <Toast variant={message.variant}>{message.text}</Toast>}
          <Button
            onClick={handleSubmit}
            className="mt-4 mx-auto py-4 w-full sm:w-56"
          >
            ارسال نظر
          </Button>
        </form>
      )}
    </div>
  );
};

export default SingleComment;
