import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "@/common/Button";
import createComment from "@/services/createCommentService";
import Toast from "@/components/Toast";
import SingleComment from "./SingleComment";
import ReplyComment from "./ReplyComment";

const CourseComments = ({ product, setCommentList, commentList }) => {
  const [message, setMessage] = useState(null);
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const [comment, setComment] = useState("");

  const handleChange = (e) => {
    setComment(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!userInfo)
      return setMessage({
        variant: "error",
        text: "برای ارسال نظر، لاگین کنید",
      });

    const data = {
      content: comment,
      productId: product._id,
    };

    createComment(data)
      .then((res) => {
        // console.log(res.data.message);
        setMessage({ variant: "success", text: res.data.message });
        setComment("");
        // setCommentList((prevstate) => {
        //   return [...prevstate, res.data.comments];
        // });
      })
      .catch((err) => {
        setMessage({ variant: "error", text: err.response?.data.message });
        // console.log(err.response?.data.message);
      });
  };

  return (
    <section className="section-border">
      <h2 className="text-xl mt-8 mb-4  font-bold"> نظرات </h2>
      {/* Comment List */}

      {commentList &&
        commentList.map((comment, index) => {
          return (
            !comment.responseTo &&
            comment.status === 2 && (
              <React.Fragment key={comment._id}>
                <SingleComment
                  comment={comment}
                  productId={product._id}
                  setCommentList={setCommentList}
                />
                <ReplyComment
                  commentList={commentList}
                  productId={product._id}
                  parentCommentId={comment._id}
                  setCommentList={setCommentList}
                />
              </React.Fragment>
            )
          );
        })}

      {/* root Comment Form */}
      <form className="mt-8">
        <span className=" text-gray-500">ارسال دیدگاه جدید</span>
        <textarea
          className="focus:ring-primary p-4 rounded my-4 w-full border-none ring-2 ring-slate-300 shadow-sm focus:outline-none focus:ring-2 dark:focus-within:ring-blue-500 dark:text-slate-100 dark:placeholder:text-slate-500 dark:ring-2 dark:ring-slate-500 dark:focus:ring-blue-400 dark:bg-transparent"
          value={comment}
          onChange={handleChange}
          placeholder="نظرت رو برام بنویس ..."
        />
        {message && <Toast variant={message.variant}>{message.text}</Toast>}
        <Button
          onClick={handleSubmit}
          className="mt-4 mx-auto py-4 w-full sm:w-56"
        >
          ارسال نظر
        </Button>
      </form>
    </section>
  );
};

export default CourseComments;
