import { useSelector, useDispatch } from "react-redux";
import Button from "@/common/Button";
import { addToCart } from "@/redux/cart/cartActions";
import * as constants from "../../constants";
import useIsEnrolled from "@/hooks/useIsEnrolled";
import GoToLearn from "@/common/GoToLearn";
import toast from "react-hot-toast";

const AddToCartMobileSingleCourse = ({ product }) => {
  const isEnrolled = useIsEnrolled(product._id);

  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  const addToCartHandler = (item) => {
    dispatch(addToCart(item));
    toast.success(`${item.name} ${constants.addToCartMessage}`);
  };

  const commonClass = `z-10 fixed  justify-between items-center
   w-full bottom-0 bg-white dark:bg-slate-700 dark:border-t-slate-500 px-4 py-4 border-t-2 border-gray-200 left-0 sm:hidden right-0`;

  return isEnrolled ? (
    <div className={commonClass}>
      <GoToLearn />
    </div>
  ) : !cart.addedItems.length ? (
    <div className={commonClass}>
      <Button onClick={() => addToCartHandler(product)} className="w-full">
        ثبت نام دوره
      </Button>
    </div>
  ) : null;
};

export default AddToCartMobileSingleCourse;
