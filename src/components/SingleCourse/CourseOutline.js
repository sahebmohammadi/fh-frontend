import CloudDownloadOutlinedIcon from "@material-ui/icons/CloudDownloadOutlined";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import IconButton from "@material-ui/core/IconButton";
import downloadEpisode from "@/services/downloadEpisodeService";
import toArabicNumber from "@/utils/toArabicNumbers";
import { useEffect, useRef, useState } from "react";

import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { CircularProgress } from "@material-ui/core";

function getFileName(str) {
  return str.substring(str.lastIndexOf("/") + 1);
}

const CourseOutline = ({ outlineList }) => {
  const [expanded, setExpanded] = useState(false);
  const [seasonList, setSeasonList] = useState(null);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    const seasons = [];
    for (let i = 0; i < outlineList.length; i++) {
      if (outlineList[i].isSeasion) {
        seasons.push({ ...outlineList[i], index: i });
      }
    }
    const output = [];
    for (let i = 0; i < seasons.length; i++) {
      const slicedEpisodes = outlineList.slice(
        seasons[i].index,
        seasons[i + 1] ? seasons[i + 1].index : outlineList.length
      );
      output.push({ season: seasons[i], episods: slicedEpisodes });
    }
    setSeasonList(output);
  }, [outlineList]);

  const dlLinkRef = useRef();
  const downloadHandler = async ({ episodeId, type }) => {
    if (type === "cash") return;
    downloadEpisode(episodeId)
      .then((res) => {
        const dlLink = res.data;
        dlLinkRef.current.href = dlLink;
        dlLinkRef.current.setAttribute("download", "");
        dlLinkRef.current.click();
        dlLinkRef.current.href = "";
      })
      .catch((err) => console.log(err));
  };
  let i = -1;

  return (
    <section className="section-border">
      <a
        ref={dlLinkRef}
        href=""
        download=""
        className="hidden"
        target="_blank"
      ></a>
      <h2 className="text-xl mt-8 mb-4  font-bold"> سرفصل ها </h2>

      {seasonList ? (
        seasonList.map(({ season, episods }, index) => {
          return (
            <Accordion
              key={season.title}
              className="border-none border-gray-200 rounded-2xl outline-none dark:bg-slate-800 dark:border-slate-700"
              expanded={expanded === `panel${index + 1}`}
              onChange={handleChange(`panel${index + 1}`)}
            >
              <AccordionSummary
                expandIcon={<ExpandMoreIcon className="dark:text-slate-400" />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
              >
                <div className="font-bold text-gray-700 dark:text-slate-400 text-xs md:text-base rounded p-2 mb-1">
                  {season.title}
                </div>
              </AccordionSummary>
              <AccordionDetails className="border-t dark:border-slate-600">
                <div className="w-full">
                  {episods.map((episode) => {
                    if (!episode.isSeasion) i++;
                    return (
                      !episode.isSeasion && (
                        <section
                          key={episode.episodeId}
                          onClick={() => downloadHandler(episode)}
                        >
                          <DLLink {...episode} index={i} />
                        </section>
                      )
                    );
                  })}
                </div>
              </AccordionDetails>
            </Accordion>
          );
        })
      ) : (
        <div className="py-4 px-8">
          <CircularProgress color="primary" />
        </div>
      )}
    </section>
  );
};

export default CourseOutline;

const DLLink = (props) => {
  return (
    <div className="flex justify-between items-center mb-4 text-gray-700 dark:text-slate-300  rounded text-sm hover:bg-primary hover:text-white py-2">
      <div className="flex items-center">
        <span className="ml-4 p-2 bg-primary text-white rounded-full w-8 h-8 flex justify-center items-center">
          {toArabicNumber(props.index + 1)}
        </span>
        <p className="text-xs md:text-base"> {props.title}</p>
      </div>

      <div className="flex items-center">
        <div className="text-xs mx-2 sm:mx-4">
          {toArabicNumber(props.duration.split(" ")[0])} دقیقه
        </div>
        <div>
          <IconButton size="small" className="dark:text-purple-500">
            {props.type === "free" ? (
              <CloudDownloadOutlinedIcon />
            ) : (
              <LockOutlinedIcon />
            )}
          </IconButton>
        </div>
      </div>
    </div>
  );
};

const SeasionTitle = (props) => {
  return (
    <div className="flex items-center mb-4 bg-gray-100 text-gray-700  font-bold rounded text-sm py-2">
      <span className="ml-4 p-2 bg-gray-500 text-white rounded-full w-8 h-8 flex justify-center items-center">
        {/* {toArabicNumber(props.index + 1)} */}
      </span>
      <p> {props.title}</p>
    </div>
  );
};
