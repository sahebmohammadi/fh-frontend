import Button from "@/common/Button";
import MobileButtomCartSummery from "@/components/CartItems/MobileButtomCartSummery";
import Link from "next/link";
import { useDispatch } from "react-redux";
import { addToCart } from "@/redux/cart/cartActions";
import * as constants from "../../constants";
import AddToCartMobileSingleCourse from "./AddToCartMobileSingleCourse";
import CourseDetail from "./CourseDetail";
import { useEffect, useState } from "react";
import toArabicNumber from "@/utils/toArabicNumbers";
import CountDomwTimer from "@/components/CountDowntimer/index";
import { CircularProgress } from "@material-ui/core";
import useIsInCart from "@/hooks/useIsInCart";
import useIsEnrolled from "@/hooks/useIsEnrolled";
import GoToLearn from "@/common/GoToLearn";
import BackendBreadcrumbs from "@/common/BackendBreadCrumb";
import { BsClockHistory, BsCodeSlash } from "react-icons/bs";
import { AiOutlineUser } from "react-icons/ai";
import { MdSupportAgent } from "react-icons/md";
import { SiGotomeeting } from "react-icons/si";
import { BiSupport } from "react-icons/bi";
import toast from "react-hot-toast";

const SingleCourse = ({ product, breadCrumbs }) => {
  return (
    <>
      <BackendBreadcrumbs breadCrumbs={breadCrumbs} />
      <main
        className={`grid grid-rows-1 grid-cols-1 md:grid-cols-4 gap-x-8 gap-y-16 md:gap-y-24`}
      >
        <section className="col-span-4 md:col-span-3">
          <CourseIntro product={product} />
        </section>
        <section className={`col-span-4 md:col-span-1`}>
          <CourseRegister {...product} />
        </section>
        {["advanced-javascript", "responsive-web", "tailwindCSS"].includes(
          product.pageLink
        ) && (
          <section className="section-border col-span-4 md:col-span-4 dark:bg-slate-800 dark:border-slate-700/60 dark:border">
            <CourseProjects product={product} />
          </section>
        )}
        <section className={`row-strat-1 row-span-1 col-span-4`}>
          <CourseDetail product={product} />
        </section>
      </main>
    </>
  );
};

export default SingleCourse;

const CourseIntro = ({ product }) => {
  return (
    <div className="flex flex-col mb-4">
      <h1 className="text-lg sm:text-2xl mb-4 font-black text-gray-700 mx-auto dark:text-white">
        {product.name}
      </h1>
      <div className="r1_iframe_embed h-52 sm:h-88">
        <iframe
          title={product.name}
          src={product.introduction || ""}
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen={true}
          webkitallowfullscreen={1}
          mozallowfullscreen={1}
          className="sm:rounded-xl md:rounded-2xl"
        ></iframe>
      </div>
    </div>
  );
};

const CourseRegister = (product) => {
  const isInCart = useIsInCart(product._id);
  const isEnrolled = useIsEnrolled(product._id);
  const dispatch = useDispatch();

  const addToCartHandler = (item) => {
    dispatch(addToCart(item));
    toast.success(`${item.name} ${constants.addToCartMessage}`);
  };

  return (
    <div className="section-border md:sticky md:top-20 dark:bg-slate-800 dark:border-slate-700/50 dark:border">
      <div className="flex flex-col justify-between text-gray-700 dark:text-slate-400">
        <div className="flex items-center mb-4 text-base">
          <BsClockHistory className="text-primary ml-2" size={22} />
          <p className="">
            مدت : {toArabicNumber(product.duration.split(" ")[0])} ساعت
          </p>
        </div>
        <div className="text-gray-700 dark:text-slate-400 mb-4 flex items-center w-full text-base">
          <AiOutlineUser size={24} className="ml-2 text-primary" />
          <span className="ml-2"> تعداد دانشجو : </span>
          <span>{toArabicNumber(product.numsOfStudents)}</span>
        </div>
        {/* <div className="text-gray-700 dark:text-slate-400 mb-4 flex items-center w-full text-base">
          <BsCodeSlash size={24} className="ml-2 text-primary" />
          <span className="ml-2"> تعداد جلسات : </span>
          <span>
            {product.numsOfStudents ? (
              toArabicNumber(product.numsOfStudents)
            ) : (
              <CircularProgress color="primary" size={16} />
            )}
          </span>
        </div> */}
        <div className="text-gray-700 dark:text-slate-400 mb-4 flex items-center w-full text-base">
          <BiSupport size={24} className="ml-2 text-primary" />
          <span className="">
            پشتیبانی <strong className="dark:text-slate-300">نا محدود</strong> -{" "}
            <strong className="dark:text-slate-300">مربی محور</strong>
          </span>
        </div>
        <div className="text-gray-700 dark:text-slate-400 mb-4 flex items-center w-full text-base">
          <SiGotomeeting size={23} className="ml-2 text-primary" />
          <span className="">لایو های هفتگی با دانشجویان</span>
        </div>
        <div className="flex items-center my-5 justify-between">
          {product.discount !== 0 && (
            <div className="flex items-center mb-1">
              <div className="text-gray-500  ml-1 line-through text-xl dark:text-white">
                {toArabicNumber(product.price)}
              </div>
              <div className="bg-rose-500 rounded-full py-0.5 px-2 text-white text-xsm flex justify-center items-center">
                % {toArabicNumber(product.discount)}
              </div>
            </div>
          )}
          <div
            className={`font-bold ${
              product.discount ? "" : "text-center md:text-base flex-1"
            } `}
          >
            <span className="text-gray-700 ml-2 text-xl font-bold dark:text-white">
              {toArabicNumber(product.offPrice)}
            </span>
            <span>تومان </span>
          </div>
        </div>
        <CountDomwTimer productId={product._id} />

        <section>
          {isEnrolled ? (
            <GoToLearn className="" />
          ) : (
            <Link href="/auth?redirect=complete-profile">
              <a className="">
                {isInCart ? (
                  <Button className="w-full py-4">ادامه سفارش</Button>
                ) : (
                  <Button
                    className="w-full py-4"
                    onClick={() => addToCartHandler(product)}
                  >
                    ثبت نام دوره
                  </Button>
                )}
              </a>
            </Link>
          )}
        </section>
      </div>

      <AddToCartMobileSingleCourse product={product} />
      <MobileButtomCartSummery />
    </div>
  );
};

const projectList = [
  { title: "پروژه modal", link: "", type: "advanced-javascript", id: 1 },
  {
    title: "پروژه todo list",
    link: "https://todo-list-app-1oiyp0izv-sahebmohammadi.vercel.app/",
    type: "advanced-javascript",
    id: 2,
  },
  {
    title: "پروژه سبد خرید (Shopping Cart)",
    link: "https://shopping-cart-h5u7tvjyy-sahebmohammadi.vercel.app/",
    type: "advanced-javascript",
    id: 3,
  },
  {
    title: "پروژه سرچ و فیلتر روی محصولات فروشگاه",
    link: "https://filter-search-rp.vercel.app/",
    type: "advanced-javascript",
    id: 4,
  },
  {
    title: "پروژه اپلیکیشن تک صفحه ای (Single Page Application)",
    link: "",
    type: "advanced-javascript",
    id: 5,
  },
  {
    title: "پروژه آنلاین شبیه سایت تپسی + چند سکشن جدید تر",
    link: "https://tap30-demo.netlify.app/",
    type: "responsive-web",
    id: 6,
  },
  {
    title: "پروژه کاملا رسپانسیو فروشگاه آنلاین",
    link: "https://s6.uupload.ir/files/home_d7.png",
    type: "tailwindCSS",
    id: 6,
  },
  {
    title: "صفحه محصولات - حالت موبایل",
    link: "https://s6.uupload.ir/files/shop_72yg.png",
    type: "tailwindCSS",
    id: 7,
  },
  {
    title: "صفحه دسته بندی -  حالت موبایل",
    link: "https://s6.uupload.ir/files/category_1q0g.png",
    type: "tailwindCSS",
    id: 8,
  },
  {
    title: "صفحه سبد خرید - حالت دسکتاپ",
    link: "https://s6.uupload.ir/files/cart-1_tslo.png",
    type: "tailwindCSS",
    id: 9,
  },
  {
    title: "صفحه سبد خرید - حالت دسکتاپ",
    link: "https://s6.uupload.ir/files/cart_g24z.png",
    type: "tailwindCSS",
    id: 10,
  },
];

const CourseProjects = ({ product }) => {
  return (
    <div className="mb-4">
      <h2 className="text-lg sm:text-xl font-bold dark:text-slate-300 text-gray-700 mx-auto mb-4">
        مشاهده آنلاین پروژه های دوره
      </h2>
      {projectList.map((p, index) => {
        return (
          p.type === product.pageLink && (
            <div className="flex items-center mb-4" key={p.id}>
              <span className="ml-4 p-2 bg-gray-500 dark:bg-slate-600 text-white rounded-full w-6 h-6 flex justify-center items-center">
                {/* {toArabicNumber(index + 1)} */}
              </span>
              {p.link ? (
                <a href={p.link} target="_blank">
                  <h3 className="text-primary">{p.title}</h3>
                </a>
              ) : (
                <h3 className="text-primary">{p.title}</h3>
              )}
            </div>
          )
        );
      })}
    </div>
  );
};
