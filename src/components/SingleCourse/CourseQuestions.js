import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useState } from "react";

const CourseQuestions = ({ product }) => {
  const [expanded, setExpanded] = useState(false);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <section className="section-border mb-8">
      <h2 className="text-xl mt-8 mb-4  font-bold">سوالات متداول </h2>
      {product.questions.map((item, index) => {
        return (
          <Accordion
            key={item._id}
            className="border-none border-gray-200 rounded-2xl outline-none  dark:bg-slate-800 dark:border-slate-700"
            expanded={expanded === `panel${index + 1}`}
            onChange={handleChange(`panel${index + 1}`)}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon className="dark:text-slate-400" />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <p className="font-bold text-gray-700 dark:text-slate-400  md:text-base rounded mb-1">
                {item.question}
              </p>
            </AccordionSummary>
            <AccordionDetails className="border-t dark:border-slate-600">
              <p className="text-gray-500 font-bold text-sm dark:text-slate-300 mt-2 md:leading-10 md:text-base">
                {item.answer}
              </p>
            </AccordionDetails>
          </Accordion>
        );
      })}
    </section>
  );
};

export default CourseQuestions;
