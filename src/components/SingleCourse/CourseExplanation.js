import { Fragment } from "react";

const CourseExplanation = ({ product }) => {
  return (
    <section className="section-border dark:bg-slate-800 text-slate-300">
      <h2 className="text-2xl mb- font-bold">توضیحات </h2>
      {product.detail.map((item) => {
        return (
          <div key={item._id} className="mb-6 md:mb-8">
            <h3 className="mt-4 mb-2 font-bold text-gray-700 md:text-lg dark:text-slate-300">
              {item.label}
            </h3>
            <p className="text-gray-500 md:leading-10 leading-7 md:text-lg dark:text-slate-400">
              {item.text}
            </p>
          </div>
        );
      })}
    </section>
  );
};

export default CourseExplanation;
