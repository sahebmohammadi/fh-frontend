import Link from "next/link";
import { addToCart } from "@/redux/cart/cartActions";
import { useDispatch } from "react-redux";
import * as constants from "../../../constants";
import MobileButtomCartSummery from "@/components/CartItems/MobileButtomCartSummery";
import Button from "@/common/Button";
import toArabicNumber from "@/utils/toArabicNumbers";
import { BsClockHistory, BsArrowLeft } from "react-icons/bs";
import useIsEnrolled from "@/hooks/useIsEnrolled";
import useIsInCart from "@/hooks/useIsInCart";
import GoToLearn from "@/common/GoToLearn";
import toast from "react-hot-toast";
import Image from "next/image";

const Product = (props) => {
  return (
    <div className=" bg-white shadow-xl dark:shadow-none rounded-lg flex flex-col dark:bg-slate-700 overflow-hidden">
      <Link
        as={`/courses/${props.pageLink}`}
        href={{
          pathname: `/courses/${props.pageLink}`,
          query: { data: props._id },
        }}
      >
        <a>
          <div className="aspect-w-13 aspect-h-9 mb-4">
            {/* <img
              src={props.image}
              className="w-full h-full object-center object-cover"
              alt={props.name}
            /> */}
            <Image
              alt={props.name}
              src={props.image}
              layout="fill"
              className="object-cover object-center h-full w-full"
            />
          </div>
        </a>
      </Link>
      <section className="flex flex-col px-4">
        <Link
          as={`/courses/${props.pageLink}`}
          href={{
            pathname: `/courses/${props.pageLink}`,
            query: { data: props._id },
          }}
        >
          <a className="text-gray-700 text-lg sm:text-lg mb-8 block hover:text-blue-600 font-bold transition-all duration-500 ease-in-out dark:text-white dark:hover:text-blue-400">
            {props.name}
          </a>
        </Link>

        <div className="flex items-center text-gray-500 font-bold mb-4 dark:text-gray-300">
          <BsClockHistory
            className="text-primary ml-2 dark:text-blue-300"
            size={20}
          />
          <p>مدت : {toArabicNumber(props.duration.split(" ")[0])} ساعت</p>
        </div>
        <Link
          as={`/courses/${props.pageLink}`}
          href={{
            pathname: `/courses/${props.pageLink}`,
            query: { data: props._id },
          }}
        >
          <a>
            <div className="flex items-center text-primary font-bold mb-8 dark:text-blue-400">
              <p>مشاهده دوره</p>
              <BsArrowLeft
                className="text-primary dark:text-blue-400 mr-2 
                transform transition duration-500 hover:-translate-x-3 hover:scal-120"
                size={20}
              />
            </div>
          </a>
        </Link>
        <ProductButtomSection {...props} />
        <MobileButtomCartSummery />
      </section>
    </div>
  );
};

export default Product;

const ProductButtomSection = (props) => {
  const dispatch = useDispatch();
  const isEnrolled = useIsEnrolled(props._id);
  const isInCart = useIsInCart(props._id);

  const addToCartHandler = (product) => {
    dispatch(addToCart(product));
    toast.success(`${product.name} ${constants.addToCartMessage}`);
  };

  if (isEnrolled) return <GoToLearn className="mb-4" />;

  return (
    <div className="flex justify-between items-center mb-4">
      {isInCart ? (
        <Link href="/auth?redirect=complete-profile">
          <a>
            <Button>ادامه سفارش</Button>
          </a>
        </Link>
      ) : (
        <Button onClick={() => addToCartHandler(props)}>ثبت نام دوره</Button>
      )}

      <div className="flex flex-col justify-between">
        {props.discount !== 0 && (
          <div className="flex items-center mb-1">
            <div className="text-gray-500 text-sm ml-2 line-through dark:text-gray-100">
              {toArabicNumber(props.price)}
            </div>
            <div className="bg-rose-500 rounded-full py-0.5 px-2 text-white text-xsm flex justify-center items-center">
              % {toArabicNumber(props.discount)}
            </div>
          </div>
        )}
        <div className="font-bold ">
          <span className="text-gray-700 ml-3 dark:text-white">
            {toArabicNumber(props.offPrice)}
          </span>
          <span className="dark:text-gray-300">تومان </span>
        </div>
      </div>
    </div>
  );
};
