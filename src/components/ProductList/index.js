import Product from "./Product";

const ProductsList = ({ products }) => {
  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-12 sm:gap-16">
      {products
        .sort((a, b) => new Date(b.price) - new Date(a.price))
        .map((product, index) => {
          return (
            <Product key={product._id} {...product} productIndex={index} />
          );
        })}
    </div>
  );
};

export default ProductsList;
