import { useSelector } from "react-redux";

const CustomerInfo = ({ formik }) => {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;

  return (
    <section className="flex flex-col justify-start text-gray-500">
      <div className="text-gray-700 text-lg mb-4 dark:text-slate-200">
        جزییات صورت حساب
      </div>
      <CustomerInfoItem value={userInfo?.name} label="نام" />
      <CustomerInfoItem value={userInfo?.email} label="ایمیل" />
      <CustomerInfoItem value={userInfo?.phoneNumber} label="شماره موبایل" />
    </section>
  );
};

export default CustomerInfo;

const CustomerInfoItem = ({ label, value }) => {
  return (
    <div className={`flex items-center justify-between sm:justify-start mb-2`}>
      <p className="dark:text-slate-400">{label}</p>
      <p className="sm:mr-4 dark:text-slate-300">{value}</p>
    </div>
  );
};
