import { useSelector } from "react-redux";
import toArabicNumber from "@/utils/toArabicNumbers";

import CouponComponent from "./CouponComponent";
import { useState } from "react";

const CheckoutSummary = ({ onCoupon, coupon }) => {
  const cart = useSelector((state) => state.cart);
  const [isCoupon, setIsCoupon] = useState(false);
  const updatePrice = (coupon) => {
    if (coupon && coupon.discountType === "fixedCart") {
      return cart.total - coupon.amount;
    } else if (coupon && coupon.discountType === "percent")
      return cart.total * (1 - coupon.amount / 100);
    else return cart.total;
  };

  return (
    <div className="flex flex-col justify-start text-gray-500">
      <div className="mb-4 text-lg text-gray-700 dark:text-slate-200">
        سفارش شما
      </div>
      <SummaryItem
        value="جمع جزء"
        label="محصول"
        className="mb-4 border-b dark:border-b-slate-500 border-gray-200 "
      />
      {cart.addedItems.map((item) => (
        <SummaryItem
          key={item._id}
          value={`${item.offPrice * item.quantity} تومان`}
          label={`${item.name} * ${item.quantity}`}
        />
      ))}

      <p
        onClick={() => setIsCoupon(!isCoupon)}
        className="cursor-pointer text-sm text-primary mt-8"
      >
        {isCoupon ? "نه ندارم" : "   کد تخفیف دارید ؟"}
      </p>
      {isCoupon ? <CouponComponent onClick={onCoupon} /> : null}
      <SummaryItem
        value={`${updatePrice(coupon)} تومان`}
        // value={`${coupon ? cart.total - coupon.amount : cart.total} تومان`}
        label="مجموع"
        className="mt-8 mb-4 sm:mb-2 border-b border-gray-200 font-bold text-gray-700 dark:text-slate-200 dark:border-b-slate-500"
      />
    </div>
  );
};

export default CheckoutSummary;

const SummaryItem = ({ label, value, className }) => {
  return (
    <div className={`flex items-center justify-between mb-2 ${className}`}>
      <p className="dark:text-slate-400">{label}</p>
      <p className="dark:text-slate-300">{toArabicNumber(value)}</p>
    </div>
  );
};
