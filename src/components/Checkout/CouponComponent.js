import { useState } from "react";

const CouponComponent = ({ onClick }) => {
  const [coupon, setCoupon] = useState("");

  return (
    <div className="flex justify-between items-center mt-2">
      <input
        style={{ direction: "ltr" }}
        value={coupon}
        onChange={(e) => setCoupon(e.target.value)}
        className="border rounded-lg dark:bg-transparent dark:border-slate-500 dark:text-white px-4 py-2 border-gray-300 text-primary w-1/2 focus:border-none focus:ring-2 ring-primary"
      />
      <button
        onClick={() => onClick(coupon)}
        className="border border-primary rounded-lg p-2 text-primary hover:text-white hover:bg-primary"
      >
        اعمال تخفیف
      </button>
    </div>
  );
};

export default CouponComponent;
