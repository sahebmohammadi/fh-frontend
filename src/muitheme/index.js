import { createTheme } from "@material-ui/core/styles";

const theme = createTheme({
  overrides: {
    MuiAccordion: {
      root: {
        "&:before": {
          display: "none",
        },
        border: "solid #efefef 1px",
        marginBottom: "10px",
      },
      rounded: {
        borderRadius: "15px",
        "&:last-child": {
          borderRadius: "15px !important",
        },
      },
    },
  },
  direction: "rtl",
  palette: {
    type: "light",
    primary: {
      main: "#733DD8",
      // main: "#3f51b5",
    },
    secondary: {
      main: "#979797",
    },
    error: {
      main: "#f44336",
    },
  },
  shadows: ["none"],
  typography: {
    fontFamily: [
      "iranyekan",
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
    ].join(","),
  },
});

export default theme;
