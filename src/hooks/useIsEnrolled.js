import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const useIsEnrolled = (courseId) => {
  const [isEnrolled, setIsEnrolled] = useState(false);
  const { userInfo } = useSelector((state) => state.userSignin);

  function checkIdEnrolled(id) {
    if (userInfo && userInfo?.enrolledCourses) {
      setIsEnrolled(userInfo.enrolledCourses.some((c) => c == id));
    } else {
      setIsEnrolled(false);
    }
  }

  useEffect(() => {
    checkIdEnrolled(courseId);
  }, [courseId, userInfo]);

  return isEnrolled;
};

export default useIsEnrolled;
