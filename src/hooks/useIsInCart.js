import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const useIsInCart = (courseId) => {
  const [isInCart, setIsInCart] = useState(false);
  const cart = useSelector((state) => state.cart);

  function checkCart(id) {
    setIsInCart(cart.addedItems.some((item) => item._id == id));
  }

  useEffect(() => {
    checkCart(courseId);
  }, [cart]);

  return isInCart;
};

export default useIsInCart;
