import Layout from "@/containers/Layout";
import CheckoutSummary from "@/components/Checkout/CheckoutSummary";
import Link from "next/link";
import CustomerInfo from "@/components/Checkout/CustomerInfo";
import Button from "@/common/Button";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import Toast from "@/components/Toast";
import { createOrder } from "../../redux/order/orderActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import checkCoupon from "@/services/checkCouponService";
import StepperComponent from "@/common/StepperComponent";
import Head from "next/head";
import toast from "react-hot-toast";

const Checkout = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const cart = useSelector((state) => state.cart);
  const { userInfo } = useSelector((state) => state.userSignin);
  const [coupon, setCoupon] = useState(null);

  useEffect(() => {
    if (!userInfo) router.push("/auth");
  }, [userInfo]);

  const handleCoupon = (_coupon) => {
    // console.log(cart.addedItems);
    checkCoupon(_coupon, cart.addedItems)
      .then((res) => {
        toast.success("تخفیف با موفقیت اعمال شد");
        setCoupon(res.data.coupon);
      })
      .catch((err) => {
        if (err?.response?.data?.message) {
          toast.error(err.response.data.message);
        }
      });
  };

  const orderCreate = useSelector((state) => state.orderCreate);
  const { loading, success, error, order, data: orderResponse } = orderCreate;
  const { code, status, msg } = orderResponse || {};

  const orderHandler = () => {
    const orderItems = cart.addedItems.map((item) => ({
      name: item.name,
      quantity: item.quantity,
      product: item._id,
      price: item.offPrice,
      image: item.image,
    }));

    dispatch(
      createOrder({
        orderItems,
        paymentMethod: "Zarinpal",
        coupon: coupon,
      })
    );
  };

  // redirect :
  useEffect(() => {
    if (code && code === "FREE" && status === "OK")
      return router.push("https://fronthooks.ir?status%3DOK%26msg%3DPaid");
    if (code && code !== "FREE")
      return router.push(`https://www.zarinpal.com/pg/StartPay/${code}`);
  }, [orderResponse, error, success, loading]);

  if (cart.addedItems.length === 0)
    return (
      <Layout>
        <Head>
          <title> ثبت سفارش | فرانت هوکس</title>
        </Head>
        <Toast>متاسفانه سبد خرید شما خالیه</Toast>
        <Link href="/courses">
          <a>
            <div className="mt-4">
              <Toast variant="success">میخوام برم به صفحه دوره ها</Toast>
            </div>
          </a>
        </Link>
      </Layout>
    );

  return (
    <Layout>
      <Head>
        <title> ثبت سفارس | فرانت هوکس</title>
      </Head>
      <StepperComponent activeStep={2} />
      <main className={`grid grid-rows-2 grid-cols-4 sm:grid-cols-6 gap-6`}>
        <section
          className={`section-border dark:bg-slate-800 dark:border dark:border-slate-700/60 col-span-4  md:col-span-4`}
        >
          <CustomerInfo />
        </section>
        <section
          className={`section-border dark:bg-slate-800 dark:border dark:border-slate-700/60 col-span-4 md:col-span-2`}
        >
          <CheckoutSummary onCoupon={handleCoupon} coupon={coupon} />
        </section>
        <section
          className={`section-border dark:bg-slate-800 dark:border dark:border-slate-700/60 row-span-1 col-span-4 md:col-span-6`}
        >
          <p className="text-primary font-bold text-xl my-4">
            {code ? "در حال انتقال به درگاه پرداخت" : "پرداخت امن زرین پال "}
          </p>
          {code ? (
            <div className="py-4 px-8">
              <CircularProgress color="primary" />
            </div>
          ) : (
            <Button className="w-full py-4 text-xl" onClick={orderHandler}>
              ثبت سفارش
            </Button>
          )}
        </section>
      </main>
    </Layout>
  );
};

export default Checkout;
