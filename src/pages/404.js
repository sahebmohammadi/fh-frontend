import Link from "next/link";
import Head from "next/head";
import Layout from "@/containers/Layout";
import { BsArrowLeft } from "react-icons/bs";
const NotfountPage = () => {
  return (
    <Layout>
      <Head>
        <title>صفحه 404 | فرانت هوکس </title>
      </Head>
      <main className="text-center">
        <h2 className="text-red-400 font-bold text-lg">
          صفحه ای که دنبالش بودید، پیدا نشد !
        </h2>
        <h2 className="text-primary font-extrabold text-lg mt-8 text-center">
          <Link href="/">
            <a className="flex justify-center items-center">
              <span> بریم به صفحه اصلی ؟</span>
              <BsArrowLeft
                className="text-primary mr-2 
                transform transition duration-500 hover:-translate-x-3 hover:scal-120"
                size={20}
              />
            </a>
          </Link>
        </h2>
      </main>
    </Layout>
  );
};

export default NotfountPage;
