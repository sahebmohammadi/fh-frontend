import BackendBreadcrumbs from "@/common/BackendBreadCrumb";
import MetaTagSEO from "@/components/MetaComponent";
import Layout from "@/containers/Layout";
import getPageMetaData from "@/services/getPageMetaDataService";
import { useState, useEffect } from "react";
import Link from "next/link";
import getPostCategories from "@/services/getPostCategoriesService";
import { useRouter } from "next/router";
import queryString from "query-string";
import http from "@/services/htppService";
import FetchLoading from "@/common/FetchLoading";
import BasicPagination from "@/common/Pagination";
import PostList from "@/components/posts/PostList";
import { AdjustmentsIcon } from "@heroicons/react/outline";
import { SortDescendingIcon } from "@heroicons/react/outline";
import BottomSheet from "@/common/BottomSheet";
import usePush from "@/hooks/usePush";

// export const getStaticProps = async () => {
//   const {
//     data: { metaData },
//   } = await getPageMetaData("/blogs");

//   const {
//     data: { data: postCategories },
//   } = await getPostCategories();

//   const {
//     data: { data: posts },
//   } = await getPosts();

//   return {
//     props: { metaData, postCategories, posts },
//   };
// };

export async function getServerSideProps(context) {
  const { params, query, req } = context;
  const {
    data: { metaData },
  } = await getPageMetaData("/blogs");
  const {
    data: { data: postCategories },
  } = await getPostCategories();

  const {
    data: { data: posts },
  } = await http.get(`/posts?${queryString.stringify(query)}`, {
    withCredentials: true,
    headers: {
      Cookie: req.headers.cookie || "cookie1=value1",
    },
  });

  return {
    props: { posts, metaData, postCategories },
  };
}

const sortsOptions = [
  { label: "جدید ترین", id: "newest" },
  { label: "پر بازدید ترین", id: "most" },
  { label: "محبوب ترین", id: "popular" },
];

const Blogs = ({ metaData, postCategories, posts }) => {
  const router = useRouter();
  const [sort, setSort] = useState(router.query.sort || "newest");
  const [expanded, setExpanded] = useState(true);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isOpenSheet, setIsOpenSheet] = useState(false);

  const sortHandler = async (id) => {
    setIsRefreshing(true);
    setSort(id);
    delete router.query.page;
    router.query.sort = id;
    usePush(router);
  };

  useEffect(() => {
    setIsRefreshing(false);
  }, [posts]);

  const pageHandler = (e, page) => {
    setIsRefreshing(true);
    router.query.page = page;
    usePush(router);
  };

  return (
    <Layout>
      <MetaTagSEO seo={metaData} />
      <BackendBreadcrumbs breadCrumbs={metaData.breadcrumb} />
      <main>
        <div className="grid md:grid-cols-12 gap-y-4 md:gap-y-12 md:gap-x-8">
          {/* ctegory desktop */}
          <div className="md:col-span-3 hidden md:block row-span-3">
            <div className="bg-white dark:bg-slate-800 dark:text-slate-400 overflow-hidden text-myGray-500 rounded-[32px] sticky top-20">
              <div
                onClick={() => setExpanded(!expanded)}
                className="bg-purple-100 dark:bg-slate-700 cursor-pointer p-6 flex items-center justify-between"
              >
                <span className="text-lg">دسته بندی مقالات</span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className={`h-5 w-5 transition duration-500 ${
                    expanded ? "rotate-180 " : ""
                  }`}
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth="2"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19 9l-7 7-7-7"
                  />
                </svg>
              </div>
              {expanded && (
                <ul className="py-6">
                  <li key="all-posts">
                    <Link href="/blogs">
                      <a
                        className={`py-2 transition duration-300 px-6 w-full mb-1 block hover:bg-blue-50 hover:text-blue-700 dark:hover:bg-slate-700 dark:hover:text-sky-500 text-base ${
                          !router.query.categorySlug && "bg-primary text-white"
                        }`}
                      >
                        همه پست ها
                      </a>
                    </Link>
                  </li>
                  {postCategories.map((category) => {
                    return (
                      <li key={category._id}>
                        <Link href={`/blogs/${category.englishTitle}`}>
                          <a
                            className={`py-2 transition duration-300 px-6 w-full mb-1 block hover:bg-blue-50 hover:text-blue-700 text-base dark:hover:bg-slate-700 dark:hover:text-sky-500 ${
                              router.query.categorySlug ===
                                category.englishTitle && "bg-primary text-white"
                            }`}
                          >
                            {category.title}
                          </a>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              )}
            </div>
          </div>
          {/* category mobile */}
          <div className="overflow-auto block md:hidden">
            <ul className="flex items-center gap-x-3 overflow-x-auto w-full pb-4 text-myGray-dark">
              <li className="shrink-0">
                <Link href={`/blogs`}>
                  <a
                    className={`block rounded-full border border-myGray-300 text-myGray-dark px-4 py-2 bg-white shrink-0 dark:bg-slate-700 dark:text-slate-400 dark:border-none ${
                      !router.query.categorySlug &&
                      " text-primary border-purple-500 bg-purple-50 dark:bg-slate-400 dark:text-slate-700"
                    }`}
                  >
                    همه پست ها
                  </a>
                </Link>
              </li>
              {postCategories.map((category) => {
                return (
                  <li className="shrink-0" key={category._id}>
                    <Link href={`/blogs/${category.englishTitle}`}>
                      <a
                        className={`block rounded-full border border-myGray-300 text-myGray-dark px-4 py-2 bg-white dark:bg-slate-700 dark:text-slate-400 dark:border-none shrink-0 ${
                          router.query.categorySlug === category.englishTitle &&
                          "text-primary border-purple-500 bg-purple-50 dark:bg-slate-400 dark:text-slate-700"
                        }`}
                      >
                        {category.title}
                      </a>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </div>
          {/*  mobile filter */}
          <div className="flex gap-x-4 text-myGray-400 md:hidden">
            <div
              className="flex py-2 justify-center gap-x-4 items-center border border-myGray-400 flex-1 rounded-2xl cursor-pointer dark:border-slate-500 dark:text-slate-500"
              onClick={() => setIsOpenSheet(!isOpenSheet)}
            >
              <AdjustmentsIcon className="dark:text-slate-500 w-5 h-5 text-myGray-400" />
              <span>فیلتر</span>
            </div>
            <div
              className="flex py-2 justify-center gap-x-4 items-center border border-myGray-400 flex-1 rounded-2xl dark:border-slate-500 dark:text-slate-500"
              onClick={() => setIsOpenSheet(!isOpenSheet)}
            >
              <SortDescendingIcon className="dark:text-slate-500 w-5 h-5 text-myGray-400" />
              <span>مرتب سازی</span>
            </div>
          </div>
          {/* desktop sort */}
          <div className="md:col-span-9 hidden md:block">
            <div className="bg-white rounded-3xl dark:bg-slate-800 dark:text-slate-400 px-8 py-2 flex items-center text-myGray-dark">
              <span className="ml-3 flex items-center">
                <img src="/images/filterIcon.svg" alt="" />
                <span className="font-light mr-3 text-myGray-400 dark:text-slate-400">
                  مرتب سازی:
                </span>
              </span>
              <ul className="flex items-center gap-x-4 text-myGray-500 dark:text-slate-400">
                {sortsOptions.map(({ id, label }) => {
                  return (
                    <li
                      key={id}
                      onClick={() => sortHandler(id)}
                      className={`relative block py-3 px-3 cursor-pointer ${
                        sort === id ? `text-primary font-bold` : ``
                      }`}
                    >
                      <span> {label}</span>
                      {sort === id && (
                        <div className="absolute -bottom-2 w-9 bg-primary h-[3px] rounded-2xl"></div>
                      )}
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
          {/* blogs */}
          <div
            className="md:col-span-9 grid grid-cols-6 gap-y-8 gap-x-8 relative"
            // style={{ filter: isRefreshing ? "blur(3px)" : "" }}
          >
            {isRefreshing ? (
              <FetchLoading isRefreshing={isRefreshing} />
            ) : (
              <PostList posts={posts} />
            )}
          </div>
          <div className="flex justify-center md:col-span-9">
            <BasicPagination
              totalPages={posts.totalPages}
              pageHandler={pageHandler}
              currentPage={posts.page}
            />
          </div>
        </div>
        <BottomSheet
          isOpenSheet={isOpenSheet}
          setIsOpenSheet={setIsOpenSheet}
          title="فیلتر ها"
        >
          <div>
            <ul
              className={`flex flex-col items-center gap-x-4 text-myGray-500 dark:text-slate-400`}
            >
              {sortsOptions.map(({ id, label }) => {
                return (
                  <li
                    key={id}
                    onClick={() => {
                      sortHandler(id);
                      setIsOpenSheet(false);
                    }}
                    className={`w-full mb-2 rounded overflow-hidden text-center relative block py-2 px-3 cursor-pointer ${
                      sort === id
                        ? `text-primary font-bold border-purple-600 border-r-4`
                        : ``
                    }`}
                  >
                    <span> {label}</span>
                  </li>
                );
              })}
            </ul>
          </div>
        </BottomSheet>
      </main>
    </Layout>
  );
};

export default Blogs;
