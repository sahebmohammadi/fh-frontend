import Button from "@/common/Button";
import Layout from "@/containers/Layout";
import Head from "next/head";
import Link from "next/link";

const Lottery = () => {
  return (
    <Layout>
      <Head>
        <title>شرایط قرعه کشی برگشت وجه</title>
      </Head>
      <main className="mx-auto container max-w-4xl mt-4">
        <h1 className="font-bold text-xl dark:text-slate-300 md:text-2xl  mb-8">
          شرایط دوره هدیه و قرعه کشی برگشت وجه
        </h1>
        <section className="md:text-lg">
          <h2 className="font-bold mb-4 text-lg text-primary list-disc">
            دوره هدیه در مسیر فریلنسری
          </h2>
          <p className="md:leading-10 mb-10">
            خیلیا بعد از مدت ها دوست دارن که به صورت فریلنسری کار بکنند.
            فریلنسری یک سبک زندگی هست و چالش های خاص خودش رو داره. از قرار
            دادنویسی، قیمت گذاری پروژه ها، مذاکره با مشتری، شبکه سازی، مدیریت
            زمان و کار، استفاده از لینکدین و... از جمله چالش های یک فریلنسر هست.
            این دوره به ارزش 329 هزار تومان آماده شد تا تجربه چند ساله من را در
            این مسیر داشته باشید. این دوره فقط به کسایی که
            <strong className="text-primary">
              {" "}
              هر در و دو دوره ریکت و جاوااسکریپت{" "}
            </strong>
            ثبت نام می کنند و همچنین دانشجوهای قبلی به عنوان هدیه تقدیم می شود.
          </p>
          <h2 className="font-bold text-lg mb-4 text-primary">
            برگشت کل هزینه به 5 نفر از دانشجویان!
          </h2>
          <div className="leading-7 text-gray-700 dark:text-slate-400">
            <p className="md:leading-10">
              کل هزینه دوره ها به برندگان این قرعه کشی برگشت داده میشه. قرعه کشی
              بین افرادی برگزار میشه که در
              <span className="text-primary font-bold"> 24 ساعت اول </span>
              در دوره ها ثبت نام می کنند.
            </p>
            <br />
            <p className="md:leading-10">
              تخفیف ویژه اردیبهشت ماه ماه به مدت
              <span className="text-red-600 font-bold"> 48 ساعت </span>
              اعمال می شود. از بین کسایی که در
              <span className="text-primary font-bold"> 24 ساعت اول </span> ثبت
              نام می کنند. به قید قرعه
              <span className="text-green-400 font-bold"> 5 نفر </span>
              انتخاب می شوند و کل هزینه ای که بابت ثبت نام در دوره ها پرداخت
              کرده اند برگشت داده میشه. اما نکته مهم اینجاست که شانس همه یکسان
              نیست و شما می توانید امتیاز یا شانس بیشتری را کسب کنید. مثلا کسی
              که 4 امتیاز دارد، 4 دفعه اسمش در قرعه کشی شرکت داده می شود.
            </p>
            <p className="mt-4">نحوه امتیاز دهی به صورت زیر است :</p>
            <ul className="flex flex-col space-y-1 md:space-y-3">
              <li className="mt-2">
                {" "}
                ثبت نام در دوره{" "}
                <span className="text-primary font-bold">
                  {" "}
                  جاوااسکریپت{" "}
                </span>: <span className="text-primary font-bold"> 1 </span>
                امتیاز
              </li>
              <li>
                {" "}
                ثبت نام در دوره{" "}
                <span className="text-primary font-bold"> ریکت </span> :{" "}
                <span className="text-primary font-bold"> 2 </span>
                امتیاز
              </li>
              <li>
                ثبت نام در هر
                <span className="text-primary font-bold">
                  {" "}
                  دو دوره ریکت و جاوااسکریپت{" "}
                </span>
                : <span className="text-primary font-bold"> 4 </span> امتیاز
              </li>
            </ul>
          </div>
          <p className="mt-8 text-xs leading-6 text-gray-500 md:text-base dark:text-slate-300">
            اگه هنوز برات سواله که چه دوره ای برات مفیده؟
            <Link href="/learning-path">
              <a className="text-blue-700 dark:text-blue-400">
                {" "}
                حتما مقاله نقشه راه فرانت اند رو بخون{" "}
              </a>
            </Link>
          </p>
        </section>

        <div className="text-center w-full my-8">
          <Button className="w-full md:max-w-lg">
            <Link href="/courses">
              <a className="block py-1 md:text-base">رفتن به صفحه دوره ها</a>
            </Link>
          </Button>
        </div>
      </main>
    </Layout>
  );
};

export default Lottery;
