import ProductsList from "@/components/ProductList";
import getAllProducts from "@/services/getAllProducts";
import Button from "@/common/Button";
import toArabicNumber from "@/utils/toArabicNumbers";
import PaymentModal from "@/components/PaymentModal/index";
import Layout from "@/containers/Layout";
import Link from "next/link";
import getPageMetaData from "@/services/getPageMetaDataService";
import MetaTagSEO from "@/components/MetaComponent";
import Image from "next/image";
export const getStaticProps = async () => {
  const { data: products } = await getAllProducts();
  const {
    data: { metaData },
  } = await getPageMetaData("/");
  return {
    props: { products, metaData },
    revalidate: 120,
  };
};

function HomePage({ darkMode, children, products, metaData }) {
  return (
    <Layout>
      <MetaTagSEO seo={metaData} />
      <PaymentModal />
      <HomeBanner />
      <CoursesDescription />
      <main className="py-8 sm:py-16">
        <div
          className="flex-1 container md:max-w-screen-xl mx-auto"
          id="courses"
        >
          <div className="justify-center flex">
            <span className=" mb-6 text-xl md:text-2xl relative font-black sm:mb-10">
              <div className="z-10 relative dark:text-gray-200 text-center">
                دوره های برنامه نویسی آکادمی <br className="block md:hidden" />{" "}
                فرانت هوکس
              </div>
              <span className="w-full h-4 dark:bg-sky-500 bg-purple-200 absolute -bottom-1 left-0"></span>
            </span>
          </div>
          <ProductsList products={products} />
        </div>
        <Introduction />
        <Statistics />
      </main>
    </Layout>
  );
}

export default HomePage;

function HomeBanner() {
  return (
    <section className="flex-col md:flex-row flex md:justify-around md:items-center">
      {/* <div
        className="h-48  sm:h-120 bg-local bg-no-repeat bg-cover w-full md:w-1/2"
        style={{
          backgroundImage: "url(/images/banner.png)",
          backgroundSize: "cover",
          backgroundPosition: "center top",
        }}
      ></div> */}
      <div className="md:aspect-w-8 aspect-w-6 aspect-h-4 md:aspect-h-3 w-full md:w-1/2">
        <Image
          alt="وبسایت آموزش برنامه نویسی فرانت هوکس"
          src="/images/banner.png"
          layout="fill"
          className="object-contain object-center w-full"
        />
      </div>
      <div className="flex flex-col items-center mt-4 pb-4">
        {/* <p className=" hidden sm:block">logo</p> */}
        <h1 className="font-black text-xs mt-4 sm:text-2xl dark:text-white">
          دوره های آموزشی آکادمی فرانت هوکس
        </h1>

        <p className="text-xs sm:text-lg my-4 sm:my-8 dark:text-gray-200">
          برنامه نویسی را سریع، آسان و پروژه محور یاد بگیرید
        </p>
        <a href="#courses">
          <Button className="sm:text-lg relative">
            <span>مشاهده دوره ها</span>
          </Button>
        </a>
        <p className="mt-8 text-xs leading-6 text-gray-500 md:text-base dark:text-gray-300">
          اگه هنوز برات سواله که چه دوره ای برات مفیده؟
          <br />
          <Link href="/learning-path">
            <a className="flex items-center w-full justify-center text-blue-700 dark:text-sky-400 font-bold mt-2 md:mt-4 relative">
              <span className="ml-3">
                <span className="flex h-3 w-3 relative">
                  <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-primary opacity-75"></span>
                  <span className="relative inline-flex rounded-full h-3 w-3 bg-primary"></span>
                </span>
              </span>
              <span> حتما مقاله نقشه راه فرانت اند رو بخون</span>
            </a>
          </Link>
        </p>
      </div>
    </section>
  );
}

const waranties = [
  {
    icon: "practical",
    title: "کاربردی و پروژه محور",
    desc: " با کار روی پروژه های واقعی، بازار رو از نزدیک لمس کنید",
  },
  {
    icon: "time",
    title: "ویدئو های کوتاه و با کیفیت",
    desc: "هر مبحث در قالب یک جلسه کوتاه و کاربردی آماده شده است",
  },
  {
    icon: "mentor",
    title: "همراهی مربی",
    desc: "با مربی‌های حرفه‌ای و با حوصله رفع اشکال می‌کنید",
  },
  {
    icon: "quality",
    title: "تضمین کیفیت",
    desc:
      "بهترین پشتیبانی و به روز ترین سطح آموزش موجود در ایران رو دریافت می‌کنید",
  },
  {
    icon: "refund",
    title: "تضمین بازگشت وجه",
    desc: "اگه راضی نبودید تا ۱۵ روز فرصت دارید انصراف بدید",
  },
];

function CoursesDescription() {
  return (
    <section
      className="mt-10 mb-6 sm:my-20 grid grid-cols-10 gap-8 
    container md:max-w-screen-xl mx-auto"
    >
      {waranties.map((item, index) => {
        return (
          <section
            key={index}
            className={`bg-white rounded-xl flex flex-col 
                       items-center justify-center col-span-5 p-4 sm:p-8 sm:col-span-3 md:col-span-2 dark:bg-slate-800 
                       ${index === 4 && "col-span-10 sm:col-span-2"}`}
          >
            <div
              className={`mb-4 w-7 sm:w-10 
            ${index === 4 && "w-10"}`}
            >
              <img src={`/images/${item.icon}.svg`} alt="icon"></img>
            </div>
            <p className="mb-4 font-bold text-xs sm:text-sm text-center dark:text-gray-100">
              {item.title}
            </p>
            <p className="text-center text-xs dark:text-gray-300">
              {item.desc}
            </p>
          </section>
        );
      })}
    </section>
  );
}

function Introduction() {
  return (
    <section
      className="flex-col md:flex-row-reverse flex md:justify-between md:items-center
     mt-24 sm:mt-32 container md:max-w-screen-xl mx-auto p-4"
    >
      <div className="flex flex-col items-start pb-4 w-full sm:w-2/3">
        <h2 className="font-extrabold text-lg sm:text-2xl">
          فرانت اند (front-end) چیه ؟
        </h2>

        <p className="text-xs sm:text-sm my-4 sm:my-8 leading-7 sm:leading-8 text-gray-700 dark:text-slate-400">
          بذار خیلی خودمونی و به دور از تعاریف کلیشه ای بهت بگم که فرانت اند
          (front-end) چیه ؟ کیه و چیکار میکنه ؟ هر وبسایت و یا اپلیکیشنی دو بخش
          اساسی داره. یکی سمت سرور که کاربر نمیبینه و یکی سمت کاربر که میشه همون
          ظاهر سایت. دقیقا درست حدس زدی به چیزایی که ما مستقیما باهاش کار میکنیم
          و میبینم میگن فرانت سایت. مثل هدر، فوتر، بخش اصلی محتوای سایت، فرم های
          ثبت نام و.. که پیاده کردن همه اینا به عهده توسعه دهنده فرانت
          (front-end developer) هست. با اومدن فریمورک های جذاب جاوااسکریپت مثل
          vue.js و کتابخونه ریکت (react.js) وظیفه فرانت خیلی سنگین تر شده و خیلی
          از کارا که قبلا توسط توسعه دهنده بک اند (back-end developer) انجام
          میشد، الان دیگه فرانت اند کارا انجام میدن و همین دلیل باعث شده بازار
          استخدامی فرانت اند خیلی داغ بشه و حقوق فرانت اند هم خیلی بیشتر شده و
          همه دنبال افراد متخصص توی این حوزه هستند. بهت تبریک میگم به دنیای پر
          رمز و راز و جذاب برنامه نویسی وب و مخصوصا فرانت، خوش اومدی. از تجربه
          ما استفاده کن و تا لحظه ورود به بازار کار با نهایت افتخار کنارتیم.
        </p>

        <p className="front-bold text-xs sm:text-sm text-gray-700 mb-4 dark:text-gray-200">
          <span> میخوایی بدونی چه دوره ای برا شما مفید هست ؟ </span>
          <strong className="text-blue-600 dark:text-sky-400">
            <Link href="/learning-path">
              <a>حتما مقاله نقشه راه فرانت اند رو مطالعه کن. </a>
            </Link>
          </strong>{" "}
        </p>
      </div>
      <div className=" w-full md:w-1/3 ml-8">
        <img src="/images/frontend-intro.svg"></img>
      </div>
    </section>
  );
}

const statistics = [
  {
    title: toArabicNumber("5 +"),
    desc: "سال سابقه فعالیت حرفه ای",
  },
  {
    title: toArabicNumber("2433 +"),
    desc: "دانشجو خصوصی و آنلاین",
  },
  {
    title: toArabicNumber("97 % +"),
    desc: "رضایت از آموزش",
  },
];

function Statistics() {
  return (
    <section
      className="grid gap-4 grid-cols-9 md:justify-between 
     mt-12 sm:mt-32 container md:max-w-screen-xl mx-auto p-4"
    >
      <div className="col-span-9 sm:col-span-3">
        <h2 className="font-extrabold text-lg sm:text-2xl flex flex-col mb-2">
          آمار ها باعث افتخار ما هستند
        </h2>
        <p className="text-gray-700 text-xs sm:text-sm dark:text-slate-400">
          آخرین به روز رسانی : اردیبهشت {toArabicNumber(1401)}
        </p>
      </div>
      {statistics.map((item, index) => {
        return (
          <section
            key={index}
            className="bg-white sm:col-span-2 col-span-9  rounded-xl py-3 px-5 dark:bg-slate-800"
          >
            <div className=" sm:flex-col flex flex-row items-center justify-between w-full">
              <h3 className="font-extrabold text-lg sm:text-2xl">
                {item.title}
              </h3>
              <p className="text-primary sm:mt-2 dark:text-sky-500">
                {item.desc}
              </p>
            </div>
          </section>
        );
      })}
    </section>
  );
}
