import PostList from "@/components/posts/PostList";
import Layout from "@/containers/Layout";
import http from "@/services/htppService";
import Head from "next/head";
import { useEffect, useState } from "react";
import FetchLoading from "@/common/FetchLoading";
import queryString from "query-string";
import { useRouter } from "next/router";
import BasicPagination from "@/common/Pagination";

export async function getServerSideProps(context) {
  const { query, req } = context;
  const {
    data: { data: posts },
  } = await http.get(`/posts/bookmarks?${queryString.stringify(query)}`, {
    withCredentials: true,
    headers: {
      Cookie: req.headers.cookie || "cookie1=value1",
    },
  });

  return {
    props: { posts },
  };
}

const BookMarkedPosts = ({ posts }) => {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const router = useRouter();

  useEffect(() => {
    setIsRefreshing(false);
  }, [posts]);

  const pageHandler = (e, page) => {
    setIsRefreshing(true);
    router.query.page = page;
    router.push(router);
  };

  return (
    <Layout>
      <Head>
        <title>پست های ذخیره شده</title>
      </Head>
      <main className="mx-auto max-w-screen-lg">
        <h1 className="font-extrabold text-2xl mb-8">پست های ذخیره شده</h1>
        <div className="md:col-span-9 grid grid-cols-6 gap-y-8 gap-x-8">
          {isRefreshing ? (
            <FetchLoading isRefreshing={isRefreshing} />
          ) : (
            <PostList posts={posts} />
          )}
        </div>
        {posts.totalPages > 1 && (
          <div className="flex justify-center mt-6">
            <BasicPagination
              totalPages={posts.totalPages}
              pageHandler={pageHandler}
              currentPage={posts.page}
            />
          </div>
        )}
      </main>
    </Layout>
  );
};

export default BookMarkedPosts;
