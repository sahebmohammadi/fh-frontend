import BackendBreadcrumbs from "@/common/BackendBreadCrumb";
import MetaTagSEO from "@/components/MetaComponent";
import ProductsList from "@/components/ProductList";
import Layout from "@/containers/Layout";
import getAllProducts from "@/services/getAllProducts";
import getPageMetaData from "@/services/getPageMetaDataService";
import Head from "next/head";

export const getStaticProps = async () => {
  const { data } = await getAllProducts();
  const {
    data: { metaData },
  } = await getPageMetaData("/courses");
  return {
    props: { products: data, metaData },
    revalidate: 120,
  };
};

const Products = ({ products, metaData }) => {
  return (
    <Layout>
      <MetaTagSEO seo={metaData} />
      <main>
        <BackendBreadcrumbs breadCrumbs={metaData.breadcrumb} />
        <ProductsList products={products} />
      </main>
    </Layout>
  );
};

export default Products;
