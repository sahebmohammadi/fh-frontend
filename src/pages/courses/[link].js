import Layout from "@/containers/Layout";
import SingleCourse from "@/components/SingleCourse";
import getAllProducts from "@/services/getAllProducts";
import getOneProductPage from "@/services/getOneProductPage";
import MetaTagSEO from "@/components/MetaComponent";

export const getStaticPaths = async () => {
  const { data } = await getAllProducts();

  // Get the paths we want to pre-render based on posts
  const paths = data.map((p) => {
    return {
      params: { link: p.pageLink }, // link should be the same as [link] name
    };
  });

  // paths : [ {params:{link:"React"}},... ]

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return {
    paths, // specific key of path
    fallback: false,
  };
};

export const getStaticProps = async (context) => {
  const link = context.params.link;
  const {
    data: { product, seo },
  } = await getOneProductPage(link);

  return {
    props: { product, seo }, // will be passed to the page component as props,
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 120,
  };
};

const SingleProductPage = ({ product, seo }) => {
  return (
    <>
      <MetaTagSEO seo={seo} />
      <Layout>
        <SingleCourse product={product} breadCrumbs={seo.breadcrumb} />
      </Layout>
    </>
  );
};

export default SingleProductPage;
