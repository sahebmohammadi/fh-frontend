import { useFormik } from "formik";
import * as Yup from "yup";
import Link from "next/link";
import Input from "@/components/FormInput";
import Head from "next/head";
import SingleFormLayout from "@/components/SingleFormLayout";
import Layout from "@/containers/Layout";
import { useRouter } from "next/router";

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { register } from "../../redux/user/userActions";
import Toast from "@/components/Toast";
import Button from "@/common/Button";
import * as constants from "../../constants";

const formConstants = constants.formConstants;
const { labels } = formConstants;

//  initial values
const initialValues = {
  name: "",
  email: "",
  phoneNumber: "",
  password: "",
  confirmPassword: "",
};

//  validation schema
const validationSchema = Yup.object({
  name: Yup.string()
    .required(formConstants.fullName)
    .min(6, formConstants.fullNameType),
  email: Yup.string()
    .required(formConstants.email)
    .email(formConstants.emailtype),
  phoneNumber: Yup.string()
    .required(formConstants.phoneNumber)
    .matches(/^[0-9]{11}$/, formConstants.phoneType)
    .nullable(),
  password: Yup.string()
    .required(formConstants.password)
    .min(8, formConstants.passwordType),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("password"), ""], formConstants.passwordMatch)
    .required(formConstants.password),
});

const RegisterForm = () => {
  const router = useRouter();
  const query = router.query.redirect;
  const redirect = query || "/";
  const dispatch = useDispatch();
  const userRegister = useSelector((state) => state.userRegister);
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading } = userSignin;

  // redirect user :
  useEffect(() => {
    if (userInfo) router.push(redirect);
  }, [router.query, redirect, userInfo]);

  //  onSubmit
  const onSubmit = (values) => {
    const { name, email, phoneNumber, password } = values;
    dispatch(register(name, email.toLowerCase(), password, phoneNumber));
  };
  
  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: true,
  });

  return (
    <Layout>
      <Head>
        <title>Front Hooks- Signup</title>
      </Head>
      <SingleFormLayout title={formConstants.signupTitle} {...userRegister}>
        <form
          onSubmit={formik.handleSubmit}
          className="flex flex-col space-y-4"
        >
          <Input label={labels.name} name="name" formik={formik} />
          <Input label={labels.email} name="email" formik={formik} />
          <Input
            type="tel"
            label={labels.phoneNumber}
            name="phoneNumber"
            formik={formik}
            placeholder="09121234567"
          />
          <Input
            label={labels.password}
            name="password"
            type="password"
            formik={formik}
          />
          <Input
            label={labels.passwordConfirm}
            name="confirmPassword"
            type="password"
            formik={formik}
          />
          {!loading && userRegister.error && (
            <Toast>{userRegister.error}</Toast>
          )}

          <Button type="submit" disabled={!formik.isValid}>
            {redirect !== "/" ? "ثبت اطلاعات و ادامه سفارش" : "ثبت نام"}
          </Button>
          <Link href={`/login?redirect=${redirect}`}>
            <p className="mt-4 py-4 cursor-pointer">
              قبلا ثبت نام کردی ؟ لاگین کنید
            </p>
          </Link>
        </form>
      </SingleFormLayout>
    </Layout>
  );
};

export default RegisterForm;
export function getStaticProps() {
  return {
    // returns the default 404 page with a status code of 404
    notFound: true,
  };
}
