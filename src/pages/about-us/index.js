import Layout from "@/containers/Layout";
import { toArabicDigits } from "@/utils/toArabicNumbers";
import getPageMetaData from "@/services/getPageMetaDataService";
import MetaTagSEO from "@/components/MetaComponent";
import BackendBreadcrumbs from "@/common/BackendBreadCrumb";


export const getStaticProps = async () => {
  const {
    data: { metaData },
  } = await getPageMetaData("/about-us");
  return {
    props: { metaData },
  };
};
const cvData = [
  {
    title: "Senior Front-end developer",
    position:
      "Next.js and React.js Developer in <strong>Calaq</strong> startup",
    info: `Calaq group are mainly focusing on analysis the effect of daily
      news from any sources such as Telegram, Twitter on stock market
      fluctuations. As front-end developer in this team, my
      responsibility is developing UI platform using React and next.js
      to optimise the efficiency and boost performance of AI
      developer.`,
    time: [1399, 1400],
  },
  {
    title: "Front-end Consultant",
    position: "Front-end team consultant in <strong>Tack-size</strong> startup",
    info: `I focused on reviewing the code, selecting up-to-date and optimized tools, choosing useful libraries for React.js logic and component-based styles frameworks.`,
    time: [1399, 1400],
  },
  {
    title: "Front-end developer",
    position: "Next.js Developer in <strong>Decooj</strong> startup",
    info: `Decooj stratup was focusing on providing easiest way to sell the furniture related needs.
    Decooj web application has three main users, including merchant, typical users, and admin.
    Merchant can build their platform and upload all the furnitures to sell. Besides, they can make order to other merchants to buy materials utilized in their industry.
    the users can easily add their needs to cart, buy and set delivery time.
    All These lovely process takes place in Decooj PWA.`,
    time: [1398, 1399],
  },
  {
    title: "Freelancer React.js developer",
    position: "React.js Developer in <strong>Winatalent</strong> website",
    info: `Acting as a React.js freelancer in winatalent website, focusing on foreign projects`,
    time: [1398, 1399],
  },
  {
    title: "Freelancer Front-end developer",
    position:
      "Freelancer Front-end Developer at <strong>Fiverr, Upwork</strong> website",
    info: `Acting as a freelancer Front-end, focusing on Iranian projects inclduing; React.js, vanilla Js projects`,
    time: [1395, 1398],
  },
  {
    title: "Freelancer MATLAB developer",
    position: "Petroleum Engineer MATLAB developer",
    info: `as Petroleum Engineer, I was focusing on simulating, optimizing of oil wells problems. 
    these projects mainly incldued Complex Mathematics problems, handling by MATLAB programming software `,
    time: [1393, 1395],
  },
];

const Index = ({ metaData }) => {
  return (
    <Layout>
      <MetaTagSEO seo={metaData} />
      <BackendBreadcrumbs breadCrumbs={metaData.breadcrumb} />
      <main>
        <div className="max-w-screen-md mx-auto">
          <header>
            <h1 className="font-extrabold text-2xl mb-6 dark:text-white">
              درباره فرانت هوکس
            </h1>
          </header>

          <p className="leading-8 md:leading-10 text-md md:text-lg mb-5">
            ما فرانت هوکس هستیم. هدف اصلی کسب و کار ما فراهم کردن بستری برای
            ورود علاقه مندان به برنامه نویسی به بازار کار هست. اما چطور؟
          </p>
          <p className="leading-8 md:leading-10 text-md md:text-lg">
            فرانت هوکس در اوایل فروردین ماه سال 1400 به منظور ایجاد یک منبع جامع
            برای تبدیل شدن به یک متخصص فرانت اند و یادگیری اصولی برنامه نویسی وب
            در کنار پشتیبانی دائمی و دوره های کاملا پروژه محور شروع به کار کرد.
          </p>
          <h2 className="font-extrabold text-xl my-5 dark:text-slate-300">
            حلقه گم شده آموزش های برنامه نویسی؟
          </h2>
          <p className="leading-8 md:leading-10 text-md md:text-lg">
            مشکل اصلی اکثر دوره های برنامه نویسی این هست که درک عمیقی از مفاهیم
            پایه ای در ذهن دانشجو ایجاد نمی شود. علاوه بر این نبود پروژه های
            حرفه ای تثبیت مطالب را برای دانشجو بسیار مشکل می کند. همچنین، گاها
            دانشجو دچار مشکلاتی می شود که قطعا وجود یک مربی با تجربه می تواند
            خیلی مفید باشد. همه این مشکلات باعث می شود که یادگیری برنامه نویسی
            برای افراد تازه کار به شکست منجر شود.
          </p>
          <h2 className="font-extrabold text-xl my-5 dark:text-slate-300">
            مزیت دوره های فرانت هوکس
          </h2>
          <p className="leading-8 md:leading-10 text-md md:text-lg">
            همه دوره ها کاملا آپدیت هستند و در اولین بروز رسانی فریمورک های
            مربوطه دوره ها نیز به صورت رایگان برای اعضای قبلی آپدیت می شوند. مهم
            تر از همه پشتیبانی دوره ها هیچ زمان محدودی ندارد و خود من - صاحب
            محمدی - شخصا مربی و همراه شما هستم. همه دوره های فرانت هوکس با تمرکز
            خاصی روی یادگیری اصولی فرانت اند در طی پروژه های حرفه ای ارائه شده
            است و خیال شما از بابت پروژه محور بودن، آپدیت بودن و پشتیبانی کاملا
            راحت هست.
          </p>
        </div>
        <div class="my-container pl-10 my-8">
          <ul className="flex justify-center flex-wrap">
            {cvData.map((item) => {
              return (
                <li className="bg-white rounded-xl shadow-lg md:w-1/2 dark:bg-slate-800">
                  {/* <span></span> */}
                  <div className="text-left p-0">
                    <div class="title text-primary">{item.title}</div>
                    <div
                      class="text-gray-700 dark:text-slate-300 md:text-base"
                      dangerouslySetInnerHTML={{ __html: item.position }}
                    />
                    <div
                      class="text-gray-500 dark:text-slate-400 text-xs md:text-base mt-4 text-left"
                      style={{ direction: "ltr" }}
                    >
                      {item.info}
                    </div>
                  </div>
                  <span class="number">
                    <span>{toArabicDigits(item.time[1])}</span>
                    <span>{toArabicDigits(item.time[0])}</span>
                  </span>
                </li>
              );
            })}
          </ul>
        </div>
      </main>
    </Layout>
  );
};

export default Index;
