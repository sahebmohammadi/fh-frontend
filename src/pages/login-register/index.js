import { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Link from "next/link";
import Input from "@/components/FormInput";
import Head from "next/head";
import SingleFormLayout from "@/components/SingleFormLayout";
import Layout from "@/containers/Layout";
import router, { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { register } from "@/redux/user/userActions";
import Toast from "@/components/Toast";
import Button from "@/common/Button";
import * as constants from "../../constants";
import { toArabicDigits } from "../../utils/toArabicNumbers";
import * as userAuth from "../../services/userAuthServicer";
import toast from "react-hot-toast";
const formConstants = constants.formConstants;
const { labels } = formConstants;

const RESEND_TIME = 90;

//  initial values
const initialValues = {
  name: "",
  email: "",
  // phoneNumber: "",
  password: "",
  confirmPassword: "",
};

//  validation schema
const validationSchema = Yup.object({
  name: Yup.string()
    .required(formConstants.fullName)
    .min(6, formConstants.fullNameType),
  email: Yup.string()
    .required(formConstants.email)
    .email(formConstants.emailtype),
  // phoneNumber: Yup.string()
  //   .required(formConstants.phoneNumber)
  //   .matches(/^[0-9]{11}$/, formConstants.phoneType)
  //   .nullable(),
  password: Yup.string()
    .required(formConstants.password)
    .min(8, formConstants.passwordType),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("password"), ""], formConstants.passwordMatch)
    .required(formConstants.password),
});

const MuiltiStepForm = (props) => {
  const dispatch = useDispatch();
  const userRegister = useSelector((state) => state.userRegister);
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading } = userSignin;
  const [step, setStep] = useState(1);
  const [emailPhone, setEmailPhone] = useState("");

  const [codeResponse, setCodeResponse] = useState(null);
  const [codeError, setCodeError] = useState(null);
  const [verifyResponse, setVerifyResponse] = useState(null);
  const [verifyError, setVerifyError] = useState(null);

  //   // redirect user :
  //   useEffect(() => {
  //     if (userInfo) router.push(redirect);
  //   }, [router.query, redirect, userInfo]);

  //  onSubmit
  const onSubmit = (values) => {
    const { name, email, phoneNumber, password } = values;
    dispatch(register(name, email.toLowerCase(), password, phoneNumber));
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: true,
  });

  const [verficationCode, setVerficationCode] = useState("");
  const [time, setTime] = useState(RESEND_TIME);

  useEffect(() => {
    const timer =
      time > 0 && setInterval(() => setTime((time) => time - 1), 1000);
    return () => {
      if (timer) {
        clearInterval(timer);
      }
    };
  }, [time]);

  const resendNewCodeHandler = () => {
    // console.log("resend code to", emailPhone);
    setTime(RESEND_TIME);
  };

  const verficationCodeHandler = (e) => {
    e.preventDefault();
    // if (!time) setTime(RESEND_TIME);
    userAuth
      .verifyCode({ verficationCode })
      .then(({ data }) => {
        const { isActivated, isUserExist } = data;
        setCodeError(null);
        setVerifyError(null);
        toast.success(data.message);
        setVerifyResponse(data);
        if (!isUserExist) {
          setStep(3);
        } else {
          router.push("/");
        }
      })
      .catch((err) => {
        // setCodeResponse(null);
        setVerifyResponse(null);
        console.log(err?.response?.data);
        if (err?.response?.data?.message) {
          toast.error(err.response.data.message);
          setVerifyError(err.response.data);
        }
        // PREVENT TO GO FURTHER ...
      });
  };

  const sendCodeHandler = (e) => {
    e.preventDefault();
    userAuth
      .sendCode({ emailPhone })
      .then((res) => {
        setCodeResponse(res.data);
        toast.success(res.data.message);
        setStep(2);
        setTime(RESEND_TIME);
        setCodeError(null);
        setVerifyError(null);
      })
      .catch((err) => {
        setVerifyResponse(null);
        setCodeResponse(null);
        setCodeResponse(null);
        if (err?.response?.data) {
          setCodeError(err?.response?.data?.message);
          toast.error(err.response.data.message);
        }
      });

    // setTime(RESEND_TIME);
    // console.log("verify code send to", emailPhone);
  };

  const verficationCodeChange = (e) => {
    setVerficationCode(e);
  };

  const renderStep = (step) => {
    switch (step) {
      case 1:
        return (
          <PhoneNumber
            emailPhone={emailPhone}
            setEmailPhone={setEmailPhone}
            sendCodeHandler={sendCodeHandler}
            codeError={codeError}
          />
        );
      case 2:
        return (
          <CodeValidation
            formik={formik}
            verficationCodeHandler={verficationCodeHandler}
            verficationCodeChange={verficationCodeChange}
            resendNewCodeHandler={resendNewCodeHandler}
            codeResponse={codeResponse}
            time={time}
            verifyError={verifyError}
            verifyResponse={verifyResponse}
          />
        );
      case 3:
        return <UserInfo formik={formik} labels={labels} />;
      default:
        return null;
    }
  };

  return (
    <Layout>
      <Head>
        <title> ورود / ثبت نام | فرانت هوکس</title>
      </Head>
      <SingleFormLayout
        title={"ورود / ثبت نام"}
        {...userRegister}
        step={step}
        setStep={setStep}
      >
        {renderStep(step)}
        {!loading && userRegister.error && <Toast>{userRegister.error}</Toast>}
        {/* <Link href={`/login?redirect=${redirect}`}>
            <p className="mt-4 py-4 text-xs text-gray-400 cursor-pointer">
              قبلا ثبت نام کردی ؟ لاگین کنید
            </p>
          </Link> */}
      </SingleFormLayout>
    </Layout>
  );
};

export default MuiltiStepForm;

const PhoneNumber = ({
  sendCodeHandler,
  emailPhone,
  setEmailPhone,
  codeError,
}) => {
  return (
    <form onSubmit={sendCodeHandler} className="flex flex-col">
      <div>
        <label
          className="flex flex-row mb-4 text-sm text-gray-500"
          htmlFor="emailPhone"
        >
          شماره موبایل یا ایمیل خود را وارد کنید
        </label>
        <input
          dir="ltr"
          // placeholder={placeholder}
          className="mb-2 text-left border p-2 text-sm rounded border-gray-200 outline-none w-full
           focus:outline-none focus:ring-2 focus:ring-primary focus:border-transparent"
          type="text"
          id="emailPhone"
          name="emailPhone"
          onChange={(e) => setEmailPhone(e.target.value)}
          value={emailPhone}
        />
      </div>
      <Button type="submit" className="w-full my-4 py-4">
        ورود به فرانت هوکس
      </Button>
    </form>
  );
};

const CodeValidation = ({
  verficationCodeChange,
  verficationCodeHandler,
  resendNewCodeHandler,
  codeResponse,
  verifyResponse,
  verifyError,
  time,
}) => {
  const resendNewCodeSection = () => {
    return (
      <div className="mb-8 mt-8 text-center text-xs">
        {time > 0 ? (
          <p>ارسال مجدد کد تا {toArabicDigits(time)} ثانیه دیگر</p>
        ) : (
          <p
            onClick={resendNewCodeHandler}
            className="cursor-pointer py-4 text-xs text-primary"
          >
            دریافت مجدد کد تایید
          </p>
        )}
      </div>
    );
  };

  return (
    <form onSubmit={verficationCodeHandler} className="flex flex-col">
      {/* BACKEND RESPONSE OR ERROR */}
      <p className="text-gray-400 text-xs">
        {codeResponse ? codeResponse.message : <span>&nbsp;&nbsp;</span>}
      </p>
      <div className="text-gray-700 font-bold my-4">کد تایید را وارد کنید</div>
      <div
        className="custom-styles text-center my-2 flex justify-center"
        dir="ltr"
      ></div>
      {resendNewCodeSection()}
      {/* <p className="text-gray-400 text-xs">
        {codeResponse ? codeResponse.message : <span>&nbsp;&nbsp;</span>}
      </p> */}
      <Button
        type="submit"
        className="py-4 rounded outline-none w-full text-white 
               bg-primary focus:outline-none mb-4"
      >
        تایید و ادامه
      </Button>
    </form>
  );
};

const UserInfo = ({ formik, labels }) => {
  const router = useRouter();
  const query = router.query.redirect;
  const redirect = query || "/";

  return (
    <form onSubmit={formik.handleSubmit} className="flex flex-col space-y-4">
      <Input label={labels.name} name="name" formik={formik} />
      <Input label={labels.email} name="email" formik={formik} />
      {/* <Input
        type="tel"
        label={labels.phoneNumber}
        name="phoneNumber"
        formik={formik}
        placeholder="09121234567"
      /> */}
      <Input
        label={labels.password}
        name="password"
        type="password"
        formik={formik}
      />
      <Input
        label={labels.passwordConfirm}
        name="confirmPassword"
        type="password"
        formik={formik}
      />
      {/* {!loading && userRegister.error && <Toast>{userRegister.error}</Toast>} */}

      <Button type="submit" disabled={!formik.isValid}>
        {redirect !== "/" ? "ثبت اطلاعات و ادامه سفارش" : "ثبت نام"}
      </Button>
    </form>
  );
};

export function getStaticProps() {
  return {
    // returns the default 404 page with a status code of 404
    notFound: true,
  };
}
