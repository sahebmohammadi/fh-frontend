import { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Input from "@/components/FormInput";
import Head from "next/head";
import Layout from "@/containers/Layout";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
// import Toast from "@/components/Toast";
import Button from "@/common/Button";
import * as constants from "../../constants";
import StepperComponent from "@/common/StepperComponent";
import CircularProgress from "@material-ui/core/CircularProgress";
import * as userAuth from "@/services/userAuthServicer";
import toast from "react-hot-toast";
import { toEnglishDigits } from "@/utils/toEnglishDigits";

const formConstants = constants.formConstants;
const { labels } = formConstants;

//  initial values
const initialValues = {
  name: "",
  email: "",
  phoneNumber: "",
};

//  validation schema
const validationSchema = Yup.object({
  name: Yup.string()
    .required(formConstants.fullName)
    .min(6, formConstants.fullNameType),
  email: Yup.string()
    .required(formConstants.email)
    .email(formConstants.emailtype),
  phoneNumber: Yup.string()
    .required(formConstants.phoneNumber)
    .matches(/^[\u0600-\u06FF\s0-9]{11}$/, formConstants.phoneType)
    .nullable(),
});

const MuiltiStepForm = (props) => {
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.userSignin);
  const [loading, setLoading] = useState(false);
  const { phoneNumber, email, name } = userInfo || {};
  const router = useRouter();
  const query = router.query.redirect;
  const redirect = query || "/";

  useEffect(() => {
    if (!userInfo) router.push("/auth");
  }, [userInfo]);

  useEffect(() => {
    if (userInfo && name && email && phoneNumber) router.push("/checkout");
  }, [userInfo]);

  //  onSubmit
  const onSubmit = (values) => {
    setLoading(true);
    const formattedValues = {
      name: values.name,
      email: toEnglishDigits(values.email),
      phoneNumber: toEnglishDigits(values.phoneNumber),
    };
    userAuth
      .completeProfile(formattedValues)
      .then(({ data }) => {
        setLoading(false);
        const { name, email, phoneNumber, enrolledCourses } = data;
        const { isActivated, isUserExist } = data;
        toast.success(data.message);
        dispatch({
          type: "USER_SIGNIN_SUCCESS",
          payload: { name, email, phoneNumber, enrolledCourses },
        });
        router.push("/checkout");
      })
      .catch((err) => {
        setLoading(false);
        if (err?.response?.data?.message) {
          toast.error(err.response.data.message);
        }
      });
  };

  const formik = useFormik({
    initialValues: {
      phoneNumber: phoneNumber || "",
      email: email || "",
      name: name || "",
    },
    onSubmit,
    validationSchema,
    validateOnMount: true,
    enableReinitialize: true,
  });

  return (
    <Layout>
      <StepperComponent activeStep={1} />
      <Head>
        <title> تکمیل اطلاعات کاربری | فرانت هوکس</title>
      </Head>
      <SingleFormLayout loading={loading}>
        <form onSubmit={formik.handleSubmit} className="flex flex-col">
          <div className="flex flex-col space-y-4">
            <Input label={labels.name} name="name" formik={formik} />
            {!phoneNumber && (
              <Input
                label={labels.phoneNumber}
                name="phoneNumber"
                type="phoneNumber"
                formik={formik}
                type="tel"
              />
            )}
            {!email && (
              <Input label={labels.email} name="email" formik={formik} />
            )}
          </div>

          <Button type="submit" disabled={!formik.isValid} className="mt-10">
            ثبت اطلاعات و ادامه سفارش
          </Button>
        </form>
      </SingleFormLayout>
    </Layout>
  );
};

export default MuiltiStepForm;

function SingleFormLayout({ children, title, loading, error }) {
  return (
    <div className="sm:border rounded-lg border-gray-300 w-full sm:max-w-md sm:my-8 sm:m-auto md:my-4 flex flex-col">
      <div className="rounded-t-lg mb-4">
        <div className="px-8 flex items-center mt-4">
          <div className="text-2xl font-black  py-4 text-primary text-center flex-1">
            فرانت هوکس
          </div>
        </div>
      </div>
      {!loading ? (
        <div className="py-4 px-8">{children}</div>
      ) : (
        <div className="py-4 px-8">
          <CircularProgress color="primary" />
        </div>
      )}
    </div>
  );
}
