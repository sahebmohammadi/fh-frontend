import { useFormik } from "formik";
import * as Yup from "yup";
import { useContext, useEffect, useState } from "react";
import Link from "next/link";
import Head from "next/head";
import SingleFormLayout from "@/components/SingleFormLayout";
import Input from "@/components/FormInput";
import Layout from "@/containers/Layout";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { signin } from "@/redux/user/userActions";
import Toast from "@/components/Toast";
import Button from "@/common/Button";
import * as constants from "../../constants";

const formConstants = constants.formConstants;
const { labels } = formConstants;
//  initial values
const initialValues = {
  email: "",
  password: "",
};

//  validation schema
const validationSchema = Yup.object({
  email: Yup.string().required(formConstants.email),
  password: Yup.string().required(formConstants.password),
});

const Login = (props) => {
  const router = useRouter();
  const query = router.query.redirect;
  const redirect = query || "/";
  const dispatch = useDispatch();
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;
  const [isVerified, setIsVerified] = useState(false);
  useEffect(() => {
    if (userInfo && userInfo.isAdmin) router.push("/admin-profile");
    if (userInfo && !userInfo.isAdmin) router.push(redirect);
  }, [router.query, redirect, userInfo]);

  const onSubmit = ({ email, password }) => {
    dispatch(signin(email.split(" ")[0], password));
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnMount: true,
    onSubmit,
  });
  const reCaptchaLoaded = () => {
    // console.log("reCaptcha loaded ....");
  };
  // specifying verify callback function
  var verifyCallback = function (response) {
    if (response) setIsVerified(true);
  };

  return (
    <Layout>
      <Head>
        <title>Front Hooks - Login</title>
      </Head>
      <SingleFormLayout title={formConstants.loginTitle} {...userSignin}>
        <form onSubmit={formik.handleSubmit} className="flex flex-col">
          <Input
            className="flex-1 mb-4"
            label={labels.email}
            name="email"
            formik={formik}
          />
          <Input
            className="flex-1 mb-6"
            label={labels.password}
            name="password"
            type="password"
            formik={formik}
          />

          {!loading && error && <Toast>{error}</Toast>}
          <div className="flex justify-center mb-4">
            <Recaptcha sitekey={process.env.NEXT_PUBLIC_SITE_KEY} />
          </div>
          <Button type="submit" disabled={!formik.isValid && !isVerified}>
            ورود
          </Button>
          <Link href={`/signup?redirect=${redirect}`}>
            <a>
              <p className="text-right mt-8">
                هنوز ثبت نام نکردی؟ برای ثبت نام کلیک کن
              </p>
            </a>
          </Link>
          <Link href="/forgot-password">
            <a>
              <p className="text-right mt-2">رمز عبور رو فراموش کردی ؟</p>
            </a>
          </Link>
        </form>
      </SingleFormLayout>
    </Layout>
  );
};

export default Login;
export function getStaticProps() {
  return {
    // returns the default 404 page with a status code of 404
    notFound: true,
  };
}
