import Layout from "@/containers/Layout";
import AddedItemsList from "@/components/CartItems/AddedItemsList";
import CartSummery from "@/components/CartItems/CartSummery";
import { useSelector } from "react-redux";
import Link from "next/link";
import Toast from "@/components/Toast";
import Head from "next/head";
const Cart = (props) => {
  const cart = useSelector((state) => state.cart);

  if (cart.addedItems.length === 0)
    return (
      <Layout>
        <Head>
          <title> سبد خرید | فرانت هوکس </title>
        </Head>
        <Toast>متاسفانه سبد خرید شما خالیه</Toast>
        <Link href="/courses">
          <a>
            <div className="mt-4">
              <Toast variant="success">میخوام برم به صفحه دوره ها</Toast>
            </div>
          </a>
        </Link>
      </Layout>
    );

  return (
    <Layout>
      <Head>
        <title> سبد خرید | فرانت هوکس </title>
      </Head>
      <main className="grid grid-cols-1 sm:grid-cols-4 gap-4">
        <section
          className={`section-border dark:bg-slate-800 dark:border dark:border-slate-700/60 md:col-span-3`}
        >
          <AddedItemsList />
        </section>
        <section
          className={`section-border dark:bg-slate-800 dark:border dark:border-slate-700/60 md:col-span-1 h-72`}
        >
          <CartSummery />
        </section>
      </main>
    </Layout>
  );
};

export default Cart;
