import Layout from "@/containers/Layout";
import MetaTagSEO from "@/components/MetaComponent";
import getPosts from "@/services/getPostsService";
import { useState, useEffect } from "react";
import Link from "next/link";
import toArabicNumber, { toArabicDigits } from "@/utils/toArabicNumbers";

import { useRouter } from "next/router";
import getPostBySlug from "@/services/getPostBySlugService";
import http from "@/services/htppService";
import toJalaliDate from "@/common/toJalaliDate";
import { BsBookmarkPlus, BsBookmarkCheck } from "react-icons/bs";
import { IoLogoLinkedin, IoLogoTwitter } from "react-icons/io";
import { MdContentCopy } from "react-icons/md";
import { FaTelegram } from "react-icons/fa";
import { CopyToClipboard } from "react-copy-to-clipboard";
import PostComments from "@/components/posts/postComment";
import Head from "next/head";
import usePush from "@/hooks/usePush";
import toast from "react-hot-toast";

// export const getStaticPaths = async () => {
//   const {
//     data: { data: posts },
//   } = await getPosts();

//   // Get the paths we want to pre-render based on posts
//   const paths = posts.docs.map((post) => {
//     return {
//       params: {
//         slug: post.slug,
//       },
//     };
//   });

//   return {
//     paths, // specific key of path
//     fallback: false,
//   };
// };

// export const getStaticProps = async (context) => {
//   const slug = context.params.slug;

//   const {
//     data: { data: post },
//   } = await getPostBySlug(slug);

//   return {
//     props: { post },
//   };
// };

export async function getServerSideProps({ req, params }) {
  const { slug } = params;
  // console.log("cookies", req.headers.cookie);
  const {
    data: { data: post },
  } = await http.get(`/posts/${slug}`, {
    withCredentials: true,
    headers: {
      Cookie: req.headers.cookie || "cookie1=value1",
    },
  });

  return {
    props: {
      post,
    },
  };
}

const PosePage = ({ post }) => {
  const router = useRouter();
  const [isVisible, setIsVisible] = useState(true);
  const [copied, setCopied] = useState(false);
  let heightToHideFrom = 0;

  // useEffect(() => {
  //   window.addEventListener("scroll", listenToScroll);
  //   return () => window.removeEventListener("scroll", listenToScroll);
  // }, []);

  const listenToScroll = () => {
    // let node = document.querySelector("#comment-section");
    // const elTop = node.getBoundingClientRect().top;
    // if (elTop < 450) {
    //   isVisible && // to limit setting state only the first time
    //     setIsVisible(false);
    // } else {
    //   setIsVisible(true);
    // }
  };

  const bookmarkHandler = async (postId) => {
    http
      .put(`/posts/bookmark/${postId}`)
      .then(({ data }) => {
        toast.success(data.message);
        usePush(router);
      })
      .catch((err) => {
        if (err?.response?.data?.message) {
          toast.error(err.response.data.message);
        }
      });
  };

  const likeHandler = async (postId) => {
    http
      .put(`/posts/like/${postId}`)
      .then(({ data }) => {
        toast.success(data.message);
        usePush(router);
      })
      .catch((err) => {
        if (err?.response?.data?.message) {
          toast.error(err.response.data.message);
        }
      });
  };

  const copyHandler = () => {
    setCopied(true);
    setTimeout(() => {
      setCopied(false);
    }, 1000);
  };

  return (
    <>
      {/* <MetaTagSEO seo={seo} /> */}
      <Head>
        <title>{post.title}</title>
      </Head>
      <Layout>
        <div className="flex ">
          <main className="mx-auto max-w-screen-2xl">
            <header className="flex flex-col md:flex-row gap-y-5 bg-red--100 md:justify-between md:items-start mb-12 mt-5 max-w-screen-md mx-auto">
              <div className="flex items-stretch">
                <img
                  className="w-14 h-14 md:w-20 md:h-20 rounded-full ring-2 ring-white dark:ring-white/90"
                  src="/images/saheb.png"
                  alt={post.author.name}
                />
                <div className="flex flex-col mr-4 justify-between">
                  <div>
                    {" "}
                    <span className="font-extrabold text-base">
                      {post.author.name} {""}
                    </span>
                    <Link href={`/blogs/${post.category.englishTitle}`}>
                      <a className="bg-white border border-blue-500 text-xs text-blue-500 px-3 py-1 mr-1 rounded-full transition-all duration-300 hover:bg-blue-500 hover:text-white dark:bg-opacity-10 dark:bg-slate-500 dark:hover:bg-opacity-100 dark:hover:bg-blue-500">
                        {post.category.title}
                      </a>
                    </Link>
                  </div>
                  <span className="font-normal text-xs hidden md:block">
                    {post.author.biography}
                  </span>

                  <div className="font-normal text-myGray-400 text-sm dark:text-slate-500">
                    <span>{toJalaliDate(post.createdAt)}</span>
                    <span className="mx-1"> &bull;</span>
                    <span>
                      <span> خواندن</span>
                      <span> {toArabicDigits(post.readingTime)} </span>
                      <span>دقیقه </span>
                    </span>
                  </div>
                </div>
              </div>
              <div className="flex">
                <button>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 hover:text-black text-gray-500 cursor-pointer dark:text-slate-400"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="1"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1"
                    />
                  </svg>
                </button>
                <button
                  className="mr-4 border border-gray-200 text-gray-500 hover:text-gray-600 dark:text-slate-400 dark:hover:text-slate-200 dark:border-slate-400 rounded-full px-3 py-1 flex items-center "
                  onClick={() => bookmarkHandler(post._id)}
                >
                  <span className="ml-1 text-xs ">
                    {post.isBookmarked ? "ذخیره شده" : "ذخیره"}
                  </span>
                  {post.isBookmarked ? (
                    <BsBookmarkCheck size={22} className="" />
                  ) : (
                    <BsBookmarkPlus size={22} className="" />
                  )}
                </button>
              </div>
            </header>
            <section className="flex">
              <article className="mx-auto max-w-screen-md  prose prose-headings:front-extrabold md:prose-h2:text-2xl prose-h2:text-xl  prose-h2:mt-8 prose-h2:mb-5 prose-p:leading-8 md:prose-p:leading-10 prose-p:text-md md:prose-p:text-lg dark:prose-invert dark:prose-p:text-slate-400">
                <h1 className="text-xl md:text-3xl mb-8">{post.title}</h1>
                <h2 className="">عنوان تستی</h2>
                <p className="">
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و
                  با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و
                  مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی
                  تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای
                  کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و
                  آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم
                  افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
                  طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
                  صورت می توان امید داشت که تمام و دشواری موجود در ارائه
                  راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
                  حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای
                  موجود طراحی اساسا مورد استفاده قرار گیرد.
                </p>
                <h2 className=""> دومی عنوان تستی</h2>
                <p className="">
                  لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و
                  با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و
                  مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی
                  تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای
                  کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و
                  آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم
                  افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص
                  طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
                  صورت می توان امید داشت که تمام و دشواری موجود در ارائه
                  راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل
                  حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای
                  موجود طراحی اساسا مورد استفاده قرار گیرد.
                </p>
              </article>
            </section>
          </main>
          <aside
            id="hide"
            className={`${
              isVisible ? "opacity-100" : "opacity-0"
            } w-[220px] items-center transition-all duration-300 hidden md:flex`}
          >
            <div className="flex flex-col sticky top-60">
              <div>
                <div className="font-bold text-base mb-3">
                  {post.author.name}
                </div>
              </div>
              <div className="font-normal text-xs text-gray-700 hidden md:block dark:text-slate-500">
                {post.author.biography}
              </div>
              <hr className="my-4 dark:bg-slate-700 bg-gray-500 h-0.5 border-0" />
              <div className="flex items-center gap-x-5 text-gray-500 dark:text-slate-400">
                <button className="flex items-center   transition duration-300 px-1 py-0.5 rounded ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 ml-1"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="1"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
                    />
                  </svg>
                  <span className="">{toArabicNumber(post.commentsCount)}</span>
                </button>
                <div className="flex items-end  transition duration-300  px-1 py-0.5 rounded text-gray-500 dark:text-slate-400">
                  <button onClick={() => likeHandler(post._id)}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className={`stroke-gray-500 h-6 w-6 ml-1 hover:stroke-red-500 ${
                        post.isLiked ? "fill-red-500 stroke-0" : ""
                      }`}
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth="1"
                      fill="none"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                      />
                    </svg>
                  </button>

                  <span className="">{toArabicNumber(post.likesCount)}</span>
                </div>
                <button
                  onClick={() => bookmarkHandler(post._id)}
                  className="flex items-end transition duration-300 px-1 py-0.5 rounded"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className={`h-6 w-6  ${
                      post.isBookmarked
                        ? "fill-gray-500 dark:fill-slate-400"
                        : ""
                    }`}
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="1"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </aside>
        </div>
        <section className="max-w-screen-xl mx-auto">
          {/* tag section of blog */}
          <section className="mt-20 mb-16">
            <ul className="flex items-center gap-y-4 gap-x-4 flex-wrap mb-7">
              {[1, 2, 3, 4].map((item) => {
                return (
                  <li className="block mb-3" key={item}>
                    <Link href="#">
                      <a className="bg-gray-100 border border-gray-300 hover:bg-gray-300 transition-all duration-300 rounded-full py-1.5 px-3 text-gray-500 dark:border-slate-700 dark:text-slate-400 dark:bg-slate-500 dark:bg-opacity-30">
                        ری اکت
                      </a>
                    </Link>
                  </li>
                );
              })}
            </ul>
            {/* like- comment count */}
            <div className="flex justify-between flex-wrap gap-y-5 mb-6 max-w-screen-lg">
              <div className="flex items-center gap-x-5 text-gray-500 justify-evenly md:justify-start w-full md:w-auto">
                <button className="flex items-center transition dark:text-slate-400 duration-300 px-1 py-0.5 rounded ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-6 w-6 ml-1"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="1"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
                    />
                  </svg>
                  <span className="">{toArabicNumber(post.commentsCount)}</span>
                </button>
                <div className="flex items-end  transition duration-300  px-1 py-0.5 rounded text-gray-500 bg">
                  <button onClick={() => likeHandler(post._id)}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className={`heart h-6 w-6 ml-1 hover:stroke-red-500 ${
                        post.isLiked ? "fill-red-500 stroke-0 is-active" : ""
                      }`}
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth="1"
                      fill="none"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                      />
                    </svg>
                  </button>
                  <span className="">{toArabicNumber(post.likesCount)}</span>
                </div>
                <button
                  onClick={() => bookmarkHandler(post._id)}
                  className="flex items-end transition duration-300 px-1 py-0.5 rounded dark:text-slate-400"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className={`h-6 w-6 ${
                      post.isBookmarked
                        ? "fill-gray-500 dark:fill-slate-400"
                        : ""
                    }`}
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth="1"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
                    />
                  </svg>
                </button>
              </div>
              {/* share on social ... */}
              <div className="flex items-center md:gap-x-6 w-full md:w-auto">
                <div className="flex items-center md:gap-x-4 gap-x-3 w-full">
                  <a
                    href={`https://www.linkedin.com/shareArticle?mini=true&url=https://fronthooks.ir/posts/${post.slug}`}
                    target="_blank"
                    className="block"
                  >
                    <IoLogoLinkedin
                      size={30}
                      className="fill-gray-400 dark:fill-slate-500 dark:hover:fill-slate-400 hover:fill-gray-500 transition-all duration-300 cursor-pointer"
                    />
                  </a>
                  <a
                    href={`https://twitter.com/share?text=${post.title}&url=https://fronthooks.ir/posts/${post.slug}`}
                    target="_blank"
                    // rel="noopener"
                    className="block"
                  >
                    <IoLogoTwitter
                      size={24}
                      className="fill-gray-400 dark:fill-slate-500 dark:hover:fill-slate-400 hover:fill-gray-500 transition-all duration-300 cursor-pointer"
                    />
                  </a>
                  <a
                    className="block"
                    target="_blank"
                    href={`https://telegram.me/share/url?url=https://fronthooks.ir/posts/${post.slug}&text=${post.title}`}
                  >
                    <FaTelegram
                      className="fill-gray-400 dark:fill-slate-500 dark:hover:fill-slate-400 hover:fill-gray-500 transition-all duration-300 cursor-pointer"
                      size={24}
                    />
                  </a>
                </div>
                <div className="relative">
                  <CopyToClipboard
                    text={`https://fronthooks.ir/posts/${post.slug}`}
                    onCopy={copyHandler}
                  >
                    <div className="cursor-pointer rounded-full px-4 py-2 flex gap-x-2 items-center border border-gray-400 dark:border-slate-500 dark:text-slate-400 text-xs text-gray-700">
                      <span>کپی&nbsp;لینک</span>

                      <MdContentCopy size={16} />
                    </div>
                  </CopyToClipboard>
                  {copied && (
                    <span className="bg-blue-500 absolute top-10 text-white rounded-full px-3 py-1">
                      کپی شد
                    </span>
                  )}
                </div>
              </div>
            </div>
            <hr className="mb-8 dark:bg-slate-700 bg-gray-500 h-0.5 border-0" />
            {/* author data */}
            <div className="flex items-stretch mb-20">
              <img
                className="w-12 h-12 md:w-14 md:h-14 rounded-full"
                src="/images/saheb.png"
                alt={post.author.name}
              />
              <div className="flex flex-col mr-4 justify-between">
                <div>
                  {" "}
                  <span className="font-extrabold text-base">
                    {post.author.name} {""}
                  </span>
                </div>
                <span className="font-normal text-xs">
                  {/* {post.author.biography} */}
                  برنامه نویس و توسعه دهنده وب
                </span>
              </div>
            </div>
          </section>
          {/* related blogs */}
          <section className="mb-16">
            <h3 className="mb-10 font-black text-2xl md:text-3xl">
              پست های مشابه
            </h3>
            <div className="flex flex-col md:flex-row gap-y-8 flex-wrap gap-x-4">
              {post.related.map((blog) => {
                return (
                  <div
                    className="rounded-[38px] p-2.5 flex flex-col gap-y-2 sm:flex-row justify-between items-stretch bg-white dark:bg-slate-800 ring-1 ring-slate-700/50"
                    key={blog._id}
                  >
                    <div className="py-2 pr-4 ml-2 flex items-center justify-center">
                      <Link href={`/posts/${blog.slug}`}>
                        <a>
                          <img
                            src="/images/logos_vue.png"
                            alt=""
                            className="w-24 h-24"
                          />
                        </a>
                      </Link>
                    </div>
                    <div className=" bg-myGray-200 rounded-[28px] px-3 py-2 dark:bg-slate-700">
                      <Link href={`/posts/${blog.slug}`}>
                        <a className="font-bold md:text-lg block mt-2 mb-4 hover:text-blue-600 transition-all duration-300 dark:text-gray-200">
                          {blog.title}
                        </a>
                      </Link>
                      <div className="flex justify-between items-center">
                        <div className="flex items-stretch">
                          <img
                            src="/images/saheb.png"
                            alt=""
                            className="w-9 h-9 rounded-full ring-white ring-1 ml-2"
                          />
                          <div className="flex flex-col text-xs">
                            <span className="block mb-1.5">
                              {blog.author.name}
                            </span>
                            <span className="block font-light">
                              <span> خواندن</span>
                              <span> {toArabicDigits(blog.readingTime)} </span>
                              <span>دقیقه </span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </section>
          {/* comment section */}
          <PostComments post={post} />
        </section>
      </Layout>
    </>
  );
};

export default PosePage;
