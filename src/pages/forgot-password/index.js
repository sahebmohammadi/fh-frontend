import Button from "@/common/Button";
import { useState } from "react";
import forgotPassword from "../../services/forgotPassswordService";
import Layout from "@/containers/Layout";
import Toast from "@/components/Toast";

const ForgotPassword = () => {
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState(null);
  const handleChange = (e) => setEmail(e.target.value);

  const handleSubmit = () => {
    forgotPassword(email)
      .then((res) => {
        // console.log(res.data.message);
        setMessage({ variant: "success", text: res.data.message });
      })
      .catch((err) => {
        if (err && err.response) {
          const message = err?.response?.data?.message;
          setMessage({ variant: "error", text: message });
        }
      });
  };
  return (
    <Layout>
      <section className="mx-auto sm:w-80 w-full">
        <p className="font-bold mb-4">ایمیل خود را وارد کنید</p>
        <input
          dir="ltr"
          type="email"
          className="my-4 text-left p-2 border text-sm rounded border-gray-200 outline-none w-full
          focus:outline-none focus:ring-2 focus:ring-primary focus:border-0"
          value={email}
          onChange={handleChange}
        />
        {message && <Toast variant={message.variant}>{message.text}</Toast>}
        <Button className="w-full" onClick={handleSubmit}>
          تایید
        </Button>
      </section>
    </Layout>
  );
};
export default ForgotPassword;
export function getStaticProps() {
  return {
    // returns the default 404 page with a status code of 404
    notFound: true,
  };
}
