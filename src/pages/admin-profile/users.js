import AdminLayout from "@/containers/AdminLayout";
import { useEffect, useState } from "react";
import getAllUsers from "@/services/getAllUsersService";
import withAdminAuth from "@/common/guard/withAdminAuth";

const Users = (props) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getAllUsers()
      .then(({ data }) => {
        // console.log(data.docs);
        setUsers(data.docs);
      })
      .catch((err) => console.log(err?.response?.data.message));
  }, []);

  return (
    <AdminLayout>
      <table className="text-center min-w-full divide-y divide-gray-200 ">
        <thead className="bg-gray-50">
          <tr className="dark:bg-slate-700 dark:text-slate-400">
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              #
            </th>
            <th>نام </th>
            <th> ایمیل</th>
            <th>نقش</th>
            <th>وضعیت</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-200 dark:bg-slate-800 dark:text-slate-400">
          {users &&
            users.map((user, index) => {
              return (
                <tr key={user._id}>
                  <td className="px-6 py-4"> {index + 1}</td>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.isAdmin ? "admin" : "member"}</td>
                  <td>Active</td>
                  <td>edit</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </AdminLayout>
  );
};

export default withAdminAuth(Users);
