import AdminLayout from "@/containers/AdminLayout";
import { useEffect, useState } from "react";
import getAllOrders from "@/services/getAllOrdersServices";
import toJalaliDate from "@/common/toJalaliDate";
import withAdminAuth from "@/common/guard/withAdminAuth";

const Orders = (props) => {
  const [orders, setOrders] = useState(null);

  useEffect(() => {
    getAllOrders()
      .then((res) => {
        setOrders(res.data);
      })
      .catch((err) => console.log(err?.response?.data.message));
  }, []);

  return (
    <AdminLayout>
      <table className="text-center min-w-full divide-y divide-gray-200">
        <thead className="bg-gray-50">
          <tr className="dark:bg-slate-700 dark:text-slate-400">
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              #
            </th>
            <th>نام محصول</th>
            <th>نام سفارش دهنده</th>
            <th>ایمیل سفارش دهندها</th>
            <th>تاریخ سفارش</th>
            <th>هزینه</th>
            <th>کد تخفیف</th>
            <th>تراکنش</th>
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-200 dark:bg-slate-800 dark:text-slate-400">
          {orders &&
            orders.map((order, i) => {
              return order.orderItems.map((orderItem, index) => {
                return (
                  <tr key={orderItem._id} className="">
                    <td className="px-6 py-4"> {i + 1}</td>
                    <td className="text-right">{orderItem.name}</td>
                    <td>{order?.user?.name}</td>
                    <td>{order?.user?.email}</td>
                    <td>{toJalaliDate(order.createdAt)}</td>
                    <td>
                      {order.coupon || order.totalPrice < orderItem.price
                        ? order.totalPrice
                        : orderItem.price}
                    </td>
                    <td>{order.coupon ? "coupon" : "-"}</td>
                    <td>{order.isPaid ? "موفق" : "پرداخت نشده"}</td>
                  </tr>
                );
              });
            })}
        </tbody>
      </table>
    </AdminLayout>
  );
};

export default withAdminAuth(Orders);
