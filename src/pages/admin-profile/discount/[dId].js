import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import editDiscount from "@/services/editDiscountService";
import { useSelector } from "react-redux";
import withAdminAuth from "@/common/guard/withAdminAuth";

const Discount = (props) => {
  const router = useRouter();
  const query = router.query;
  useEffect(() => {}, [router.query]);

  const [discount, setDiscount] = useState({
    startTime: "",
    duration: "",
    offPrice: "",
    discountValue: "",
  });

  const handlechange = (e) => {
    setDiscount({ ...discount, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    editDiscount(query.pId, {
      ...discount,
      _id: query.dId,
    })
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  };

  return (
    <>
      <form
        className="bg-white mt-10 flex flex-col p-4"
        onSubmit={handleSubmit}
      >
        <label className="w-1/4 flex justify-between mt-4">
          زمان شروع
          <input
            className="py-2 px-4 border-2 border-gray-800 rounded mr-4"
            type="text"
            name="startTime"
            value={discount.startTime}
            onChange={handlechange}
          />
        </label>
        <label className="w-1/4 flex justify-between mt-4">
          مدت زمان
          <input
            className="py-2 px-4 border-2 border-gray-800 rounded mr-4"
            type="text"
            name="duration"
            value={discount.duration}
            onChange={handlechange}
          />
        </label>
        <label className="w-1/4 flex justify-between mt-4">
          قیمت با تخفیف
          <input
            className="py-2 px-4 border-2 border-gray-800 rounded mr-4"
            type="number"
            name="offPrice"
            value={discount.offPrice}
            onChange={handlechange}
          />
        </label>
        <label className="w-1/4 flex justify-between mt-4">
          میزان تخفیف (درصد)
          <input
            className="py-2 px-4 border-2 border-gray-800 rounded mr-4"
            type="text"
            name="discountValue"
            value={discount.discountValue}
            onChange={handlechange}
          />
        </label>
        <button
          type="submit"
          className="bg-primary text-white p-4 w-20 mt-4 rounded"
        >
          تایید
        </button>
      </form>
    </>
  );
};

export default withAdminAuth(Discount);
