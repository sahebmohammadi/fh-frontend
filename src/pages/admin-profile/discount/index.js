import AdminLayout from "@/containers/AdminLayout";
import { useEffect, useState } from "react";
import getAllDiscounts from "@/services/getDiscountService";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import toJalaliDate from "@/common/toJalaliDate";
import Link from "next/link";
import withAdminAuth from "@/common/guard/withAdminAuth";

const Discounts = (props) => {
  const [discounts, setDiscounts] = useState(null);

  useEffect(() => {
    getAllDiscounts()
      .then((res) => {
        setDiscounts(res.data);
      })
      .catch((err) => console.log(err?.response?.data.message));
  }, []);

  return (
    <AdminLayout>
      <table className="text-center min-w-full divide-y divide-gray-200">
        <thead className="bg-gray-50">
          <tr className="text-right dark:bg-slate-700 dark:text-slate-400">
            <th
              scope="col"
              className="px-6 py-3  text-xs font-medium text-gray-500 uppercase tracking-wider text-right"
            >
              #
            </th>
            <th>نام محصول</th>
            <th>شروع تخفیف</th>
            <th>مدت زمان تخفیف</th>
            <th> قیمت با تخفیف</th>
            <th> ویرایش </th>
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-200 dark:bg-slate-800 dark:text-slate-400">
          {discounts &&
            discounts.map((discount, i) => {
              return (
                <tr key={discount._id} className="text-right">
                  <td className="px-6 py-4 text-right"> {i + 1}</td>
                  <td className="text-right">{discount.productId.name}</td>
                  <td>{toJalaliDate(discount.startTime)}</td>
                  <td>{discount.duration}</td>
                  <td>{discount.productId.offPrice}</td>
                  <td>
                    <Link
                      href={`/admin-profile/discount/${discount._id}/?pId=${discount.productId._id}`}
                    >
                      <a>ویرایش</a>
                    </Link>
                  </td>
                  {/* <td>{order.coupon ? order.totalPrice : orderItem.price}</td>
                  <td>{order.isPaid ? "موفق" : "پرداخت نشده"}</td> */}
                </tr>
              );
            })}
        </tbody>
      </table>
    </AdminLayout>
  );
};

export default withAdminAuth(Discounts);
