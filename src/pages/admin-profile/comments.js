import AdminLayout from "@/containers/AdminLayout";
import { useEffect, useState } from "react";
import getAllComments from "@/services/getAllCommentsService";
import { useSelector } from "react-redux";
import EditCommentModal from "@/components/Modal/Modal";
import { useRouter } from "next/router";
import toJalaliDate from "../../common/toJalaliDate";
import withAdminAuth from "@/common/guard/withAdminAuth";
const Comments = (props) => {
  const [comments, setComments] = useState(null);
  const [open, setOpen] = useState(false);
  const [commentId, setCommentId] = useState(false);

  useEffect(() => {
    getAllComments()
      .then((res) => {
        // console.log(res.data);
        setComments(res.data);
      })
      .catch((err) => console.log(err?.response?.data.message));
  }, []);

  const handleEdit = (id) => {
    setCommentId(id);
    setOpen(true);
  };

  return (
    <AdminLayout>
      <table className="text-center min-w-full divide-y divide-gray-200 ">
        <thead className="bg-gray-50  dark:bg-slate-800 dark:text-slate-500">
          <tr className="text-right dark:bg-slate-700 dark:text-slate-400">
            <th
              scope="col"
              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
            >
              #
            </th>
            <th>نویسنده </th>
            <th> تاریخ</th>
            <th>متن نظر</th>
            <th>وضعیت</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-200 dark:bg-slate-800 dark:text-slate-400">
          {comments &&
            comments.map((comment, index) => {
              return (
                <tr key={comment._id} className="text-right">
                  <td className="px-6 py-4"> {index + 1}</td>
                  <td>{comment.writer?.name}</td>
                  <td>{toJalaliDate(comment.createdAt)}</td>
                  <td>{comment.content}</td>
                  <td>
                    {comment.status == 1
                      ? "pending"
                      : comment.status == 2
                      ? "accepted"
                      : "rejected"}
                  </td>
                  <td>
                    <button
                      onClick={() => handleEdit(comment._id)}
                      className="border border-primary p-2 rounded"
                    >
                      edit
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
      <EditCommentModal open={open} setOpen={setOpen} commentId={commentId} />
    </AdminLayout>
  );
};

export default withAdminAuth(Comments);
