import AdminLayout from "@/containers/AdminLayout";
import withAdminAuth from "@/common/guard/withAdminAuth";

const AdminProfile = (props) => {
  return (
    <AdminLayout>
      <div>پنل مدیریتی ....</div>
    </AdminLayout>
  );
};

export default withAdminAuth(AdminProfile);
