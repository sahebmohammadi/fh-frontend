import Button from "@/common/Button";
import { useState } from "react";
import resetPassword from "../../services/resetPasswordService";
import Layout from "@/containers/Layout";
import Toast from "@/components/Toast";
import { useRouter } from "next/router";
import Link from "next/link";

const ResetPassword = () => {
  const router = useRouter();

  const [password, setpassword] = useState("");
  const [message, setMessage] = useState(null);
  const handleChange = (e) => setpassword(e.target.value);

  const handleSubmit = () => {
    resetPassword(password, router.query.token)
      .then((res) => {
        // console.log(res.data.message);
        setMessage({ variant: "success", text: res.data.message });
      })
      .catch((err) => {
        if (err && err.response) {
          const message = err?.response?.data?.message;
          setMessage({ variant: "error", text: message });
        }
      });
  };
  return (
    <Layout>
      <section className="mx-auto sm:w-80 w-full">
        <p className="font-bold mb-4"> پسورد جدید را وارد کنید</p>
        <input
          dir="ltr"
          // type="password"
          className="text-left px-4 py-2 w-full mb-4 border rounded"
          value={password}
          onChange={handleChange}
        />
        {message && <Toast variant={message.variant}>{message.text}</Toast>}
        <Button className="w-full" onClick={handleSubmit}>
          تایید
        </Button>
        {message && message.variant === "success" && (
          <Link href="/login">
            <a className="mt-4 block">میخوایی لاگین کنی؟</a>
          </Link>
        )}
      </section>
    </Layout>
  );
};
export default ResetPassword;

export function getStaticProps() {
  return {
    // returns the default 404 page with a status code of 404
    notFound: true,
  };
}
