import React from "react";
// setting up MUI theme :
import { ThemeProvider } from "next-themes";
import { ThemeProvider as MUiThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "../muitheme";
import { useEffect } from "react";
import { create } from "jss";
import rtl from "jss-rtl";
import { StylesProvider, jssPreset } from "@material-ui/core/styles";
// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

// styles:
import "tailwindcss/tailwind.css";
import "../../public/styles/globals.css";

// adding React Toastify
// setting up redux :
import { Provider, useSelector } from "react-redux";

// redux - persist
import { wrapper } from "@/redux/store";
import { useStore } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
import Script from "next/script";
// add all products to cart :
import { useDispatch } from "react-redux";

// check jwt
import { listMineOrder } from "@/redux/order/orderActions";
import http from "@/services/htppService";
import { USER_SIGNIN_SUCCESS } from "@/redux/user/userTypes";
import Head from "next/head";
import { Toaster } from "react-hot-toast";

function MyApp(props) {
  const { Component, pageProps } = props;
  const dispatch = useDispatch();
  const store = useStore((state) => state);
  const { userInfo } = useSelector((state) => state.userSignin);

  let persistor = persistStore(store);

  useEffect(() => {
    http
      .get("/user/user-data")
      .then(({ data: { user } }) => {
        store.dispatch({ type: USER_SIGNIN_SUCCESS, payload: user });
      })
      .catch((err) => {});
  }, []);

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles?.parentNode?.removeChild(jssStyles);
    }
  }, []);

  useEffect(() => {
    if (userInfo) {
      dispatch(listMineOrder());
    }
  }, [userInfo]);

  return (
    <React.Fragment>
      {/* <Script
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o+"?href="+window.location.href;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="c3d70483-2570-48e4-89a6-3cf773e5ef8d";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();`,
        }}
      /> */}
      <MUiThemeProvider theme={theme}>
        <ThemeProvider
          forcedTheme={Component.theme || undefined}
          attribute="class"
        >
          <CssBaseline />
          <StylesProvider jss={jss}>
            <Provider store={store}>
              <Toaster />
              <PersistGate
                persistor={persistor}
                loading={<Component {...pageProps} />}
              >
                <Component {...pageProps} />
              </PersistGate>
            </Provider>
          </StylesProvider>
        </ThemeProvider>
      </MUiThemeProvider>
    </React.Fragment>
  );
}

export default wrapper.withRedux(MyApp);
