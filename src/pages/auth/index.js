import { useState, useEffect, useRef } from "react";
import Head from "next/head";
import SingleFormLayout from "@/components/SingleFormLayout";
import Layout from "@/containers/Layout";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import Button from "@/common/Button";
import * as constants from "../../constants";
import { toArabicDigits } from "@/utils/toArabicNumbers";
import * as userAuth from "@/services/userAuthServicer";
import StepperComponent from "@/common/StepperComponent";
import ReCAPTCHA from "react-google-recaptcha";
import ReactCodeInput from "react-verification-code-input";
import toast from "react-hot-toast";
import { toEnglishDigits } from "@/utils/toEnglishDigits";
const RESEND_TIME = 2 * 60;

const MuiltiStepForm = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { userInfo } = useSelector((state) => state.userSignin);
  const [step, setStep] = useState(1);
  const [emailPhone, setEmailPhone] = useState("");
  const [codeResponse, setCodeResponse] = useState(null);
  const [verficationCode, setVerficationCode] = useState("");
  const [time, setTime] = useState(RESEND_TIME);
  const [loading, setLoading] = useState(false);
  const query = router.query.redirect;
  const redirect = query || "/";
  const recaptchaRef = useRef();

  useEffect(() => {
    if (redirect === "/") {
      if (userInfo) router.push("/");
    } else {
      if (userInfo?.name && userInfo?.email && userInfo?.phoneNumber) {
        router.push(`/checkout`);
      } else if (userInfo?.email || userInfo?.phoneNumber) {
        router.push("/complete-profile");
      }
    }
  }, [userInfo]);

  useEffect(() => {
    const timer =
      time > 0 && setInterval(() => setTime((time) => time - 1), 1000);
    return () => {
      if (timer) {
        clearInterval(timer);
      }
    };
  }, [time]);

  const resendNewCodeHandler = (e) => {
    sendCodeHandler(e);
  };

  const verficationCodeHandler = async (e) => {
    e.preventDefault();
    const recaptchaToken = await recaptchaRef.current.executeAsync();
    recaptchaRef.current.reset();

    if (!recaptchaToken) {
      toast.error("تیک گزینه «من ربات نیستم» را بزنید");
      return;
    }
    
    setLoading(true);

    userAuth
      .verifyCode(verficationCode, recaptchaToken)
      .then((res) => {
        const { name, email, phoneNumber, enrolledCourses } = res.data;
        const { isActivated, isUserExist } = res.data;
        toast.success(res.data.message);
        dispatch({
          type: "USER_SIGNIN_SUCCESS",
          payload: { name, email, phoneNumber, enrolledCourses },
        });

        if (redirect === "/") {
          router.push("/");
        } else {
          if (name && email && phoneNumber) {
            router.push(`/checkout`);
          } else {
            router.push("/complete-profile");
          }
        }
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        // console.log(err?.response?.data);
        if (err?.response?.data) {
          toast.error(err.response.data.message);
        }
      });
  };

  const sendCodeHandler = async (e) => {
    e.preventDefault();
    const recaptchaToken = await recaptchaRef.current.executeAsync();
    recaptchaRef.current.reset();

    if (!recaptchaToken) {
      toast.error("تیک گزینه «من ربات نیستم» را بزنید");
      return;
    }

    setLoading(true);
    userAuth
      .sendCode(emailPhone, recaptchaToken)
      .then((res) => {
        setLoading(false);
        setTime(RESEND_TIME);
        setCodeResponse(res.data);
        toast.success(res.data.message);
        setStep(2);
      })
      .catch((err) => {
        setLoading(false);
        if (err?.response?.data) {
          toast.error(err.response.data.message);
        }
      });
  };

  const verficationCodeChange = (e) => {
    setVerficationCode(e);
  };

  const renderStep = (step) => {
    switch (step) {
      case 1:
        return (
          <PhoneNumber
            emailPhone={emailPhone}
            setEmailPhone={setEmailPhone}
            sendCodeHandler={sendCodeHandler}
            time={time}
            recaptchaRef={recaptchaRef}
            changeTokenHandler={changeTokenHandler}
          />
        );
      case 2:
        return (
          <CodeValidation
            verficationCodeHandler={verficationCodeHandler}
            verficationCodeChange={verficationCodeChange}
            resendNewCodeHandler={resendNewCodeHandler}
            changeTokenHandler={changeTokenHandler}
            codeResponse={codeResponse}
            time={time}
            recaptchaRef={recaptchaRef}
          />
        );
      default:
        return null;
    }
  };

  const changeTokenHandler = async () => {
    // recaptchaRef.current.reset();
    // const recaptchaToken = await recaptchaRef.current.executeAsync();
  };

  return (
    <Layout>
      {redirect !== "/" && <StepperComponent activeStep={0} />}
      <Head>
        <title> ورود / ثبت نام | فرانت هوکس</title>
      </Head>
      <SingleFormLayout
        title={"ورود / ثبت نام"}
        loading={loading}
        step={step}
        setStep={setStep}
      >
        {renderStep(step)}
      </SingleFormLayout>
    </Layout>
  );
};

export default MuiltiStepForm;

const PhoneNumber = ({
  sendCodeHandler,
  emailPhone,
  setEmailPhone,
  recaptchaRef,
}) => {
  return (
    <form onSubmit={sendCodeHandler} className="flex flex-col">
      <div>
        <label
          className="flex flex-row mb-4 text-sm text-gray-500 dark:text-slate-300"
          htmlFor="emailPhone"
        >
          شماره موبایل یا ایمیل خود را وارد کنید
        </label>
        <input
          dir="ltr"
          className="mb-2 text-left border p-2 text-sm rounded border-gray-200 outline-none w-full
           focus:outline-none focus:ring-2 focus:ring-primary focus:border-transparent dark:bg-transparent dark:border-slate-500 dark:focus:border-transparent"
          type="text"
          id="emailPhone"
          name="emailPhone"
          onChange={(e) => setEmailPhone(toEnglishDigits(e.target.value))}
          value={emailPhone}
        />
      </div>
      <div className="my-4 flex justify-center">
        <ReCAPTCHA
          ref={recaptchaRef}
          sitekey="6LecLSIhAAAAANvjK1NW6ABQuWMLjB2q5LLyOg6A"
          hl="fa"
          size="invisible"
        />
      </div>
      <Button type="submit" className="w-full my-4 py-4">
        ورود به فرانت هوکس
      </Button>
    </form>
  );
};

const CodeValidation = ({
  verficationCodeChange,
  verficationCodeHandler,
  resendNewCodeHandler,
  codeResponse,
  time,
  recaptchaRef,
}) => {
  const resendNewCodeSection = () => {
    return (
      <div className="mb-8 mt-8 text-center text-xs dark:text-slate-500">
        {time > 0 ? (
          <p>ارسال مجدد کد تا {toArabicDigits(time)} ثانیه دیگر</p>
        ) : (
          <p
            onClick={resendNewCodeHandler}
            className="cursor-pointer py-4 text-xs text-primary"
          >
            دریافت مجدد کد تایید
          </p>
        )}
      </div>
    );
  };

  return (
    <form onSubmit={verficationCodeHandler} className="flex flex-col">
      <p className="text-gray-400 text-xs dark:text-slate-400">
        {codeResponse ? codeResponse.message : <span>&nbsp;&nbsp;</span>}
      </p>
      <div className="text-gray-700 font-bold my-4 dark:text-slate-300">
        کد تایید را وارد کنید
      </div>
      <div
        className="custom-styles text-center my-2 flex justify-center"
        dir="ltr"
      >
        <ReactCodeInput
          fields={6}
          type="number"
          onChange={verficationCodeChange}
          fieldWidth={40}
          fieldHeight={40}
          autoFocus={true}
        />
      </div>
      {resendNewCodeSection()}
      <div className="my-4 flex justify-center">
        <ReCAPTCHA
          ref={recaptchaRef}
          sitekey="6LecLSIhAAAAANvjK1NW6ABQuWMLjB2q5LLyOg6A"
          hl="fa"
          size="invisible"
        />
      </div>
      <Button
        disabled={!time}
        type="submit"
        className="py-4 rounded outline-none w-full text-white 
               bg-primary focus:outline-none mb-4"
      >
        تایید و ادامه
      </Button>
    </form>
  );
};
