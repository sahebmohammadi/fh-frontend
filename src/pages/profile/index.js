import Toast from "@/components/Toast";
import UserLayout from "@/containers/UserLayout";
import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import withAuth from "@/common/guard/withAuth";
function isBrowser() {
  return typeof window !== "undefined";
}

const Profile = () => {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;

  const router = useRouter();

  useEffect(() => {
    if (!userInfo && isBrowser) {
      router.push("/auth");
    }
  }, [userInfo]);

  return (
    <UserLayout>
      <Head>
        <title>اطلاعات کاربری </title>
      </Head>
      <Toast variant="success">
        سلام، {userInfo?.name}
        <br />
        فایل‌های دوره‌هایی که ثبت نام کرده‌اید در بخش «دوره های من» قابل مشاهده
        است. فایل دوره های غیر رایگان از سایت قابل دانلود نیست.
      </Toast>
      <p className="leading-7 bg-green-100 text-green-700 p-4 rounded">
        توجه: اگر در دوره های
        <strong> غیر رایگان </strong>
        ثبت نام کرده اید، لطفا مطابق ایمیل دریافتی تحت عنوان لایسنس دسترسی دوره
        ها اقدام کنید.
      </p>
      <a
        target="_blank"
        href="https://t.me/Fronthooks_support"
        className="mt-4 text-blue-600 py-2 block"
      >
        پیام به پشتیبانی تلگرام؟
      </a>
    </UserLayout>
  );
};

export default withAuth(Profile);
