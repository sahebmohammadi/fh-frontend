import { useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { listMineOrder } from "@/redux/order/orderActions";
import downloadEpisode from "@/services/downloadEpisodeService";
import UserLayout from "@/containers/UserLayout";
import { useRouter } from "next/router";
import toArabicNumber from "@/utils/toArabicNumbers";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CircularProgress from "@material-ui/core/CircularProgress";
import Toast from "@/components/Toast";
import withAuth from "@/common/guard/withAuth";

function getFileName(str) {
  return str.substring(str.lastIndexOf("/") + 1);
}
function isBrowser() {
  return typeof window !== "undefined";
}

const Downloads = () => {
  const router = useRouter();
  const dlLinkRef = useRef();
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;

  const { orders, loading, error } = useSelector(
    (state) => state.orderMineList
  );
  const [expanded, setExpanded] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    if (!userInfo && !userInfo?.token && isBrowser) {
      router.push("/auth");
    }
  }, [userInfo]);

  const downloadHandler = async (episodeId) => {
    downloadEpisode(episodeId)
      .then((res) => {
        const dlLink = res.data;
        dlLinkRef.current.href = dlLink;
        dlLinkRef.current.setAttribute("download", getFileName(dlLink));
        dlLinkRef.current.click();
        dlLinkRef.current.href = "";
      })
      .catch((err) => console.log(err));
  };

  if (loading && !error) {
    return (
      <UserLayout>
        <div className="py-4 px-8">
          <CircularProgress color="primary" />
        </div>
      </UserLayout>
    );
  }
  return (
    <UserLayout>
      {!loading && error && <Toast>{error}</Toast>}
      <a ref={dlLinkRef} href="" download="" className="hidden"></a>
      {orders &&
        orders.orderItems &&
        orders.orderItems.map((orderItem, index) => {
          return (
            <Accordion
              key={orderItem.name}
              className="border-none border-gray-200 rounded-2xl outline-none z-10 dark:bg-slate-800 dark:border-slate-700"
              expanded={expanded === `panel${index + 1}`}
              onChange={handleChange(`panel${index + 1}`)}
            >
              <AccordionSummary
                expandIcon={<ExpandMoreIcon className="dark:text-slate-400" />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
              >
                <div className="font-bold text-gray-700 dark:text-slate-400 rounded p-2 mb-1">
                  {orderItem.name}
                </div>
              </AccordionSummary>
              <AccordionDetails>
                <div className="w-full">
                  {orderItem.episodes.map((episode, index) => {
                    return episode.isSeasion ? (
                      <SeasionTitle
                        {...episode}
                        index={index}
                        key={episode._id}
                      />
                    ) : (
                      <section
                        key={episode._id}
                        onClick={() => downloadHandler(episode._id)}
                      >
                        <DLLink {...episode} index={index} key={episode._id} />
                      </section>
                    );
                  })}
                </div>
              </AccordionDetails>
            </Accordion>
          );
        })}
    </UserLayout>
  );
};

export default withAuth(Downloads);

const DLLink = (props) => {
  return (
    <div className="flex justify-between items-center mb-4 text-gray-700 dark:text-slate-400  rounded text-sm hover:bg-primary dark:hover:bg-slate-600 hover:text-white py-2 cursor-pointer">
      <div className="flex items-center">
        <span className="ml-4 p-2 bg-primary text-white rounded-full w-8 h-8 flex justify-center items-center">
          {toArabicNumber(props.index + 1)}
        </span>
        <p> {props.title}</p>
      </div>

      <div className="flex items-center">
        <div className="text-xs mx-2 sm:mx-4">
          {toArabicNumber(props.duration.split(" ")[0])} دقیقه
        </div>
      </div>
    </div>
  );
};

const SeasionTitle = (props) => {
  return (
    <div className="flex items-center mb-4 bg-gray-100 text-gray-700 dark:text-slate-200 dark:bg-slate-500  font-bold rounded text-sm py-2">
      <span className=" mr-1 ml-4 p-2 bg-gray-500 dark:bg-slate-600 text-white rounded-full w-8 h-8 flex justify-center items-center">
        {toArabicNumber(props.index + 1)}
      </span>
      <p> {props.title}</p>
    </div>
  );
};
