import { ADD_TO_CART, SUB_QUANTITY, CLEAR_CART } from "./cartTypes";

//add cart action
export const addToCart = (product) => {
  return {
    type: ADD_TO_CART,
    payload: product,
  };
};

//subtract qt action
export const subtractQuantity = (product) => {
  return {
    type: SUB_QUANTITY,
    payload: product,
  };
};

export const clearCart = () => {
  return {
    type: CLEAR_CART,
  };
};
