import { ADD_TO_CART, SUB_QUANTITY, CLEAR_CART } from "./cartTypes";

const initalState = {
  addedItems: [],
  total: 0,
};

const cartReducer = (state = initalState, { payload, type }) => {
  switch (type) {
    case ADD_TO_CART: {
      const updatedCart = [...state.addedItems];
      const updatedItemIndex = updatedCart.findIndex(
        (item) => item.id === payload.id
      );

      if (updatedItemIndex < 0) {
        updatedCart.push({ ...payload, quantity: 1 });
      } else {
        const updatedItem = { ...updatedCart[updatedItemIndex] };
        updatedItem.quantity++;
        updatedCart[updatedItemIndex] = updatedItem;
      }
      return {
        addedItems: updatedCart,
        total: state.total + payload.offPrice,
      };
    }
    case SUB_QUANTITY: {
      const updatedCart = [...state.addedItems];
      const updatedItemIndex = updatedCart.findIndex(
        (item) => item.id === payload.id
      );
      const updatedItem = { ...updatedCart[updatedItemIndex] };
      if (updatedItem.quantity === 1) {
        const filteredCart = updatedCart.filter(
          (item) => item.id !== payload.id
        );
        return {
          addedItems: filteredCart,
          total: state.total - payload.offPrice,
        };
      } else {
        updatedItem.quantity--;
        updatedCart[updatedItemIndex] = updatedItem;
        return {
          addedItems: updatedCart,
          total: state.total - payload.offPrice,
        };
      }
    }
    case CLEAR_CART: {
      localStorage.removeItem("persist:root");
      return {
        addedItems: [],
        total: 0,
      };
    }
    default:
      return state;
  }
};

export default cartReducer;
