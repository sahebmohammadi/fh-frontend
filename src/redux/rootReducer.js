import { combineReducers } from "redux";

//  cart
import cartReducer from "./cart/cartReducer";

// user :
import {
  userRegisterReducer,
  userSigninReducer,
  userVerifyReducer,
} from "./user/userReducer";

import { orderCreateReducer, orderMineListReducer } from "./order/orderReducer";

const rootReducer = combineReducers({
  cart: cartReducer,
  userVerify: userVerifyReducer,
  userSignin: userSigninReducer,
  userRegister: userRegisterReducer,
  orderCreate: orderCreateReducer,
  orderMineList: orderMineListReducer,
});

export default rootReducer;
