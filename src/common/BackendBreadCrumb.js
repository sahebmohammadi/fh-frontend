import { Breadcrumbs } from "@material-ui/core";
import Link from "next/link";

const BackendBreadcrumbs = ({ breadCrumbs }) => {
  return (
    <div className="overflow-auto p-2  mb-5 text-sm">
      <Breadcrumbs aria-label="Breadcrumb" separator="›">
        {breadCrumbs.map((bread, index) => {
          if (breadCrumbs.length - 1 == index) {
            return (
              <span
                className="text-myGray-500 text-sm font-bold md:text-base dark:text-slate-300"
                key={index}
              >
                {bread.title}
              </span>
            );
          }
          return (
            <Link href={bread.url} key={index}>
              <a className="text-myGray-400 dark:text-slate-400 dark:hover:text-sky-500 text-sm md:text-base hover:underline hover:text-blue-600">
                {bread.title}
              </a>
            </Link>
          );
        })}
      </Breadcrumbs>
    </div>
  );
};

export default BackendBreadcrumbs;
