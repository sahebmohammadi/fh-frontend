import { makeStyles } from "@material-ui/core";
import { PaginationItem } from "@material-ui/lab";
import Pagination from "@material-ui/lab/Pagination";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .Mui-selected": {
      backgroundColor: "",
      color: "",
    },
    "& .MuiPaginationItem-icon": {
      color: "#94a3b8",
    },
    "& ul > li:not(:first-child):not(:last-child) > button:not(.Mui-selected)": {
      backgroundColor: "transparent",
      color: "#64748b",
    },
    "& .Mui-disabled": {
      backgroundColor: "red",
      color: "#fff",
    },
  },
}));

export default function BasicPagination({
  totalPages,
  pageHandler,
  currentPage,
}) {
  const classes = useStyles();
  return (
    <Pagination
      // className="text-slate-100"
      count={totalPages}
      color="primary"
      className={classes.root}
      onChange={pageHandler}
      page={currentPage}
      renderItem={(item) => (
        <PaginationItem {...item} components={{}} className="text-slate-100" />
      )}
    />
  );
}
