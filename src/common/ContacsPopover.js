import { Popover, Transition } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { Fragment } from "react";
import Link from "next/link";

const solutions = [
  {
    name: "درباره ما",
    href: "/about-us",
  },
  {
    name: "کانال تلگرام",
    href: "https://t.me/fronthooks",
  },
  {
    name: "صفحه اینستاگرام",
    href: "https://instagram.com/sahebmohamadi.ir",
  },
];

export default function ContactsPopover() {
  return (
    <div className="relative">
      <Popover className="">
        {({ open }) => (
          <>
            <Popover.Button
              className={`
                ${open ? "" : "text-opacity-90"}
                group inline-flex items-center rounded-md text-sm font-medium text-gray-700 hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 dark:text-slate-300 dark:hover:text-slate-200`}
            >
              <span>ارتباط با ما</span>
              <ChevronDownIcon
                className={`${open ? "" : "text-opacity-70"}
                  ml-2 h-5 w-5 text-gray-500 transition duration-150 ease-in-out group-hover:text-opacity-80 dark:text-slate-400 mr-0.5`}
                aria-hidden="true"
              />
            </Popover.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="opacity-0 translate-y-1"
              enterTo="opacity-100 translate-y-0"
              leave="transition ease-in duration-150"
              leaveFrom="opacity-100 translate-y-0"
              leaveTo="opacity-0 translate-y-1"
            >
              <Popover.Panel
                className="md:absolute md:-right-4 z-10 mt-3 md:w-[240px] 
                transform md:px-4 lg:max-w-3xl"
              >
                <div className="md:overflow-hidden rounded-lg md:shadow-lg md:ring-1 md:ring-black md:ring-opacity-5">
                  <div className="relative flex  flex-col gapx-y-6 md:gap-y-8 gap-8 bg-white p-2 md:p-6 dark:bg-slate-700 dark:border-slate-500/70 dark:text-slate-400">
                    {solutions.map((item) => (
                      <Link href={item.href} key={item.name}>
                        <a className="-m-3 flex items-center rounded-lg p-2 transition duration-150 ease-in-out hover:bg-gray-50 dark:hover:bg-slate-600 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50">
                          <div className="mr-1 md:mr-4">
                            <p className="text-sm font-medium text-gray-900 dark:text-slate-200">
                              <span className="text-sm md:text-base">
                                {item.name}
                              </span>
                            </p>
                          </div>
                        </a>
                      </Link>
                    ))}
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    </div>
  );
}
