import { useMediaQuery } from "@material-ui/core";
import Drawer from "@material-ui/core/Drawer";
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function DrawerComponent({ children, isOpen, setIsOpen }) {
  const router = useRouter();
  const matches = useMediaQuery("(min-width:768px)");

  if (matches) {
    setIsOpen(false);
  }

  useEffect(() => {
    if (isOpen) {
      setIsOpen(!isOpen);
    }
  }, [router.asPath]);

  return (
    <div>
      <Drawer open={isOpen} onClose={() => setIsOpen(false)}>
        <div
          className=" bg-white dark:bg-slate-700 dark:text-slate-300 
             w-[312px] h-full p-4 overflow-y-auto"
        >
          {children}
        </div>
      </Drawer>
    </div>
  );
}
