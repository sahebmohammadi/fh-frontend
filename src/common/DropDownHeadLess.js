import {
  MoonIcon,
  SunIcon,
  DesktopComputerIcon,
  ChevronDownIcon,
} from "@heroicons/react/outline";
import { useTheme } from "next-themes";
import { Menu, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";

const themeItems = [
  {
    title: "تم تاریک",
    en: "dark",
    icon: <MoonIcon className="stroke-current" />,
  },
  {
    title: "تم روشن",
    en: "light",
    icon: <SunIcon className="stroke-current" />,
  },
  {
    title: "سیستم",
    en: "system",
    icon: <DesktopComputerIcon className="stroke-current" />,
  },
];

export default function ToggleThme() {
  const { theme, setTheme } = useTheme();
  if (theme === undefined) return null;

  return (
    <div className="relative inline-block text-right">
      <Menu as="div">
        <Menu.Button className="">
          {theme === "dark" ? (
            <span className="moon">
              <MoonIcon className="text-blue-500 w-7 h-7" />
            </span>
          ) : (
            <span className="sun">
              <SunIcon className="text-blue-600 w-7 h-7" />
            </span>
          )}
        </Menu.Button>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items
            className="absolute z-50  bg-white rounded-lg ring-1 ring-slate-200 shadow-lg overflow-hidden py-1 text-sm text-slate-700 font-semibold dark:bg-slate-800  dark:highlight-white/5 dark:text-slate-300 left-0 mr-4 top-12 dark:ring-slate-500 mt-2"
            style={{ width: "144px" }}
            as="ul"
          >
            <div>
              {themeItems.map((item) => {
                return (
                  <Menu.Item key={item.en}>
                    {({ active }) => (
                      <li
                        className={`py-1 flex items-center px-2 cursor-pointer ${
                          active ? "bg-gray-100 dark:bg-slate-700" : ""
                        }`}
                        onClick={() => setTheme(item.en)}
                      >
                        <span className="ml-2 w-6 h-6 stroke-current block">
                          {item.icon}
                        </span>
                        <span>{item.title}</span>
                      </li>
                    )}
                  </Menu.Item>
                );
              })}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
}

export function MobileThmeToggler() {
  const [isShowMenu, setIsShowMenu] = useState(false);
  const { theme, setTheme } = useTheme();
  const [selectedThme, setSelectedThme] = useState(themeItems[0]);
  useEffect(() => {
    const selectedThme = themeItems.find((t) => t.en === theme);
    setSelectedThme(selectedThme);
  }, [theme]);

  return (
    <div className="w-full mt-2 px-3 pb-4">
      <hr className="h-0 border-t dark:border-slate-500 mb-4" />
      <div className="flex items-center justify-between">
        <label>انتخاب تم</label>
        <div className="relative flex items-center ring-1 ring-slate-900/10 rounded-lg shadow-sm p-2 text-slate-700 font-semibold dark:bg-slate-600 dark:ring-0 dark:highlight-white/5 dark:text-slate-200 text-sm">
          <span className="w-4 h-4 ml-1">{selectedThme.icon}</span>
          <span className="text-xs ml-1">{selectedThme.title}</span>
          <ChevronDownIcon className="w-4 h-4" />
          <select
            dir="ltr"
            value={theme}
            onChange={({ target }) => setTheme(target.value)}
            className="absolute inset-0 w-full h-full opacity-0"
          >
            {themeItems.map((item) => {
              return (
                <option
                  key={item.en}
                  value={item.en}
                  className="text-right w-full dark:bg-slate-600 dark:text-slate-300"
                >
                  {item.title}
                </option>
              );
            })}
          </select>
        </div>
      </div>
    </div>
  );
}
