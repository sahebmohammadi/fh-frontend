import { Popover, Transition } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { Fragment } from "react";
import { MdWebAsset } from "react-icons/md";
import Link from "next/link";
import { SiNextdotjs, SiReact, SiTailwindcss } from "react-icons/si";
import { IoLogoJavascript } from "react-icons/io";
import { HiCode } from "react-icons/hi";

const solutions = [
  {
    name: "دوره متخصص ریکت و ریداکس",
    description: "آموزش پروژه محور React و Redux",
    href: "/courses/react-course",
    icon: ReactIcon,
  },
  {
    name: "دوره متخصص Next.js",
    description: "آموزش پروژه محور نکست جی اس تا سطح  پیشرفته",
    href: "/courses/nextjs",
    icon: NextIcon,
  },
  {
    name: "دوره جامع و پیشرفته جاوااسکریپت",
    description: "آموزش پروژه محور JavaScript از صفر تا صد",
    href: "/courses/advanced-javascript",
    icon: JavascriptIcon,
  },
  {
    name: "دوره پروژه محور تیلویند",
    description: "آموزش پروژه محور TailwindCSS",
    href: "/courses/tailwindCSS",
    icon: TailwindIcon,
  },
  {
    name: "دوره طراحی وب رسپانسیو",
    description: "آموزش اصولی HTML CSS از صفر تا پیشرفته",
    href: "/courses/responsive-web",
    icon: HTMLCSSIcon,
  },
  {
    name: "دوره حرفه ای VS Code",
    description: "با محیط کدنویسی VS Code کامل آشنا شو",
    href: "/courses/vs-code",
    icon: VScodeIcon,
    isFree: true,
  },
];

export default function CoursesPopover() {
  return (
    <div className="relative">
      <Popover className="">
        {({ open }) => (
          <>
            <Popover.Button
              className={`
                ${open ? "" : "text-opacity-90"}
                group inline-flex items-center rounded-md text-sm font-medium text-gray-700 hover:text-opacity-100 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 dark:text-slate-300 dark:hover:text-slate-200`}
            >
              <span>دوره های آموزشی</span>
              <ChevronDownIcon
                className={`${open ? "" : "text-opacity-70"}
                  ml-2 h-5 w-5 text-gray-500 transition duration-150 ease-in-out group-hover:text-opacity-80 dark:text-slate-400 mr-0.5`}
                aria-hidden="true"
              />
            </Popover.Button>
            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="opacity-0 translate-y-1"
              enterTo="opacity-100 translate-y-0"
              leave="transition ease-in duration-150"
              leaveFrom="opacity-100 translate-y-0"
              leaveTo="opacity-0 translate-y-1"
            >
              <Popover.Panel className="md:absolute md:-right-4 z-10 mt-3 md:w-screen md:max-w-sm  transform md:px-4 lg:max-w-3xl">
                <div className="md:overflow-hidden rounded-lg md:shadow-lg md:ring-1 md:ring-black md:ring-opacity-5">
                  <div className="relative grid gap-8 bg-white py-2 md:p-7 lg:grid-cols-2 dark:bg-slate-700 dark:border-slate-500/70 dark:text-slate-400">
                    {solutions.map((item) => (
                      <Link href={item.href} key={item.name}>
                        <a className="-m-3 flex items-center rounded-lg p-2 transition duration-150 ease-in-out hover:bg-gray-50 dark:hover:bg-slate-600 focus:outline-none focus-visible:ring focus-visible:ring-orange-500 focus-visible:ring-opacity-50">
                          <div className="flex h-10 w-10 shrink-0 items-center justify-center text-white sm:h-12 sm:w-12">
                            <item.icon aria-hidden="true" />
                          </div>
                          <div className="mr-1 md:mr-4">
                            <p className="text-sm font-medium text-gray-900 dark:text-slate-200 mb-2">
                              <span className="text-xs md:text-base">
                                {item.name}
                              </span>
                              {item.isFree && (
                                <span className="bg-red-500 text-xs text-red-100 mr-2 px-3 py-0.5 rounded-full">
                                  رایگان
                                </span>
                              )}
                            </p>
                            <p className="text-xs text-gray-500 dark:text-slate-400">
                              {item.description}
                            </p>
                          </div>
                        </a>
                      </Link>
                    ))}
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    </div>
  );
}

function ReactIcon() {
  return (
    <div className="rounded py-1 bg-blue-200 h-6 w-6 md:w-10 md:h-10">
      <SiReact className="text-blue-500 w-full h-full" />
    </div>
  );
}

function JavascriptIcon() {
  return (
    <div className="rounded py-1 bg-yellow-50 h-6 w-6 md:w-10 md:h-10">
      <IoLogoJavascript className="text-yellow-500 w-full h-full" />
    </div>
  );
}

function TailwindIcon() {
  return (
    <div className="rounded py-1 bg-slate-200 h-6 w-6 md:w-10 md:h-10">
      <SiTailwindcss className="text-slate-600 w-full h-full" />
    </div>
  );
}

function NextIcon() {
  return (
    <div className="rounded py-1 bg-gray-200 h-6 w-6 md:w-10 md:h-10">
      <SiNextdotjs className="text-gray-700 z-10 relative w-full h-full" />
    </div>
  );
}
function HTMLCSSIcon() {
  return (
    <div className="rounded py-1 bg-red-200 h-6 w-6 md:w-10 md:h-10">
      <MdWebAsset className="text-red-600 z-10 relative w-full h-full " />
    </div>
  );
}
function VScodeIcon() {
  return (
    <div className="rounded py-1 bg-violet-200 h-6 w-6 md:w-10 md:h-10">
      <HiCode className="text-violet-600 z-10 relative w-full h-full " />
    </div>
  );
}
