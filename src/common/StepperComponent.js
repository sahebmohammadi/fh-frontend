import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";

const StepperComponent = ({ activeStep }) => {
  return (
    <Stepper
      activeStep={activeStep}
      alternativeLabel
      style={{ backgroundColor: "transparent" }}
      // className="bg-transparent"
    >
      <Step>
        <StepLabel>
          <span className="dark:text-slate-500">احراز هویت</span>
        </StepLabel>
      </Step>
      <Step>
        <StepLabel>
          <span className="dark:text-slate-500"> تکمیل اطلاعات </span>
        </StepLabel>
      </Step>
      <Step>
        <StepLabel>
          <span className="dark:text-slate-500"> ثبت سفارش </span>
        </StepLabel>
      </Step>
    </Stepper>
  );
};

export default StepperComponent;
