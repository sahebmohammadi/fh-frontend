import { CgClose } from "react-icons/cg";

const BottomSheet = ({ isOpenSheet, title, setIsOpenSheet, children }) => {
  return (
    <div className="bottom-sheet md:hidden">
      {/* backdrop section */}
      <div
        className="fixed left-0 w-full h-full top-0 bg-gray-200 dark:bg-slate-800 z-20"
        onClick={() => setIsOpenSheet(false)}
        style={{
          display: isOpenSheet ? "block" : "none",
          backgroundColor: "rgba(0,0,0,0.4)",
        }}
      ></div>
      {/* bottom sheet */}
      <div
        className="fixed bottom-0 left-0 right-0 dark:bg-slate-700  bg-white border max-h-0 max-w-full transform z-50 w-full dark:border-slate-500 transition-all duration-400 ease-in-out"
        style={{
          maxHeight: isOpenSheet ? "100%" : "0",
          borderRadius: "24px 24px 0 0",
        }}
      >
        <div className="p-4">
          {/* sheet header */}
          <div className="flex items-center justify-center mb-4">
            <h3 className="flex-1 text-center font-bold text0-lg">{title}</h3>
            <button onClick={() => setIsOpenSheet(false)}>
              <CgClose size={20} className="text-gray-400" />
            </button>
          </div>
          {/* sheet content */}
          {children}
        </div>
      </div>
    </div>
  );
};

export default BottomSheet;
