import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import * as userAuth from "@/services/userAuthServicer";

const withAdminAuth = (WrappedComponent) => {
  const UpdatedComponent = (props) => {
    const Router = useRouter();
    const [isVerified, setIsVerified] = useState(false);

    useEffect(() => {
      // const token = JSON.parse(localStorage.getItem("token"));
      // if no token was found,then we redirect to "/" page.

      // if (!token) {
      //   Router.replace("/");
      // } else {
      // we call the api that verifies the token.
      userAuth
        .verifyToken()
        .then((res) => {
          const { role, success } = res.data;
          // if token was verified we set the state.
          if (role === "Admin" && success === "OK") {
            setIsVerified(true);
          } else {
            Router.push("/");
          }
        })
        .catch((err) => {
          // If the token was fraud we first remove it from localStorage and then redirect to "/"
          localStorage.removeItem("token");
          Router.replace("/");
          setIsVerified(false);
        });
      // }
    }, []);

    if (isVerified) {
      return <WrappedComponent {...props} />;
    } else {
      return null;
    }
  };

  return UpdatedComponent;
};

export default withAdminAuth;
