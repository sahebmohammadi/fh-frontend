import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import * as userAuth from "@/services/userAuthServicer";
import CircularProgress from "@material-ui/core/CircularProgress";
import UserLayout from "@/containers/UserLayout";
import { useSelector } from "react-redux";

const withAuth = (WrappedComponent) => {
  const UpdatedComponent = (props) => {
    const { userInfo } = useSelector((state) => state.userSignin);
    const Router = useRouter();
    const [isVerified, setIsVerified] = useState(false);

    useEffect(() => {
      // const token = JSON.parse(localStorage.getItem("token"));
      // if no token was found,then we redirect to "/" page.

      // if (!userInfo) {
      //   // Router.replace("/");
      // } else {
      // we call the api that verifies the token.
      
      userAuth
        .verifyToken()
        .then((res) => {
          const { role, success } = res.data;
          // if token was verified we set the state.
          if (
            (role === "USER" || role === "Admin") &&
            (success === true || success === "OK")
          )
            setIsVerified(true);
        })
        .catch((err) => {
          // If the token was fraud we first remove it from localStorage and then redirect to "/"
          localStorage.removeItem("token");
          Router.replace("/");
          setIsVerified(false);
        });
      // }
    }, []);

    if (isVerified) {
      return <WrappedComponent {...props} />;
    } else {
      return (
        <UserLayout>
          <div className="py-4 px-8">
            <CircularProgress color="primary" />
          </div>
        </UserLayout>
      );
    }
  };

  return UpdatedComponent;
};

export default withAuth;
