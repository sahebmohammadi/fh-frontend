import Link from "next/link";
const GoToLearn = ({ className }) => {
  return (
    <Link href="/profile/downloads">
      <a className="block w-full">
        <button
          className={`w-full py-2 px-2 rounded-lg bg-gradient-to-l text-primary border-2
            border-primary bg-white dark:bg-slate-500 dark:border-slate-400 dark:text-slate-100 dark:hover:text-sky-400 text-xs dark:hover:bg-slate-600 ${className}`}
        >
          شما دانشجوی این دوره هستید ، ادامه یادگیری؟
        </button>
      </a>
    </Link>
  );
};

export default GoToLearn;
