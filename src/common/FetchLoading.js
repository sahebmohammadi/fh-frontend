// import { CircularProgress } from "@material-ui/core";

// const FetchLoading = ({ isRefreshing }) => {
//   return (
//     <div
//       className={`absolute w-full h-full inset-0 backdrop-blur-sm  bg-white/30 cursor-none ${
//         isRefreshing ? "block" : "hidden"
//       }`}
//     >
//       <div className="w-full h-full inset-0 flex items-center justify-center">
//         <div className="">
//           <CircularProgress color="primary" size={64} />
//         </div>
//       </div>
//     </div>
//   );
// };

// export default FetchLoading;

import ContentLoader from "react-content-loader";
import { useTheme } from "next-themes";
const YoutubeFresh = ({ isRefreshing, ...props }) => {
  const { theme, setTheme } = useTheme();
  return [1, 2, 3].map((index) => {
    return (
      <ContentLoader
        // viewBox="0 0 500 420"
        height={480}
        width={470}
        rtl
        backgroundColor={theme && theme === "dark" ? "#333" : "#fff"}
        {...props}
        className="col-span-6  md:col-span-3 lg:col-span-2 w-full h-full"
      >
        <rect x="0" y="0" rx="48" ry="48" height="220" className="w-full" />
        <circle cx="35" cy="248" r="20" />
        <rect x="69" y="229" rx="2" ry="2" width="275" height="15" />
        <rect x="69" y="253" rx="2" ry="2" width="140" height="15" />
      </ContentLoader>
    );
  });
};

YoutubeFresh.metadata = {
  name: "Costal Oktopus",
  github: "coktopus", // Github username
  description: "Youtube fresh",
  filename: "YoutubeFresh", // filename of your loader
};

export default YoutubeFresh;
