import { Breadcrumbs, Typography } from "@material-ui/core";
import router, { useRouter } from "next/router";
import Link from "next/link";

const LinkRouter = (props) => (
  <Link {...props}>
    <a>{props.children}</a>
  </Link>
);

const SimpleBreadcrumbs = () => {
  const router = useRouter();

  const dynamicParts = () => {
    const pathnames = router.asPath.split("/").filter((x) => x);
    return pathnames.map((value, index) => {
      const last = index === pathnames.length - 1;
      const to = `/${pathnames.slice(0, index + 1).join("/")}`;

      // Split value so the string can be transformed and parsed later.
      // const path = value.split("-");
      // Convert first char of string to uppercase.
      // path.forEach((item, i) => {
      //   // Only capitalize starting from the second element.
      //   if (i > 0) {
      //     path[i] = path[i].charAt(0).toUpperCase() + path[i].slice(1);
      //   }
      // });

      return last ? (
        <p key={to}>{value}</p>
      ) : (
        <LinkRouter href={to} key={to}>
          {value}
        </LinkRouter>
      );
    });
  };

  return (
    <>
      <div>
        <Breadcrumbs aria-label="Breadcrumb" separator="›">
          <LinkRouter href="/">خانه</LinkRouter>
          {dynamicParts()}
        </Breadcrumbs>
      </div>
    </>
  );
};

export default SimpleBreadcrumbs;
