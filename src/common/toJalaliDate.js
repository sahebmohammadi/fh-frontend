import { toArabicDigits } from "@/utils/toArabicNumbers";
import moment from "jalali-moment";

const toJalaliDate = (date) => {
  const newDate = moment(date, "YYYY/MM/DD").locale("fa").format("YYYY/MM/DD");
  return toArabicDigits(newDate);
};

export default toJalaliDate;
