const Button = ({ className = "", onClick, children, ...props }) => {
  const angleDeg = 360 - 27;
  const colors = ["rgba(43,1,121,1) 0%", " rgba(121,56,242,1) 100%"];

  return (
    <button
      style={{
        backgroundImage: `linear-gradient(${angleDeg}deg,  ${colors[0]} 0%, ${colors[1]} 100%)`,
        color: "#fff",
      }}
      {...props}
      className={`py-3 px-6 rounded-lg bg-gradient-to-l text-white ${className}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
