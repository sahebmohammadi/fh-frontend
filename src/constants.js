export const navItemsLinks = [
  { name: "خانه", href: "/" },
  { name: "دوره های اموزشی", href: "/courses" },
  { name: "بلاگ ها", href: "/blogs" },
  { name: "لینک های مفید", href: "/about-us" },
];
export const addToCartMessage = "به سبد خرید اضافه شد";
export const removeFromCartMessage = "از  سبد خرید حذف شد";

export const products = [
  {
    _id: 111,
    name: "دوره جامع و پیشرفته جاوااسکریپت",
    pageLink: "advanced-javascript",
    description: [
      { support: "پشتیبانی مادام العمر" },
      { support: "آپدیت ریگان دوره" },
      { support: "دوره پژوره محور" },
      { support: "مدت زمان دانلود، نامحدود" },
    ],
    price: 110000,
    offPrice: 110000,
    discount: 0,
    image: "/images/React.svg",
  },
  {
    _id: 222,
    name: "دوره گیت و گیت هاب",
    pageLink: "git-github",
    description: [
      { support: "پشتیبانی مادام العمر" },
      { support: "آپدیت ریگان دوره" },
      { support: "دوره پژوره محور" },
      { support: "مدت زمان دانلود، نامحدود" },
    ],
    price: 390000,
    offPrice: 239000,
    discount: 15,
    image: "/images/React.svg",
  },
  {
    _id: 333,
    name: "دوره متخصص ES6",
    pageLink: "ES6-course",
    description: [
      { support: "پشتیبانی مادام العمر" },
      { support: "آپدیت ریگان دوره" },
      { support: "دوره پژوره محور" },
      { support: "مدت زمان دانلود، نامحدود" },
    ],
    price: 490000,
    offPrice: 339000,
    discount: 20,
    image: "/images/es6.svg",
  },
  {
    _id: 444,
    name: "دوره متخصص ریکت",
    description: [
      { support: "پشتیبانی مادام العمر" },
      { support: "آپدیت ریگان دوره" },
      { support: "دوره پژوره محور" },
      { support: "مدت زمان دانلود، نامحدود" },
    ],
    price: 290000,
    offPrice: 139000,
    discount: 10,
    image: "/images/React.svg",
  },
  {
    _id: 555,
    name: "دوره فرمیک",
    description: [
      { support: "پشتیبانی مادام العمر" },
      { support: "آپدیت ریگان دوره" },
      { support: "دوره پژوره محور" },
      { support: "مدت زمان دانلود، نامحدود" },
    ],
    price: 290000,
    offPrice: 290000,
    discount: 0,
    image: "/images/React.svg",
  },
  {
    _id: 666,
    name: "دوره نکست ",
    description: [
      { support: "پشتیبانی مادام العمر" },
      { support: "آپدیت ریگان دوره" },
      { support: "دوره پژوره محور" },
      { support: "مدت زمان دانلود، نامحدود" },
    ],
    price: 190000,
    offPrice: 190000,
    discount: 0,
    image: "/images/React.svg",
  },
];

export const formConstants = {
  email: "ایمیل را وارد کنید",
  password: "رمز عبور را وارد کنید",
  emailtype: "ایمیل نامعتبر است",
  phoneNumber: "شماره همراه را وارد کنید",
  phoneType: "شماره همراه باید 11 رقم باشد",
  passwordType: "پسورد باید حداقل شامل 8 کاراکتر باشد",
  passwordMatch: "پسورد هم خوانی ندارد",
  fullName: "نام و نام خانوادگی را وارد کنید",
  fullNameType: "نام و نام خانوادگی باید حداقل شامل 6 کاراکتر باشد",
  loginTitle: "ورود به حساب کاربری",
  signupTitle: "تکمیل اطلاعات شخصی برای سفارش",
  labels: {
    email: "ایمیل",
    phoneNumber: "شماره موبایل",
    name: "نام و نام خانوادگی",
    password: "رمز عبور",
    passwordConfirm: "تکرار رمز عبور",
  },
};
