
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/containers/**/*.{js,ts,jsx,tsx}",
    "./src/common/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: "class",
  theme: {
    extend: {
      borderWidth: {
        panel: "4px",
      },
      colors: {
        primary: {
          DEFAULT: "#733DD8",
        },
        myGray: {
          500: "#2F2F2F",
          400: "#4E4E4E",
          300: "#979797",
          200: "#f9f9f9",
          100: "#F4F4F4",
        },
      },
      margin: {
        px4: "4px",
      },
      width: {
        staticPage: "100rem",
      },
      spacing: {
        88: "22rem",
        120: "30rem",
        192: "48rem",
      },
    },
    fontFamily: {
      sans: ["Mukta", "ui-sans-serif"],
      serif: ["ui-serif"],
      mono: ['"Source Code Pro"'],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("@tailwindcss/forms"),
    require("@tailwindcss/aspect-ratio"),
  ],
  corePlugins: {
    // ...
    outline: false,
  },
};
